program cdQB1;

uses
  Vcl.Forms,
  cdQB in 'cdQB.pas' {Form4},
  dmQbIntergration in 'dmQbIntergration.pas' {QBIntergration: TDataModule},
  dmQBDesktopIntegration in 'dmQBDesktopIntegration.pas' {QBOnlineIntegration: TDataModule},
  dmQBOnlineIntegration in 'dmQBOnlineIntegration.pas' {QBDesktopIntegration: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.
