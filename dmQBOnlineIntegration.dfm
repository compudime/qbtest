object QBOnlineIntegration: TQBOnlineIntegration
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 381
  Width = 881
  object memExpenseList: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 562
    Top = 50
    object memExpenseListAccountName: TStringField
      FieldName = 'AccountName'
      Size = 35
    end
    object memExpenseListAccountType: TStringField
      FieldName = 'AccountType'
      Size = 35
    end
    object memExpenseListFill: TIntegerField
      FieldName = 'Fill'
    end
    object memExpenseListFillO: TIntegerField
      FieldName = 'FillO'
    end
  end
  object memVendorList: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 178
    Top = 50
    object memVendorListVendorName: TStringField
      FieldName = 'VendorName'
      Size = 40
    end
    object memVendorListFill: TIntegerField
      FieldName = 'Fill'
    end
    object memVendorListFillO: TIntegerField
      FieldName = 'FillO'
    end
  end
  object memCustomerList: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 434
    Top = 50
    object memCustomerListCustomerName: TStringField
      FieldName = 'CustomerName'
      Size = 40
    end
  end
  object memItemList: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 50
    Top = 50
    object memItemListItemName: TStringField
      FieldName = 'ItemName'
      Size = 100
    end
    object memItemListItemType: TStringField
      FieldName = 'ItemType'
      Size = 100
    end
    object memItemListItemAccount: TStringField
      FieldName = 'ItemAccount'
      Size = 100
    end
  end
  object memClassList: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 818
    Top = 50
    object memClassListClassName: TStringField
      FieldName = 'ClassName'
      Size = 100
    end
  end
  object memCustomerVendorList: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 690
    Top = 50
    object memCustomerVendorListName: TStringField
      FieldName = 'Name'
      Size = 100
    end
  end
  object memAccountList: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 42
    Top = 106
    object memAccountListAccountName: TStringField
      FieldName = 'AccountName'
      Size = 100
    end
    object memAccountListAccountType: TStringField
      FieldName = 'AccountType'
      Size = 50
    end
  end
  object commonFDQ: TFDQuery
    Connection = Form4.FDConnectionOnline
    Left = 176
    Top = 120
  end
  object Xml1: TJvSimpleXML
    IndentString = '  '
    Left = 306
    Top = 156
  end
  object commonFDQDetail: TFDQuery
    Connection = Form4.FDConnectionOnline
    Left = 496
    Top = 128
  end
  object XMLDocument: TXMLDocument
    Left = 434
    Top = 156
    DOMVendorDesc = 'MSXML'
  end
  object memSalesReceipt: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 306
    Top = 50
    object memSalesReceiptCat: TStringField
      FieldName = 'Cat'
      Size = 25
    end
    object memSalesReceiptName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object memSalesReceiptQbName: TStringField
      FieldName = 'QbName'
      Size = 100
    end
    object memSalesReceiptQbAccount: TStringField
      FieldName = 'QbAccount'
      Size = 100
    end
    object memSalesReceiptQbAccountType: TStringField
      FieldName = 'QbAccountType'
      Size = 100
    end
    object memSalesReceiptQbType: TStringField
      FieldName = 'QbType'
      Size = 6
    end
    object memSalesReceiptInActive: TBooleanField
      FieldName = 'InActive'
    end
    object memSalesReceiptSortColumn: TIntegerField
      FieldName = 'SortColumn'
    end
  end
end
