unit cdQB;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls,
  Vcl.Forms, Vcl.Dialogs, dmQbIntergration, dmQBOnlineIntegration, dmQBDesktopIntegration,
  FireDAC.Phys.CDataQuickBooksOnlineDef,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.CDataQuickBooksOnline, FireDAC.VCLUI.Wait,
  Vcl.StdCtrls, Data.DB, FireDAC.Comp.Client, Vcl.ComCtrls, Vcl.ExtCtrls, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.Grids, Vcl.DBGrids;

type
  TForm4 = class(TForm)
    FDPhysCDataQuickBooksOnlineDriverLink1: TFDPhysCDataQuickBooksOnlineDriverLink;
    FDConnectionOnline: TFDConnection;
    pnlOnlineSetup: TPanel;
    lblCompanyId: TLabel;
    edtCompanyId: TEdit;
    lblOnlinesetup: TLabel;
    pnlDesktopSetup: TPanel;
    lblDesktopSetup: TLabel;
    lblUser: TLabel;
    edtUser: TEdit;
    lblPassword: TLabel;
    edtConnectString: TEdit;
    pnlVendor: TPanel;
    lblVendorName: TLabel;
    edtVendorName: TEdit;
    mmoVendor: TMemo;
    btnVendorFind: TButton;
    btnVendorQuickAdd: TButton;
    btnVendorGetAccountPrefills: TButton;
    btnVendorExpenseList: TButton;
    btnVendorNameList: TButton;
    pnlBill: TPanel;
    pnlBillLeft: TPanel;
    edtDelNo: TEdit;
    edtDelDate: TEdit;
    edtDelDueDate: TEdit;
    edtVendInvNo: TEdit;
    edtDelAmount: TEdit;
    edtDelExpenseAccount: TEdit;
    edtDelMemo: TEdit;
    lblBillInfo: TLabel;
    mmoBill: TMemo;
    btnBillAdd: TButton;
    btnBillDelete: TButton;
    btnBillFind: TButton;
    btnBillGetStatus: TButton;
    btnBillUpdate: TButton;
    stat: TStatusBar;
    pnlSales: TPanel;
    lblSales: TLabel;
    btnSalesReceiptAddUpdate: TButton;
    mmoSales: TMemo;
    btnSalesReceiptDelete: TButton;
    btnSalesJournalEntryAddUpdate: TButton;
    pnl1: TPanel;
    mmoList: TMemo;
    btnCustomerNameList: TButton;
    btnItemNameList: TButton;
    btnClassNameList: TButton;
    btnCustomerVendorNameList: TButton;
    btnVendorCheckBalance: TButton;
    rbDesktop: TRadioButton;
    btnAccountNameList: TButton;
    btnConnect: TButton;
    rbOnline: TRadioButton;
    rbLegecy: TRadioButton;
    edtDelRefID: TEdit;
    btnFillTestRecord: TButton;
    chkQbSalesExportMode: TCheckBox;
    Label2: TLabel;
    Edit1: TEdit;
    edtSalesRefID: TEdit;
    Edit2: TEdit;
    FDConnectionDesktop: TFDConnection;
    FDQuery1: TFDQuery;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    procedure btnConnectClick(Sender: TObject);
    procedure btnVendorFindClick(Sender: TObject);
    procedure btnVendorQuickAddClick(Sender: TObject);
    procedure btnVendorGetAccountPrefillsClick(Sender: TObject);
    procedure btnVendorExpenseListClick(Sender: TObject);
    procedure btnVendorNameListClick(Sender: TObject);
    procedure btnVendorCheckBalanceClick(Sender: TObject);
    procedure btnBillAddClick(Sender: TObject);
    procedure btnBillDeleteClick(Sender: TObject);
    procedure btnBillFindClick(Sender: TObject);
    procedure btnBillGetStatusClick(Sender: TObject);
    procedure btnBillUpdateClick(Sender: TObject);
    procedure btnSalesReceiptAddUpdateClick(Sender: TObject);
    procedure btnSalesJournalEntryAddUpdateClick(Sender: TObject);
    procedure btnSalesReceiptDeleteClick(Sender: TObject);
    procedure btnCustomerNameListClick(Sender: TObject);
    procedure btnItemNameListClick(Sender: TObject);
    procedure btnClassNameListClick(Sender: TObject);
    procedure btnCustomerVendorNameListClick(Sender: TObject);
    procedure btnAccountNameListClick(Sender: TObject);
    procedure btnFillTestRecordClick(Sender: TObject);
    procedure rbDesktopClick(Sender: TObject);
    procedure rbOnlineClick(Sender: TObject);
  private
    AQBSalesRecord: TQBSalesRecord;
    AQBOnlineSalesRecord: TQBOnlineSalesRecord;
    AQBDesktopSalesRecord: TQBDesktopSalesRecord;

    // procedure DebugList (AList: TFDMemTable; AField1: string; AField2: string; AField3: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.btnAccountNameListClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    QBIntergration.AccountNameList;
    mmoList.Lines.Add('btnAccountNameListClick: ' + BoolToStr(QBIntergration.memAccountList.Active, True) + ', Count: '
      + IntToStr(QBIntergration.memAccountList.RecordCount));
    if QBIntergration.QbLastError <> '' then
      mmoList.Lines.Add('Error: ' + QBIntergration.QbLastError);
    DataSource1.DataSet := QBIntergration.memAccountList;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      AccountNameList;
      mmoList.Lines.Add('btnAccountNameListClick: ' + BoolToStr(memAccountList.Active, True) + ', Count: ' +
        IntToStr(memAccountList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memAccountList;
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      AccountNameList;
      mmoList.Lines.Add('btnAccountNameListClick: ' + BoolToStr(memAccountList.Active, True) + ', Count: ' +
        IntToStr(memAccountList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memAccountList;
    end;
  end;
end;

procedure TForm4.btnBillAddClick(Sender: TObject);
var
  ARefID: string;
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    ARefID := QBIntergration.BillAdd(edtVendorName.Text, edtDelNo.Text, edtDelDate.Text, edtDelDueDate.Text,
      edtVendInvNo.Text, edtDelAmount.Text, edtDelExpenseAccount.Text, edtDelMemo.Text);
    mmoBill.Lines.Add('btnBillAddClick: ' + ARefID);
    edtDelRefID.Text := ARefID;
    if QBIntergration.QbLastError <> '' then
      mmoBill.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      ARefID := BillAdd(edtVendorName.Text, edtDelNo.Text, edtDelDate.Text, edtDelDueDate.Text, edtVendInvNo.Text,
        edtDelAmount.Text, edtDelExpenseAccount.Text, edtDelMemo.Text);
      mmoBill.Lines.Add('btnBillAddClick: ' + ARefID);
      edtDelRefID.Text := ARefID;
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      ARefID := BillAdd(edtVendorName.Text, edtDelNo.Text, edtDelDate.Text, edtDelDueDate.Text, edtVendInvNo.Text,
        edtDelAmount.Text, edtDelExpenseAccount.Text, edtDelMemo.Text);
      mmoBill.Lines.Add('btnBillAddClick: ' + ARefID);
      edtDelRefID.Text := ARefID;
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnBillDeleteClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    if edtDelRefID.Text = '' then
      mmoBill.Lines.Add('btnBillDeleteClick: Ref ID empty')
    else
      mmoBill.Lines.Add('btnBillDeleteClick: ' + BoolToStr(QBIntergration.BillDelete(edtDelRefID.Text), True));
    if QBIntergration.QbLastError <> '' then
      mmoBill.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      if edtDelRefID.Text = '' then
        mmoBill.Lines.Add('btnBillDeleteClick: Ref ID empty')
      else
        mmoBill.Lines.Add('btnBillDeleteClick: ' + BoolToStr(BillDelete(edtDelRefID.Text), True));
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      if edtDelRefID.Text = '' then
        mmoBill.Lines.Add('btnBillDeleteClick: Ref ID empty')
      else
        mmoBill.Lines.Add('btnBillDeleteClick: ' + BoolToStr(BillDelete(edtDelRefID.Text), True));
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError);
    end;
  end;

end;

procedure TForm4.btnBillFindClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    mmoBill.Lines.Add('btnBillFindClick: ' + BoolToStr(QBIntergration.BillFind(edtVendInvNo.Text), True));
    if QBIntergration.QbLastError <> '' then
      mmoBill.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      mmoBill.Lines.Add('btnBillFindClick: ' + BoolToStr(BillFind(edtVendInvNo.Text), True));
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      mmoBill.Lines.Add('btnBillFindClick: ' + BoolToStr(BillFind(edtVendInvNo.Text), True));
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError);
    end;
  end;

end;

procedure TForm4.btnBillGetStatusClick(Sender: TObject);
var
  AIsPaid: Boolean;
  ABillAmount: Double;
  ATransType: string;
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    mmoBill.Lines.Add('btnBillGetStatusClick: ' + BoolToStr(QBIntergration.BillGetStatus(edtDelRefID.Text, AIsPaid,
      ABillAmount, ATransType), True));
    if QBIntergration.QbLastError <> '' then
      mmoBill.Lines.Add('Error: ' + QBIntergration.QbLastError)
    else
    begin
      mmoBill.Lines.Add('AIsPaid: ' + BoolToStr(AIsPaid, True));
      mmoBill.Lines.Add('ABillAmount: ' + FloatToStr(ABillAmount));
      mmoBill.Lines.Add('ATransType: ' + ATransType);
    end;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      mmoBill.Lines.Add('btnBillGetStatusClick: ' + BoolToStr(BillGetStatus(edtDelRefID.Text, AIsPaid, ABillAmount,
        ATransType), True));
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError)
      else
      begin
        mmoBill.Lines.Add('AIsPaid: ' + BoolToStr(AIsPaid, True));
        mmoBill.Lines.Add('ABillAmount: ' + FloatToStr(ABillAmount));
        mmoBill.Lines.Add('ATransType: ' + ATransType);
      end;

    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      mmoBill.Lines.Add('btnBillGetStatusClick: ' + BoolToStr(BillGetStatus(edtDelRefID.Text, AIsPaid, ABillAmount,
        ATransType), True));
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError)
      else
      begin
        mmoBill.Lines.Add('AIsPaid: ' + BoolToStr(AIsPaid, True));
        mmoBill.Lines.Add('ABillAmount: ' + FloatToStr(ABillAmount));
        mmoBill.Lines.Add('ATransType: ' + ATransType);
      end;

    end;
  end;
end;

procedure TForm4.btnBillUpdateClick(Sender: TObject);
var
  ARefID: string;
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    ARefID := QBIntergration.BillUpdate(edtDelRefID.Text, edtVendorName.Text, edtDelNo.Text, edtDelDate.Text,
      edtDelDueDate.Text, edtVendInvNo.Text, edtDelAmount.Text, edtDelExpenseAccount.Text, edtDelMemo.Text);
    mmoBill.Lines.Add('btnBillUpdateClick: ' + ARefID);
    edtDelRefID.Text := ARefID;
    if QBIntergration.QbLastError <> '' then
      mmoBill.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      ARefID := BillUpdate(edtDelRefID.Text, edtVendorName.Text, edtDelNo.Text, edtDelDate.Text, edtDelDueDate.Text,
        edtVendInvNo.Text, edtDelAmount.Text, edtDelExpenseAccount.Text, edtDelMemo.Text);
      mmoBill.Lines.Add('btnBillUpdateClick: ' + ARefID);
      edtDelRefID.Text := ARefID;
      if QbLastError <> '' then
        mmoBill.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnClassNameListClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    QBIntergration.ClassNameList;
    mmoList.Lines.Add('btnClassNameListClick: ' + BoolToStr(QBIntergration.memClassList.Active, True) + ', Count: ' +
      IntToStr(QBIntergration.memClassList.RecordCount));
    if QBIntergration.QbLastError <> '' then
      mmoList.Lines.Add('Error: ' + QBIntergration.QbLastError);
    DataSource1.DataSet := QBIntergration.memClassList;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      ClassNameList;
      mmoList.Lines.Add('btnClassNameListClick: ' + BoolToStr(memClassList.Active, True) + ', Count: ' +
        IntToStr(memClassList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memClassList;
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      ClassNameList;
      mmoList.Lines.Add('btnClassNameListClick: ' + BoolToStr(memClassList.Active, True) + ', Count: ' +
        IntToStr(memClassList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memClassList;
    end;
  end;
end;

procedure TForm4.btnConnectClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    if QBIntergration.QbLastError <> '' then
      mmoVendor.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      try
        FDConnectionOnline.Connected := False;
        FDConnectionDesktop.Connected := False;
        FDConnectionOnline.Connected := True;
        mmoVendor.Lines.Add('Connected to online QB successfully');
        commonFDQ.Connection := FDConnectionOnline;
        commonFDQDetail.Connection := FDConnectionOnline;
      except
        on E: Exception do
          QbLastError := E.Message;
      end;
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      try
        FDConnectionOnline.Connected := False;
        FDConnectionDesktop.Connected := False;
        FDConnectionDesktop.Params.Password := Edit2.Text;
        FDConnectionDesktop.Params.UserName := edtUser.Text;

        FDConnectionDesktop.Connected := True;
        mmoVendor.Lines.Add('Connected to desktop QB successfully');
        commonFDQ.Connection := FDConnectionDesktop;
        commonFDQDetail.Connection := FDConnectionDesktop;
      except
        on E: Exception do
          QbLastError := E.Message;
      end;
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnCustomerNameListClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    QBIntergration.CustomerNameList;
    mmoList.Lines.Add('btnCustomerNameListClick: ' + BoolToStr(QBIntergration.memCustomerList.Active, True) +
      ', Count: ' + IntToStr(QBIntergration.memCustomerList.RecordCount));
    if QBIntergration.QbLastError <> '' then
      mmoList.Lines.Add('Error: ' + QBIntergration.QbLastError);
    DataSource1.DataSet := QBIntergration.memCustomerList;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      CustomerNameList;
      mmoList.Lines.Add('btnCustomerNameListClick: ' + BoolToStr(memCustomerList.Active, True) + ', Count: ' +
        IntToStr(memCustomerList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memCustomerList;
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      CustomerNameList;
      mmoList.Lines.Add('btnCustomerNameListClick: ' + BoolToStr(memCustomerList.Active, True) + ', Count: ' +
        IntToStr(memCustomerList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memCustomerList;
    end;
  end;
end;

(*
  procedure TForm4.DebugList (AList: TFDMemTable; AField1: string; AField2: string; AField3: string);
  begin
  mmoBill.Clear;
  AList.First;
  while  not AList.EOF do begin
  mmoBill.Lines.Add(Alist.FieldByName(AField1).AsString + ' ' +  Alist.FieldByName(AField2).AsString + ' ' +
  Alist.FieldByName(AField3).AsString);
  AList.Next;
  end;
  end;
*)
procedure TForm4.btnCustomerVendorNameListClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    QBIntergration.CustomerVendorNameList;
    mmoList.Lines.Add('btnCustomerVendorNameListClick: ' + BoolToStr(QBIntergration.memCustomerVendorList.Active, True)
      + ', Count: ' + IntToStr(QBIntergration.memCustomerVendorList.RecordCount));
    if QBIntergration.QbLastError <> '' then
      mmoList.Lines.Add('Error: ' + QBIntergration.QbLastError);
    DataSource1.DataSet := QBIntergration.memCustomerVendorList;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      CustomerVendorNameList;
      mmoList.Lines.Add('btnCustomerVendorNameListClick: ' + BoolToStr(memCustomerVendorList.Active, True) + ', Count: '
        + IntToStr(memCustomerVendorList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memCustomerVendorList;
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      CustomerVendorNameList;
      mmoList.Lines.Add('btnCustomerVendorNameListClick: ' + BoolToStr(memCustomerVendorList.Active, True) + ', Count: '
        + IntToStr(memCustomerVendorList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memCustomerVendorList;
    end;
  end;
end;

procedure TForm4.btnFillTestRecordClick(Sender: TObject);
var
  I: Integer;
begin
  // AQBSalesRecord is filled differently according to JournalEntryMode, so we block changing after Test Record Filled
  if rbLegecy.Checked then
  begin
    QBIntergration.JournalEntryMode := chkQbSalesExportMode.Checked;
    chkQbSalesExportMode.Enabled := False;
    btnSalesReceiptAddUpdate.Enabled := not chkQbSalesExportMode.Checked;
    btnSalesJournalEntryAddUpdate.Enabled := chkQbSalesExportMode.Checked;

    QBIntergration.ResetQBSalesRecord(AQBSalesRecord);
    AQBSalesRecord.Date := StrToDate('12/06/2018');
    AQBSalesRecord.LastRefID := '';
    AQBSalesRecord.LogList := '1001, 1002, 1003';

    AQBSalesRecord.NetSales := 38923.63;
    AQBSalesRecord.TaxableSales := 34364.10;
    AQBSalesRecord.NonTaxableSales := -91.62;
    AQBSalesRecord.TaxExemptSales := 1601.33;
    AQBSalesRecord.Tax1Amount := 3049.82;
    AQBSalesRecord.Tax2Amount := 0;
    AQBSalesRecord.COGS := 0;

    I := 0;

    SetLength(AQBSalesRecord.Payments, I + 1);
    AQBSalesRecord.Payments[I].PayName := 'Cash';
    if QBIntergration.JournalEntryMode then
      AQBSalesRecord.Payments[I].PayAmount := 14498.34
    else
      AQBSalesRecord.Payments[I].PayAmount := 14498.34 * -1;
    Inc(I);

    SetLength(AQBSalesRecord.Payments, I + 1);
    AQBSalesRecord.Payments[I].PayName := 'Check';
    if QBIntergration.JournalEntryMode then
      AQBSalesRecord.Payments[I].PayAmount := 2235.13
    else
      AQBSalesRecord.Payments[I].PayAmount := 2235.13 * -1;
    Inc(I);

    SetLength(AQBSalesRecord.Payments, I + 1);
    AQBSalesRecord.Payments[I].PayName := 'Crdt Card';
    if QBIntergration.JournalEntryMode then
      AQBSalesRecord.Payments[I].PayAmount := 22119.53
    else
      AQBSalesRecord.Payments[I].PayAmount := 22119.53 * -1;
    Inc(I);

    SetLength(AQBSalesRecord.Payments, I + 1);
    AQBSalesRecord.Payments[I].PayName := 'On Account';
    if QBIntergration.JournalEntryMode then
      AQBSalesRecord.Payments[I].PayAmount := 399.69
    else
      AQBSalesRecord.Payments[I].PayAmount := 399.69 * -1;
    Inc(I);

    SetLength(AQBSalesRecord.Payments, I + 1);
    AQBSalesRecord.Payments[I].PayName := 'Store Crdt';
    if QBIntergration.JournalEntryMode then
      AQBSalesRecord.Payments[I].PayAmount := -329.06
    else
      AQBSalesRecord.Payments[I].PayAmount := -329.06 * -1;
    Inc(I);

    SetLength(AQBSalesRecord.Payments, I + 1);
    AQBSalesRecord.Payments[I].PayName := 'PdAcnt';
    AQBSalesRecord.Payments[I].PayAmount := 100;

    I := 0;
    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: BAKERY';
    AQBSalesRecord.DeptEntries[I].Amount := 987.54;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: CATERING';
    AQBSalesRecord.DeptEntries[I].Amount := 4600;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: DELI';
    AQBSalesRecord.DeptEntries[I].Amount := 2687.24;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: FISH';
    AQBSalesRecord.DeptEntries[I].Amount := 4743.21;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: GIFT CARD';
    AQBSalesRecord.DeptEntries[I].Amount := 125.36;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: GROCERIES';
    AQBSalesRecord.DeptEntries[I].Amount := 6497.5;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: MEATS';
    AQBSalesRecord.DeptEntries[I].Amount := 1500;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: POULTRY';
    AQBSalesRecord.DeptEntries[I].Amount := 1785.41;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: PRODUCE';
    AQBSalesRecord.DeptEntries[I].Amount := 3698.4;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: WAITERS';
    AQBSalesRecord.DeptEntries[I].Amount := 6249.15;
    Inc(I);

    SetLength(AQBSalesRecord.DeptEntries, I + 1);
    AQBSalesRecord.DeptEntries[I].DeptName := 'Dept: WINE';
    AQBSalesRecord.DeptEntries[I].Amount := 3000;
    // Inc(I);
  end
  else if rbOnline.Checked then
  begin
    QBOnlineIntegration.JournalEntryMode := chkQbSalesExportMode.Checked;
    chkQbSalesExportMode.Enabled := False;
    btnSalesReceiptAddUpdate.Enabled := not chkQbSalesExportMode.Checked;
    btnSalesJournalEntryAddUpdate.Enabled := chkQbSalesExportMode.Checked;

    QBOnlineIntegration.ResetQBSalesRecord(AQBOnlineSalesRecord);
    AQBOnlineSalesRecord.Date := StrToDate('12/06/2018');
    AQBOnlineSalesRecord.LastRefID := '';
    AQBOnlineSalesRecord.LogList := '1001, 1002, 1003';

    AQBOnlineSalesRecord.NetSales := 38923.63;
    AQBOnlineSalesRecord.TaxableSales := 34364.10;
    AQBOnlineSalesRecord.NonTaxableSales := -91.62;
    AQBOnlineSalesRecord.TaxExemptSales := 1601.33;
    AQBOnlineSalesRecord.Tax1Amount := 3049.82;
    AQBOnlineSalesRecord.Tax2Amount := 0;
    AQBOnlineSalesRecord.COGS := 0;

    I := 0;

    SetLength(AQBOnlineSalesRecord.Payments, I + 1);
    AQBOnlineSalesRecord.Payments[I].PayName := 'Cash';
    if QBOnlineIntegration.JournalEntryMode then
      AQBOnlineSalesRecord.Payments[I].PayAmount := 14498.34
    else
      AQBOnlineSalesRecord.Payments[I].PayAmount := 14498.34 * -1;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.Payments, I + 1);
    AQBOnlineSalesRecord.Payments[I].PayName := 'Check';
    if QBOnlineIntegration.JournalEntryMode then
      AQBOnlineSalesRecord.Payments[I].PayAmount := 2235.13
    else
      AQBOnlineSalesRecord.Payments[I].PayAmount := 2235.13 * -1;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.Payments, I + 1);
    AQBOnlineSalesRecord.Payments[I].PayName := 'Crdt Card';
    if QBOnlineIntegration.JournalEntryMode then
      AQBOnlineSalesRecord.Payments[I].PayAmount := 22119.53
    else
      AQBOnlineSalesRecord.Payments[I].PayAmount := 22119.53 * -1;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.Payments, I + 1);
    AQBOnlineSalesRecord.Payments[I].PayName := 'On Account';
    if QBOnlineIntegration.JournalEntryMode then
      AQBOnlineSalesRecord.Payments[I].PayAmount := 399.69
    else
      AQBOnlineSalesRecord.Payments[I].PayAmount := 399.69 * -1;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.Payments, I + 1);
    AQBOnlineSalesRecord.Payments[I].PayName := 'Store Crdt';
    if QBOnlineIntegration.JournalEntryMode then
      AQBOnlineSalesRecord.Payments[I].PayAmount := -329.06
    else
      AQBOnlineSalesRecord.Payments[I].PayAmount := -329.06 * -1;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.Payments, I + 1);
    AQBOnlineSalesRecord.Payments[I].PayName := 'PdAcnt';
    AQBOnlineSalesRecord.Payments[I].PayAmount := 100;

    I := 0;
    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: BAKERY';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 987.54;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: CATERING';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 4600;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: DELI';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 2687.24;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: FISH';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 4743.21;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: GIFT CARD';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 125.36;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: GROCERIES';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 6497.5;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: MEATS';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 1500;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: POULTRY';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 1785.41;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: PRODUCE';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 3698.4;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: WAITERS';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 6249.15;
    Inc(I);

    SetLength(AQBOnlineSalesRecord.DeptEntries, I + 1);
    AQBOnlineSalesRecord.DeptEntries[I].DeptName := 'Dept: WINE';
    AQBOnlineSalesRecord.DeptEntries[I].Amount := 3000;
  end
  else if rbDesktop.Checked then
  begin
    QBDesktopIntegration.JournalEntryMode := chkQbSalesExportMode.Checked;
    chkQbSalesExportMode.Enabled := False;
    btnSalesReceiptAddUpdate.Enabled := not chkQbSalesExportMode.Checked;
    btnSalesJournalEntryAddUpdate.Enabled := chkQbSalesExportMode.Checked;

    QBDesktopIntegration.ResetQBSalesRecord(AQBDesktopSalesRecord);
    AQBDesktopSalesRecord.Date := StrToDate('12/06/2018');
    AQBDesktopSalesRecord.LastRefID := '';
    AQBDesktopSalesRecord.LogList := '1001, 1002, 1003';

    AQBDesktopSalesRecord.NetSales := 38923.63;
    AQBDesktopSalesRecord.TaxableSales := 34364.10;
    AQBDesktopSalesRecord.NonTaxableSales := -91.62;
    AQBDesktopSalesRecord.TaxExemptSales := 1601.33;
    AQBDesktopSalesRecord.Tax1Amount := 3049.82;
    AQBDesktopSalesRecord.Tax2Amount := 0;
    AQBDesktopSalesRecord.COGS := 0;

    I := 0;

    SetLength(AQBDesktopSalesRecord.Payments, I + 1);
    AQBDesktopSalesRecord.Payments[I].PayName := 'Cash';
    if QBDesktopIntegration.JournalEntryMode then
      AQBDesktopSalesRecord.Payments[I].PayAmount := 14498.34
    else
      AQBDesktopSalesRecord.Payments[I].PayAmount := 14498.34 * -1;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.Payments, I + 1);
    AQBDesktopSalesRecord.Payments[I].PayName := 'Check';
    if QBDesktopIntegration.JournalEntryMode then
      AQBDesktopSalesRecord.Payments[I].PayAmount := 2235.13
    else
      AQBDesktopSalesRecord.Payments[I].PayAmount := 2235.13 * -1;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.Payments, I + 1);
    AQBDesktopSalesRecord.Payments[I].PayName := 'Crdt Card';
    if QBDesktopIntegration.JournalEntryMode then
      AQBDesktopSalesRecord.Payments[I].PayAmount := 22119.53
    else
      AQBDesktopSalesRecord.Payments[I].PayAmount := 22119.53 * -1;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.Payments, I + 1);
    AQBDesktopSalesRecord.Payments[I].PayName := 'On Account';
    if QBDesktopIntegration.JournalEntryMode then
      AQBDesktopSalesRecord.Payments[I].PayAmount := 399.69
    else
      AQBDesktopSalesRecord.Payments[I].PayAmount := 399.69 * -1;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.Payments, I + 1);
    AQBDesktopSalesRecord.Payments[I].PayName := 'Store Crdt';
    if QBDesktopIntegration.JournalEntryMode then
      AQBDesktopSalesRecord.Payments[I].PayAmount := -329.06
    else
      AQBDesktopSalesRecord.Payments[I].PayAmount := -329.06 * -1;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.Payments, I + 1);
    AQBDesktopSalesRecord.Payments[I].PayName := 'PdAcnt';
    AQBDesktopSalesRecord.Payments[I].PayAmount := 100;

    I := 0;
    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: BAKERY';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 987.54;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: CATERING';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 4600;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: DELI';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 2687.24;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: FISH';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 4743.21;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: GIFT CARD';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 125.36;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: GROCERIES';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 6497.5;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: MEATS';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 1500;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: POULTRY';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 1785.41;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: PRODUCE';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 3698.4;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: WAITERS';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 6249.15;
    Inc(I);

    SetLength(AQBDesktopSalesRecord.DeptEntries, I + 1);
    AQBDesktopSalesRecord.DeptEntries[I].DeptName := 'Dept: WINE';
    AQBDesktopSalesRecord.DeptEntries[I].Amount := 3000;
  end;

  mmoSales.Lines.Add('Test Record Filled');
end;

procedure TForm4.btnItemNameListClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    QBIntergration.ItemNameList;
    mmoList.Lines.Add('btnItemNameListClick: ' + BoolToStr(QBIntergration.memItemList.Active, True) + ', Count: ' +
      IntToStr(QBIntergration.memItemList.RecordCount));
    if QBIntergration.QbLastError <> '' then
      mmoList.Lines.Add('Error: ' + QBIntergration.QbLastError);
    DataSource1.DataSet := QBIntergration.memItemList;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      ItemNameList;
      mmoList.Lines.Add('btnItemNameListClick: ' + BoolToStr(memItemList.Active, True) + ', Count: ' +
        IntToStr(memItemList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memItemList;
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      ItemNameList;
      mmoList.Lines.Add('btnItemNameListClick: ' + BoolToStr(memItemList.Active, True) + ', Count: ' +
        IntToStr(memItemList.RecordCount));
      if QbLastError <> '' then
        mmoList.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memItemList;
    end;
  end;
end;

procedure TForm4.btnSalesJournalEntryAddUpdateClick(Sender: TObject);
var
  ARefID: string;
begin
  if rbLegecy.Checked then
  begin
    if AQBSalesRecord.NetSales = 0 then
    begin
      mmoSales.Lines.Add('FilltestRecord First');
      Exit;
    end;

    QBIntergration.QbLastError := '';
    QBIntergration.JournalEntryMode := True;
    ARefID := QBIntergration.SalesJournalEntryAddUpdate(AQBSalesRecord);
    mmoSales.Lines.Add('btnSalesJournalEntryAddUpdateClick: ' + ARefID);
    edtSalesRefID.Text := ARefID;
    if QBIntergration.QbLastError <> '' then
      mmoSales.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      if AQBOnlineSalesRecord.NetSales = 0 then
      begin
        mmoSales.Lines.Add('FilltestRecord First');
        Exit;
      end;
      JournalEntryMode := True;
      QbLastError := '';
      ARefID := SalesJournalEntryAddUpdate(AQBOnlineSalesRecord);
      mmoSales.Lines.Add('btnSalesJournalEntryAddUpdateClick: ' + ARefID);
      edtSalesRefID.Text := ARefID;
      if QbLastError <> '' then
        mmoSales.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      if AQBDesktopSalesRecord.NetSales = 0 then
      begin
        mmoSales.Lines.Add('FilltestRecord First');
        Exit;
      end;
      JournalEntryMode := True;
      QbLastError := '';
      ARefID := SalesJournalEntryAddUpdate(AQBDesktopSalesRecord);
      mmoSales.Lines.Add('btnSalesJournalEntryAddUpdateClick: ' + ARefID);
      edtSalesRefID.Text := ARefID;
      if QbLastError <> '' then
        mmoSales.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnSalesReceiptAddUpdateClick(Sender: TObject);
var
  ARefID: string;
begin
  if rbLegecy.Checked then
  begin
    if AQBSalesRecord.NetSales = 0 then
    begin
      mmoSales.Lines.Add('FilltestRecord First');
      Exit;
    end;

    QBIntergration.QbLastError := '';
    QBIntergration.JournalEntryMode := False;
    ARefID := QBIntergration.SalesReceiptAddUpdate(AQBSalesRecord);
    mmoSales.Lines.Add('btnSalesReceiptAddUpdateClick: ' + ARefID);
    edtSalesRefID.Text := ARefID;
    if QBIntergration.QbLastError <> '' then
      mmoSales.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      if AQBOnlineSalesRecord.NetSales = 0 then
      begin
        mmoSales.Lines.Add('FilltestRecord First');
        Exit;
      end;
      JournalEntryMode := False;
      QbLastError := '';
      ARefID := SalesReceiptAddUpdate(AQBOnlineSalesRecord);
      mmoSales.Lines.Add('btnSalesReceiptAddUpdateClick: ' + ARefID);
      edtSalesRefID.Text := ARefID;
      if QbLastError <> '' then
        mmoSales.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      if AQBDesktopSalesRecord.NetSales = 0 then
      begin
        mmoSales.Lines.Add('FilltestRecord First');
        Exit;
      end;
      JournalEntryMode := False;
      QbLastError := '';
      ARefID := SalesReceiptAddUpdate(AQBDesktopSalesRecord);
      mmoSales.Lines.Add('btnSalesReceiptAddUpdateClick: ' + ARefID);
      edtSalesRefID.Text := ARefID;
      if QbLastError <> '' then
        mmoSales.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnSalesReceiptDeleteClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    if edtSalesRefID.Text = '' then
      mmoSales.Lines.Add('btnSalesReceiptDeleteClick: Ref ID empty')
    else
      mmoSales.Lines.Add('btnSalesReceiptDeleteClick: ' +
        BoolToStr(QBIntergration.SalesReceiptDelete(edtSalesRefID.Text), True));
    if QBIntergration.QbLastError <> '' then
      mmoSales.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      if edtSalesRefID.Text = '' then
        mmoSales.Lines.Add('btnSalesReceiptDeleteClick: Ref ID empty')
      else
        mmoSales.Lines.Add('btnSalesReceiptDeleteClick: ' + BoolToStr(SalesReceiptDelete(edtSalesRefID.Text), True));
      if QbLastError <> '' then
        mmoSales.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      if edtSalesRefID.Text = '' then
        mmoSales.Lines.Add('btnSalesReceiptDeleteClick: Ref ID empty')
      else
        mmoSales.Lines.Add('btnSalesReceiptDeleteClick: ' + BoolToStr(SalesReceiptDelete(edtSalesRefID.Text), True));
      if QbLastError <> '' then
        mmoSales.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnVendorCheckBalanceClick(Sender: TObject);
var
  ABalance: Currency;
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    mmoVendor.Lines.Add('btnVendorCheckBalanceClick: ' + BoolToStr(QBIntergration.VendorCheckBalance(edtVendorName.Text,
      ABalance), True));
    mmoVendor.Lines.Add('Balance: ' + FloatToStr(ABalance));
    if QBIntergration.QbLastError <> '' then
      mmoVendor.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      mmoList.Lines.Add('btnVendorCheckBalanceClick: ' + BoolToStr(VendorCheckBalance(edtVendorName.Text,
        ABalance), True));
      mmoVendor.Lines.Add('Balance: ' + FloatToStr(ABalance));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      mmoList.Lines.Add('btnVendorCheckBalanceClick: ' + BoolToStr(VendorCheckBalance(edtVendorName.Text,
        ABalance), True));
      mmoVendor.Lines.Add('Balance: ' + FloatToStr(ABalance));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      mmoList.Lines.Add('btnVendorCheckBalanceClick: ' + BoolToStr(VendorCheckBalance(edtVendorName.Text,
        ABalance), True));
      mmoVendor.Lines.Add('Balance: ' + FloatToStr(ABalance));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnVendorExpenseListClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    QBIntergration.VendorExpenseList(edtVendorName.Text);
    mmoVendor.Lines.Add('btnVendorExpenseListClick: ' + BoolToStr(QBIntergration.memExpenseList.Active, True) +
      ', Count: ' + IntToStr(QBIntergration.memExpenseList.RecordCount));
    if QBIntergration.QbLastError <> '' then
      mmoVendor.Lines.Add('Error: ' + QBIntergration.QbLastError);
    DataSource1.DataSet := QBIntergration.memExpenseList;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      VendorExpenseList(edtVendorName.Text);
      mmoVendor.Lines.Add('btnVendorExpenseListClick: ' + BoolToStr(memExpenseList.Active, True) + ', Count: ' +
        IntToStr(memExpenseList.RecordCount));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memExpenseList;
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      VendorExpenseList(edtVendorName.Text);
      mmoVendor.Lines.Add('btnVendorExpenseListClick: ' + BoolToStr(memExpenseList.Active, True) + ', Count: ' +
        IntToStr(memExpenseList.RecordCount));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memExpenseList;
    end;
  end;

end;

procedure TForm4.btnVendorFindClick(Sender: TObject);
var
  ABalance: Currency;
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    mmoVendor.Lines.Add('btnVendorFindClick: ' + BoolToStr(QBIntergration.VendorFind(edtVendorName.Text), True));
    if QBIntergration.QbLastError <> '' then
      mmoVendor.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      mmoVendor.Lines.Add('btnVendorFindClick: ' + BoolToStr(VendorFind(edtVendorName.Text, ABalance), True));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      mmoVendor.Lines.Add('btnVendorFindClick: ' + BoolToStr(VendorFind(edtVendorName.Text, ABalance), True));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.btnVendorGetAccountPrefillsClick(Sender: TObject);
var
  AList: TStringList;
begin
  if rbLegecy.Checked then
  begin
    try
      AList := TStringList.Create;
      QBIntergration.QbLastError := '';
      mmoVendor.Lines.Add('btnVendorGetAccountPrefillsClick: ' +
        BoolToStr(QBIntergration.VendorGetAccountPrefills(edtVendorName.Text, AList), True));
      mmoVendor.Lines.Add('AList Count: ' + IntToStr(AList.Count));
      if QBIntergration.QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QBIntergration.QbLastError);
    finally
      AList.Free;
    end;
  end
  else if rbOnline.Checked then
  begin
    try
      AList := TStringList.Create;
      With QBOnlineIntegration do
      begin
        QbLastError := '';
        mmoVendor.Lines.Add('btnVendorGetAccountPrefillsClick: ' +
          BoolToStr(VendorGetAccountPrefills(edtVendorName.Text, AList), True));
        mmoVendor.Lines.Add('AList Count: ' + IntToStr(AList.Count));
        if QbLastError <> '' then
          mmoVendor.Lines.Add('Error: ' + QbLastError);
      end;
    finally
      AList.Free;
    end;
  end
  else if rbDesktop.Checked then
  begin
    try
      AList := TStringList.Create;
      With QBDesktopIntegration do
      begin
        QbLastError := '';
        mmoVendor.Lines.Add('btnVendorGetAccountPrefillsClick: ' +
          BoolToStr(VendorGetAccountPrefills(edtVendorName.Text, AList), True));
        mmoVendor.Lines.Add('AList Count: ' + IntToStr(AList.Count));
        if QbLastError <> '' then
          mmoVendor.Lines.Add('Error: ' + QbLastError);
      end;
    finally
      AList.Free;
    end;
  end;
end;

procedure TForm4.btnVendorNameListClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    QBIntergration.VendorNameList(edtVendorName.Text);
    mmoVendor.Lines.Add('btnVendorNameListClick: ' + BoolToStr(QBIntergration.memVendorList.Active, True) + ', Count: '
      + IntToStr(QBIntergration.memVendorList.RecordCount));
    if QBIntergration.QbLastError <> '' then
      mmoVendor.Lines.Add('Error: ' + QBIntergration.QbLastError);
    DataSource1.DataSet := QBIntergration.memVendorList;
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      VendorNameList(edtVendorName.Text);
      mmoList.Lines.Add('btnVendorNameListClick: ' + BoolToStr(memVendorList.Active, True) + ', Count: ' +
        IntToStr(memVendorList.RecordCount));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memVendorList;
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      VendorNameList(edtVendorName.Text);
      mmoList.Lines.Add('btnVendorNameListClick: ' + BoolToStr(memVendorList.Active, True) + ', Count: ' +
        IntToStr(memVendorList.RecordCount));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
      DataSource1.DataSet := memVendorList;
    end;
  end;
end;

procedure TForm4.btnVendorQuickAddClick(Sender: TObject);
begin
  if rbLegecy.Checked then
  begin
    QBIntergration.QbLastError := '';
    if not QBIntergration.VendorFind(edtVendorName.Text) then
      mmoVendor.Lines.Add('btnVendorQuickAddClick: ' +
        BoolToStr(QBIntergration.VendorQuickAdd(edtVendorName.Text), True));
    if QBIntergration.QbLastError <> '' then
      mmoVendor.Lines.Add('Error: ' + QBIntergration.QbLastError);
  end
  else if rbOnline.Checked then
  begin
    With QBOnlineIntegration do
    begin
      QbLastError := '';
      if not VendorFind(edtVendorName.Text) then
        mmoVendor.Lines.Add('btnVendorQuickAddClick: ' + BoolToStr(VendorQuickAdd(edtVendorName.Text), True));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end
  else if rbDesktop.Checked then
  begin
    With QBDesktopIntegration do
    begin
      QbLastError := '';
      if not VendorFind(edtVendorName.Text) then
        mmoVendor.Lines.Add('btnVendorQuickAddClick: ' + BoolToStr(VendorQuickAdd(edtVendorName.Text), True));
      if QbLastError <> '' then
        mmoVendor.Lines.Add('Error: ' + QbLastError);
    end;
  end;
end;

procedure TForm4.rbDesktopClick(Sender: TObject);
begin
  btnConnectClick(rbDesktop);
end;

procedure TForm4.rbOnlineClick(Sender: TObject);
begin
  btnConnectClick(rbOnline);
end;

end.
