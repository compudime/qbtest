unit dmQbIntergration;

interface

uses
  SysUtils, Classes, iqbaccount, iqbvendor, iqbbill, iqbcore, iqbobjsearch, JvSimpleXml, DB, dxmdaset, iqbvendorcredit,
  iqbcustomer, iqbsalesreceipt, iqbitem, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, iqbjournalentry,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  EHandleInApp = class(Exception)
  end;

  TQBSalesSetting = record
    QbAccount: string;
    QbAccountType: string;
    QbName: string;
    QbType: string;
  end;

  TQBSalesPayments = record
    PayName: string;
    PayAmount: Currency;
  end;

  TQBSalesDeptEntry = record
    DeptName: string;
    Amount: Currency;
  end;

  TQBSalesRecord = record
    LastRefID: string;
    LogList: string;
    Date: TDateTime;
    NetSales: Currency;
    TaxableSales: Currency;
    NonTaxableSales: Currency;
    TaxExemptSales: Currency;
    Tax1Amount: Currency;
    Tax2Amount: Currency;
    COGS: Currency;
    Payments: array of TQBSalesPayments;
    DeptEntries: array of TQBSalesDeptEntry;
  end;

  TQBIntergration = class(TDataModule)
    iqbAccount1: TiqbAccount;
    iqbBill1: TiqbBill;
    iqbVendor1: TiqbVendor;
    iqbObjSearch1: TiqbObjSearch;
    memExpenseList: TFDMemTable;
    memExpenseListAccountName: TStringField;
    memExpenseListFill: TIntegerField;
    memExpenseListAccountType: TStringField;
    memVendorList: TFDMemTable;
    memVendorListVendorName: TStringField;
    memExpenseListFillO: TIntegerField;
    memVendorListFill: TIntegerField;
    memVendorListFillO: TIntegerField;
    iqbVCredit1: TiqbVendorCredit;
    iqbSalesReceipt1: TiqbSalesReceipt;
    iqbCustomer1: TiqbCustomer;
    iqbItem1: TiqbItem;
    memSalesReceipt: TFDMemTable;
    memSalesReceiptCat: TStringField;
    memSalesReceiptName: TStringField;
    memSalesReceiptQbName: TStringField;
    memSalesReceiptInActive: TBooleanField;
    memCustomerList: TFDMemTable;
    memCustomerListCustomerName: TStringField;
    memItemList: TFDMemTable;
    memItemListItemName: TStringField;
    memItemListItemType: TStringField;
    memItemListItemAccount: TStringField;
    memSalesReceiptSortColumn: TIntegerField;
    XMLDocument: TXMLDocument;
    memClassList: TFDMemTable;
    memClassListClassName: TStringField;
    Xml1: TJvSimpleXML;
    iqbJournalEntry1: TiqbJournalEntry;
    memSalesReceiptQbAccount: TStringField;
    memCustomerVendorList: TFDMemTable;
    memCustomerVendorListName: TStringField;
    memAccountList: TFDMemTable;
    memAccountListAccountName: TStringField;
    memAccountListAccountType: TStringField;
    memSalesReceiptQbType: TStringField;
    memSalesReceiptQbAccountType: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    FQbLastError: string;
    FExpenseAccount: string;
    FCompanyName: string;
    FConnectString: string;
    FAPAccount: string;
    FAPAccountID: string;
    FVendorItemMode: Boolean;
    FJournalEntryMode: Boolean;
    function GetQBConnectionString: string;
    function Bill_Credit_AddUpdate(AAction, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
      AAcountID, ARefID, AMemo: string): string;
    procedure ExpenseListFillByType(ADataSet: TDataSet; AAccountType: TiqbobjsearchAccountTypes; ACaption: string);
  protected
    property ExpenseAccount: string read FExpenseAccount;
    property APAccount: string read FAPAccount;
    property APAccountID: string read FAPAccountID;
    property QBConnectionString: string read GetQBConnectionString;

    procedure ExpenseListFill;
    procedure VendorListFill;
    procedure CustomerListFill;
    procedure ItemListFill;
    procedure ClassListFill;

    function QbAccountType(const AAccountType: string): TiqbobjsearchAccountTypeEnum;
    function ExpenseAccountIDFind(const AAccountName: string): string;
    function AccountIDFind(const AAccountName: string; AAccountType: TiqbobjsearchAccountTypeEnum): string;
    function VendorListIdFromAccountPrefills(const AVendorName, AAccountName: string): string;
    function VendorListIdFromInvoice(const AVendorName, AAccountName: string): string;

    function SalesSettings(const ACat, AName: string): string;
    function SalesSettingsA(const ACat, AName: string): TQBSalesSetting;
    procedure SalesSettingsAssert(const ACat, AName: string; var AIndex: Integer);

    procedure SalesReceiptAddItem(const AItemName, AItemAmount: string); overload;
    procedure SalesReceiptAddItem(const AItemCat, AItemName: string; const AValue: Currency); overload;

    procedure JournalEntryAddItem(const AAccount, AAccountType, AType, AName, AValue, AMemo: string); overload;
    procedure JournalEntryAddItem(const AItemCat, AItemName: string; AValue: Currency; AMemo: string = ''); overload;

    function ItemFind(const AItemName: string): string;
    function CustomerFind(const ACustomerName: string): string;
  public
    property QbLastError: string read FQbLastError write FQbLastError;
    property VendorItemMode: Boolean read FVendorItemMode write FVendorItemMode;
    property JournalEntryMode: Boolean read FJournalEntryMode write FJournalEntryMode;

    function BillAdd(AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount, AMemo: string): string;
    function BillDelete(const ARefID: string): Boolean;
    function BillFind(const ADelNo: string): Boolean;
    function BillGetStatus(const ARefID: string; var AIsPaid: Boolean; var ABillAmount: Double;
      var ATransType: string): Boolean;
    function BillUpdate(ARefID, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
      AMemo: string): string;

    function VendorFind(const AVendorName: string): Boolean;
    function VendorQuickAdd(const AVendorName: string): Boolean;
    function VendorGetAccountPrefills(const AVendorName: string; var AList: TStringList): Boolean;
    procedure VendorExpenseList(const AVendorName: string);
    procedure VendorNameList(const AVendorName: string);

    function SalesReceiptAddUpdate(ARec: TQBSalesRecord): string;
    function SalesReceiptDelete(const ARefID: string): Boolean;
    function SalesJournalEntryAddUpdate(ARec: TQBSalesRecord): string;

    procedure CustomerNameList;
    procedure ItemNameList;
    procedure ClassNameList;
    procedure CustomerVendorNameList;
    procedure AccountNameList;

    procedure SalesSettingsFill; // not sure what is this
    procedure SalesSettingsMemToXML; // not sure what is this

    procedure ResetQBSalesRecord(var ARecord: TQBSalesRecord); // ??
    function VendorCheckBalance(const AVendorName: string; var ABalance: Currency): Boolean;
  end;

function QBIntergration: TQBIntergration;

implementation

uses Forms, Dialogs, System.Variants, JclStreams, cxDateUtils, System.Rtti;
{$R *.dfm}

var
  __QBIntergration: TQBIntergration;

const
  QBConnectionStringA = 'ApplicationName="Total Inventory Solutions" ';
  Error817 = 'Error retreiving QB Expense Account Number, can''t add bill.' + #10#13 + 'Possible solutions:' + #10#13 +
    '1. Add Full Rights to Chart of Account for current user (QB Enterprise).' + #10#13 +
    '2. Add Financial Rights for current user (other QB versions).' + #10#13 +
    '3. Set the Expense Account for Vendor in POS and assign the account as an "Account Prefill" in QB.' + #10#13 +
    'QB Error: %s';

function QBIntergration: TQBIntergration;
begin
  if (__QBIntergration = nil) then
    __QBIntergration := TQBIntergration.Create(Application);

  Result := __QBIntergration;
end;

{ TQBIntergration }

function TQBIntergration.ExpenseAccountIDFind(const AAccountName: string): string;
begin
  Result := AccountIDFind(AAccountName, atCostOfGoodsSold);
  if Result = '' then
    Result := AccountIDFind(AAccountName, atExpense);
end;

function TQBIntergration.AccountIDFind(const AAccountName: string; AAccountType: TiqbobjsearchAccountTypeEnum): string;
  procedure AIdInList;
  var
    I: Integer;
  begin
    for I := 0 to iqbObjSearch1.ResultCount - 1 do
    begin
      iqbAccount1.Reset;
      iqbAccount1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[I];
      if SameText(iqbAccount1.AccountName, AAccountName) then
      begin
        Result := iqbAccount1.RefId;
        Break;
      end;
    end;
  end;

begin
  Result := '';
  try
    iqbObjSearch1.Reset;
    iqbObjSearch1.QueryType := qtAccountSearch;
    iqbObjSearch1.AccountType := AAccountType;
    iqbObjSearch1.NameContains := AAccountName;
    iqbObjSearch1.Search;

    if iqbObjSearch1.ResultCount > 0 then
      AIdInList;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

procedure TQBIntergration.AccountNameList;
begin
  if not memAccountList.Active then
    memAccountList.Open;

  if memAccountList.IsEmpty then
  begin
    ExpenseListFillByType(memAccountList, atExpense, 'Expense');
    ExpenseListFillByType(memAccountList, atCostOfGoodsSold, 'Cost Of Goods Sold');
    ExpenseListFillByType(memAccountList, atAccountsReceivable, 'Accounts Receivable');
    ExpenseListFillByType(memAccountList, atIncome, 'Income');
    ExpenseListFillByType(memAccountList, atOtherAsset, 'Other Asset');
    ExpenseListFillByType(memAccountList, atOtherExpense, 'Other Expense');
    ExpenseListFillByType(memAccountList, atOtherIncome, 'Other Income');
    ExpenseListFillByType(memAccountList, atOtherCurrentAsset, 'Other Current Asset');
    ExpenseListFillByType(memAccountList, atOtherCurrentLiability, 'Other Current Liability');
    ExpenseListFillByType(memAccountList, atBankAccount, 'Bank Account');
    ExpenseListFillByType(memAccountList, atNonPosting, 'Non Posting');
    // ExpenseListFillByType(memAccountList, , '');
  end;
end;

function TQBIntergration.BillAdd(AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
  AMemo: string): string;
var
  AAcountID: string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      if AVendorName = '' then
        raise EHandleInApp.Create('QB Vendor Name not set, can''t add bill.');
      if SameText(AExpenseAccount, '<DEFAULT>') then
        AExpenseAccount := ExpenseAccount;
      if AExpenseAccount = '' then
        raise EHandleInApp.Create('QB Expense Account not set, can''t add bill.');

      AAcountID := ExpenseAccountIDFind(AExpenseAccount);

      if (AAcountID = '') and VendorItemMode then
        AAcountID := ItemFind(AExpenseAccount);

      if (AAcountID = '') and (Copy(QbLastError, 1, 3) = '817') then
      begin
        AAcountID := VendorListIdFromAccountPrefills(AVendorName, AExpenseAccount);
        if AAcountID = '' then
          AAcountID := VendorListIdFromInvoice(AVendorName, AExpenseAccount);
      end;

      if AAcountID = '' then
      begin
        if Copy(QbLastError, 1, 3) = '817' then
          raise EHandleInApp.CreateFmt(Error817, [QbLastError])
        else if QbLastError <> '' then
          raise EHandleInApp.CreateFmt('Error retreiving QB Expense Account Number, can''t add bill.' + #10#13 +
            'QB Error: %s', [QbLastError])
        else
          raise EHandleInApp.CreateFmt('QB Expense Account Number not found for Account %s, can''t add bill.',
            [AExpenseAccount]);
      end;
      Result := Bill_Credit_AddUpdate('Add', AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
        AAcountID, '', AMemo);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.BillDelete(const ARefID: string): Boolean;
var
  ASeacrhCredit: Boolean;
begin
  Result := False;
  ASeacrhCredit := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbBill1.Reset;
      iqbBill1.QBRequestId := '';
      iqbBill1.Get(ARefID);
      Result := False;
      iqbBill1.Delete;
      Result := True;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
        begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
          ASeacrhCredit := True;
        end
        else
          QbLastError := E.Message;
      end;
    end;
    if (not Result) and ASeacrhCredit then
    begin
      try
        iqbVCredit1.Reset;
        iqbVCredit1.QBRequestId := '';
        iqbVCredit1.Get(ARefID);
        Result := False;
        iqbVCredit1.Delete;
        Result := True;
        QbLastError := '';
      except
        on E: Exception do
        begin
          if SameText(E.Message,
            Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
            [ARefID])) then
            // Do nothing
          else
            QbLastError := E.Message;
        end;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.BillFind(const ADelNo: string): Boolean;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbObjSearch1.Reset;
      iqbObjSearch1.QueryType := qtBillSearch;
      iqbObjSearch1.MaxResults := 1;

      iqbObjSearch1.RefNumber := ADelNo;
      iqbObjSearch1.Search;

      Result := iqbObjSearch1.ResultCount = 1;
      if Result then
      begin
        Result := False;
        iqbBill1.Reset;
        iqbBill1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[0];
        Result := True;
      end
      else
      begin
        iqbObjSearch1.Reset;
        iqbObjSearch1.QueryType := qtVendorCreditSearch;
        iqbObjSearch1.MaxResults := 1;

        iqbObjSearch1.RefNumber := ADelNo;
        iqbObjSearch1.Search;

        Result := iqbObjSearch1.ResultCount = 1;
        if Result then
        begin
          Result := False;
          iqbVCredit1.Reset;
          iqbVCredit1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[0];
          Result := True;
        end;
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ADelNo])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ADelNo])
        else
          QbLastError := E.Message;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.BillGetStatus(const ARefID: string; var AIsPaid: Boolean; var ABillAmount: Double;
  var ATransType: string): Boolean;
var
  ASeacrhCredit: Boolean;
begin
  Result := False;
  ASeacrhCredit := False;
  ATransType := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbBill1.Reset;
      iqbBill1.QBRequestId := '';
      iqbBill1.Get(ARefID);
      AIsPaid := iqbBill1.IsPaid;
      ABillAmount := StrToFloat(iqbBill1.Amount);
      Result := True;
      ATransType := 'Bill';
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
        begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
          ASeacrhCredit := True;
        end
        else
          QbLastError := E.Message;
      end;
    end;
    if (not Result) and ASeacrhCredit then
    begin
      try
        iqbVCredit1.Reset;
        iqbVCredit1.QBRequestId := '';
        iqbVCredit1.Get(ARefID);
        AIsPaid := True;
        ABillAmount := 0 - StrToFloat(iqbVCredit1.Amount);
        Result := True;
        ATransType := 'Credit';
        QbLastError := '';
      except
        on E: Exception do
        begin
          if SameText(E.Message,
            Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
            [ARefID])) then
            // Do nothing
          else
            QbLastError := E.Message;
        end;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.BillUpdate(ARefID, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
  AMemo: string): string;
var
  AAcountID: string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      if AVendorName = '' then
        raise EHandleInApp.Create('QB Vendor Name not set, can''t update bill.');
      if SameText(AExpenseAccount, '<DEFAULT>') then
        AExpenseAccount := ExpenseAccount;
      if AExpenseAccount = '' then
        raise EHandleInApp.Create('QB Expense Account not set, can''t update bill.');
      AAcountID := ExpenseAccountIDFind(AExpenseAccount);
      if (AAcountID = '') and VendorItemMode then
        AAcountID := ItemFind(AExpenseAccount);
      if (AAcountID = '') and (Copy(QbLastError, 1, 3) = '817') then
      begin
        AAcountID := VendorListIdFromAccountPrefills(AVendorName, AExpenseAccount);
        if AAcountID = '' then
          AAcountID := VendorListIdFromInvoice(AVendorName, AExpenseAccount);
      end;

      if AAcountID = '' then
      begin
        if Copy(QbLastError, 1, 3) = '817' then
          raise EHandleInApp.CreateFmt(Error817, [QbLastError])
        else if QbLastError <> '' then
          raise EHandleInApp.CreateFmt('Error retreiving QB Expense Account Number, can''t update bill.' + #10#13 +
            'QB Error: %s', [QbLastError])
        else
          raise EHandleInApp.CreateFmt('QB Expense Account Number not found for Account %s, can''t update bill.',
            [AExpenseAccount]);
      end;
      Result := Bill_Credit_AddUpdate('Update', AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount,
        AExpenseAccount, AAcountID, ARefID, AMemo);
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ADelNo])
        else
          QbLastError := E.Message;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.Bill_Credit_AddUpdate(AAction, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount,
  AExpenseAccount, AAcountID, ARefID, AMemo: string): string;
begin
  if (APAccount <> '') and (APAccountID = '') then
  begin
    FAPAccountID := AccountIDFind(APAccount, atAccountsPayable);
    if APAccountID = '' then
      FAPAccount := '';
  end;

  if StrToFloat(AAmount) < 0 then
  begin
    iqbVCredit1.Reset;
    iqbVCredit1.QBRequestId := '';
    if ARefID <> '' then
      iqbVCredit1.Get(ARefID);
    iqbVCredit1.VendorName := AVendorName;
    iqbVCredit1.RefNumber := AVendInvNo;
    iqbVCredit1.Memo := AMemo;
    iqbVCredit1.TransactionDate := ADate;
    if APAccountID <> '' then
      iqbVCredit1.APAccountID := APAccountID;
    if not VendorItemMode then
    begin
      iqbVCredit1.ExpenseCount := 1;
      iqbVCredit1.ExpenseAccountId[0] := AAcountID;
      iqbVCredit1.ExpenseAmount[0] := FloatToStr(Abs(StrToFloat(AAmount)));
    end
    else
    begin
      iqbVCredit1.ItemCount := 1;
      iqbVCredit1.ItemId[0] := AAcountID;
      iqbVCredit1.ItemAmount[0] := FloatToStr(Abs(StrToFloat(AAmount)));
    end;

    // iqbVCredit1.RefId := ARefID;
    // iqbBill1.ExpenseAccountName[0] := AExpenseAccount;
    if SameText(AAction, 'Update') then
      iqbVCredit1.Update
    else
      iqbVCredit1.Add;
    Result := iqbVCredit1.RefId;
  end
  else
  begin
    iqbBill1.Reset;
    iqbBill1.QBRequestId := '';
    if ARefID <> '' then
      iqbBill1.Get(ARefID);
    iqbBill1.VendorName := AVendorName;
    iqbBill1.RefNumber := AVendInvNo;
    iqbBill1.Memo := AMemo;
    iqbBill1.TransactionDate := ADate;
    iqbBill1.DueDate := ADueDate;
    if APAccountID <> '' then
      iqbBill1.APAccountID := APAccountID;

    if not VendorItemMode then
    begin
      iqbBill1.ExpenseCount := 1;
      iqbBill1.ExpenseAccountId[0] := AAcountID;
      iqbBill1.ExpenseAmount[0] := AAmount;
    end
    else
    begin
      iqbBill1.ItemCount := 1;
      iqbBill1.ItemId[0] := AAcountID;
      iqbBill1.ItemAmount[0] := AAmount;
    end;
    // iqbBill1.ExpenseAccountName[0] := AExpenseAccount;

    if SameText(AAction, 'Update') then
      iqbBill1.Update
    else
      iqbBill1.Add;
    Result := iqbBill1.RefId;
  end;
end;

procedure TQBIntergration.ClassListFill;
var
  I, x: Integer;
  AXmlEle: TJvSimpleXmlElem;
begin
  if not memClassList.Active then
    memClassList.Open;
  memClassList.First;
  while not memClassList.Eof do
    memClassList.Delete;

  // AppActions.ShowHourGlassCursor;
  memClassList.DisableControls;
  try
    try
      iqbObjSearch1.Reset;
      iqbObjSearch1.QueryType := qtClass;
      iqbObjSearch1.Search;

      for I := 0 to iqbObjSearch1.ResultCount - 1 do
      begin
        Xml1.LoadFromString(iqbObjSearch1.ResultAggregate[I]);
        AXmlEle := Xml1.Root;
        for x := 0 to AXmlEle.Items.Count - 1 do
          if SameText(AXmlEle.Items[x].Name, 'FullName') then
          begin
            memClassList.Insert;
            memClassListClassName.AsString := AXmlEle.Items[x].Value;
            memClassList.Post;
            Break;
          end;
      end;

      // memClassList.SortedField := 'ClassName';
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memClassList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBIntergration.ClassNameList;
begin
  if not memClassList.Active then
    memClassList.Open;
  if memClassList.IsEmpty then
    ClassListFill;
  memClassList.First;
end;

function TQBIntergration.CustomerFind(const ACustomerName: string): string;
  procedure AIdInList;
  var
    I: Integer;
  begin
    for I := 0 to iqbObjSearch1.ResultCount - 1 do
    begin
      iqbCustomer1.Reset;
      iqbCustomer1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[I];
      if SameText(iqbCustomer1.CustomerName, ACustomerName) then
      begin
        Result := iqbCustomer1.RefId;
        Break;
      end;
    end;
  end;

begin
  Result := '';
  try
    iqbObjSearch1.Reset;
    iqbObjSearch1.QueryType := qtCustomerSearch;
    iqbObjSearch1.NameContains := ACustomerName;
    iqbObjSearch1.Search;

    if iqbObjSearch1.ResultCount > 0 then
      AIdInList;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

procedure TQBIntergration.CustomerListFill;
var
  I: Integer;
begin
  if not memCustomerList.Active then
    memCustomerList.Open;
  memCustomerList.First;
  while not memCustomerList.Eof do
    memCustomerList.Delete;

  // AppActions.ShowHourGlassCursor;
  memCustomerList.DisableControls;
  try
    try
      iqbObjSearch1.Reset;
      iqbObjSearch1.QueryType := qtCustomerSearch;
      iqbObjSearch1.Search;

      for I := 0 to iqbObjSearch1.ResultCount - 1 do
      begin
        iqbCustomer1.Reset;
        iqbCustomer1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[I];
        memCustomerList.Insert;
        memCustomerListCustomerName.AsString := iqbCustomer1.CustomerName;
        memCustomerList.Post;
      end;

      // memCustomerList.SortedField := 'CustomerName';
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memCustomerList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBIntergration.CustomerNameList;
begin
  if not memCustomerList.Active then
    memCustomerList.Open;
  if memCustomerList.IsEmpty then
    CustomerListFill;
  memCustomerList.First;
end;

procedure TQBIntergration.CustomerVendorNameList;
begin
  if not memCustomerVendorList.Active then
    memCustomerVendorList.Open;
  if not memCustomerVendorList.IsEmpty then
    Exit;

  if not memCustomerList.Active then
    memCustomerList.Open;
  if memCustomerList.IsEmpty then
    CustomerListFill;;

  if not memVendorList.Active then
    memVendorList.Open;
  if memVendorList.IsEmpty then
    VendorListFill;

  memVendorList.First;
  while not memVendorList.Eof do
  begin
    memCustomerVendorList.Insert;
    memCustomerVendorListName.AsString := memVendorListVendorName.AsString;
    memCustomerVendorList.Post;
    memVendorList.Next;
  end;

  memVendorList.First;
  while not memCustomerList.Eof do
  begin
    memCustomerVendorList.Insert;
    memCustomerVendorListName.AsString := memCustomerListCustomerName.AsString;
    memCustomerVendorList.Post;
    memCustomerList.Next;
  end;

  memCustomerVendorList.First;
end;

procedure TQBIntergration.DataModuleCreate(Sender: TObject);
begin
  FExpenseAccount := ''; // ProgPref('QbBillExpenseAccount');
  FAPAccount := ''; // ProgPref('QbBillAPAccount');
  FCompanyName := ''; // ProgPref('QbBillCompanyName');
  FConnectString := ''; // ProgPref('QbConnectString');
  FVendorItemMode := False; // SameText(ProgPref('QbVendExpenseItemMode'), 'Items');
  FJournalEntryMode := False; // SameText(ProgPref('QbSalesExportMode'), 'Journal Entry');
  FAPAccountID := '';

  iqbAccount1.QBConnectionString := QBConnectionString;
  iqbObjSearch1.QBConnectionString := QBConnectionString;
  iqbBill1.QBConnectionString := QBConnectionString;
  iqbVendor1.QBConnectionString := QBConnectionString;
  iqbVendor1.QBXMLVersion := '8.0';
  iqbVCredit1.QBXMLVersion := '8.0';

  iqbJournalEntry1.QBConnectionString := QBConnectionString;
  iqbJournalEntry1.QBXMLVersion := '8.0';
  iqbSalesReceipt1.QBConnectionString := QBConnectionString;
  iqbSalesReceipt1.QBXMLVersion := '8.0';
  iqbCustomer1.QBConnectionString := QBConnectionString;
  iqbCustomer1.QBXMLVersion := '8.0';
  iqbItem1.QBConnectionString := QBConnectionString;
  iqbItem1.QBXMLVersion := '8.0';
end;

procedure TQBIntergration.ExpenseListFill;
var
  I: Integer;
begin
  if not memExpenseList.Active then
    memExpenseList.Open;
  memExpenseList.First;
  while not memExpenseList.Eof do
    memExpenseList.Delete;

  // AppActions.ShowHourGlassCursor;
  memExpenseList.DisableControls;
  try
    try
      ExpenseListFillByType(memExpenseList, atExpense, 'Expense');
      ExpenseListFillByType(memExpenseList, atCostOfGoodsSold, 'Cost Of Goods Sold');

      // memExpenseList.SortedField := 'AccountName';
      I := 100;
      memExpenseList.First;
      while not memExpenseList.Eof do
      begin
        Inc(I);
        memExpenseList.Edit;
        memExpenseListFill.AsInteger := I;
        memExpenseListFillO.AsInteger := I;
        memExpenseList.Post;
        memExpenseList.Next;
      end;
      // memExpenseList.SortedField := 'Fill';

      if ExpenseAccount <> '' then
      begin
        memExpenseList.Insert;
        memExpenseListAccountName.AsString := '<DEFAULT>';
        memExpenseListAccountType.AsString := '(' + ExpenseAccount + ')';
        memExpenseListFill.AsInteger := 0;
        memExpenseList.Post;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memExpenseList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBIntergration.ExpenseListFillByType(ADataSet: TDataSet; AAccountType: TiqbobjsearchAccountTypes;
  ACaption: string);
var
  I: Integer;
begin
  iqbObjSearch1.Reset;
  iqbObjSearch1.QueryType := qtAccountSearch;
  iqbObjSearch1.AccountType := AAccountType;
  iqbObjSearch1.Search();

  for I := 0 to iqbObjSearch1.ResultCount - 1 do
  begin
    iqbAccount1.Reset;
    iqbAccount1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[I];
    ADataSet.Insert;
    ADataSet.FieldByName('AccountName').AsString := iqbAccount1.AccountName;
    ADataSet.FieldByName('AccountType').AsString := ACaption;
    if ADataSet.FindField('Fill') <> nil then
      ADataSet.FieldByName('Fill').AsInteger := 99;
    ADataSet.Post;
  end;
end;

function TQBIntergration.GetQBConnectionString: string;
begin
  Result := QBConnectionStringA;
  if FConnectString <> '' then
    Result := FConnectString + ' ' + Result;
  if FCompanyName <> '' then
    Result := Result + Format('CompanyFile="%s" ', [FCompanyName]);
end;

function TQBIntergration.ItemFind(const AItemName: string): string;
  procedure AIdInList;
  var
    I: Integer;
  begin
    for I := 0 to iqbObjSearch1.ResultCount - 1 do
    begin
      iqbItem1.Reset;
      iqbItem1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[I];
      if SameText(iqbItem1.ItemName, AItemName) then
      begin
        Result := iqbItem1.RefId;
        Break;
      end;
    end;
  end;

begin
  Result := '';
  try
    iqbObjSearch1.Reset;
    iqbObjSearch1.QueryType := qtItemSearch;
    iqbObjSearch1.NameContains := AItemName;
    iqbObjSearch1.Search;

    if iqbObjSearch1.ResultCount > 0 then
      AIdInList;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

procedure TQBIntergration.ItemListFill;
var
  I: Integer;
begin
  if not memItemList.Active then
    memItemList.Open;
  memItemList.First;
  while not memItemList.Eof do
    memItemList.Delete;

  // AppActions.ShowHourGlassCursor;
  memItemList.DisableControls;
  try
    try
      iqbObjSearch1.Reset;
      iqbObjSearch1.QueryType := qtItemSearch;
      iqbObjSearch1.Search;

      for I := 0 to iqbObjSearch1.ResultCount - 1 do
      begin
        iqbItem1.Reset;
        iqbItem1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[I];
        memItemList.Insert;
        memItemListItemName.AsString := iqbItem1.ItemName;
        memItemListItemAccount.AsString := iqbItem1.AccountName;
        case iqbItem1.ItemType of
          itUnknown:
            memItemListItemType.AsString := 'Unknown';
          itService:
            memItemListItemType.AsString := 'Service';
          itInventory:
            memItemListItemType.AsString := 'Inventory';
          itNonInventory:
            memItemListItemType.AsString := 'NonInventory';
          itPayment:
            memItemListItemType.AsString := 'Payment';
          itDiscount:
            memItemListItemType.AsString := 'Discount';
          itSalesTax:
            memItemListItemType.AsString := 'SalesTax';
          itSubtotal:
            memItemListItemType.AsString := 'Subtotal';
          itOtherCharge:
            memItemListItemType.AsString := 'OtherCharge';
          itInventoryAssembly:
            memItemListItemType.AsString := 'InventoryAssembly';
          itGroup:
            memItemListItemType.AsString := 'Group';
          itSalesTaxGroup:
            memItemListItemType.AsString := 'SalesTaxGroup';
          itFixedAsset:
            memItemListItemType.AsString := 'FixedAsset';
        end;
        memItemList.Post;
      end;

      // memItemList.SortedField := 'ItemName';
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memItemList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBIntergration.ItemNameList;
begin
  if not memItemList.Active then
    memItemList.Open;
  if memItemList.IsEmpty then
    ItemListFill;
  memItemList.First;
end;

procedure TQBIntergration.JournalEntryAddItem(const AAccount, AAccountType, AType, AName, AValue, AMemo: string);
var
  ACount: Integer;
  AAccountId: string;
begin
  QbLastError := '';
  { ExpenseAccountIDFind(AAccount)
    if ItemFind(AItemName) = '' then
    begin
    if QbLastError = '' then
    QbLastError := Format('Item "%s" not found in QB.', [AItemName]);
    raise EHandleInApp.Create(QbLastError);
    end; }
  AAccountId := AccountIDFind(AAccount, QbAccountType(AAccountType));
  if AAccountId = '' then
  begin
    QbLastError := Format('Account "%s" not found in QB.', [AAccount]);
    raise EHandleInApp.Create(QbLastError);
  end;
  ACount := iqbJournalEntry1.JournalLineCount;
  iqbJournalEntry1.JournalLineCount := ACount + 1;
  iqbJournalEntry1.JournalLineAccountId[ACount] := AAccountId;
  // iqbJournalEntry1.JournalLineAccountName[ACount] := AAccount;
  if SameText(AType, 'Debit') then
    iqbJournalEntry1.JournalLineType[ACount] := jltDebit
  else
    iqbJournalEntry1.JournalLineType[ACount] := jltCredit;
  iqbJournalEntry1.JournalLineAmount[ACount] := AValue;
  if AName <> '' then
    iqbJournalEntry1.JournalLineEntityName[ACount] := AName;
  if AMemo <> '' then
    iqbJournalEntry1.JournalLineMemo[ACount] := AMemo;
  // iqbSalesReceipt1.ItemTaxCodeName
end;

procedure TQBIntergration.JournalEntryAddItem(const AItemCat, AItemName: string; AValue: Currency; AMemo: string);
var
  ALineItem, AQbAccount, AQbAccountType, AType: string;
  ARec: TQBSalesSetting;
begin
  ARec := SalesSettingsA(AItemCat, AItemName);
  AQbAccount := ARec.QbAccount;
  AQbAccountType := ARec.QbAccountType;
  ALineItem := ARec.QbName;
  AType := ARec.QbType;
  if AValue < 0 then
  begin
    AValue := Abs(AValue);
    if SameText(AType, 'Debit') then
      AType := 'Credit'
    else
      AType := 'Debit';
  end;

  if (AQbAccount <> '') and (AValue <> 0) then
  begin
    if AMemo = '' then
      AMemo := AItemName;
    JournalEntryAddItem(AQbAccount, AQbAccountType, AType, ALineItem, CurrToStr(AValue), AMemo);
  end;
end;

function TQBIntergration.QbAccountType(const AAccountType: string): TiqbobjsearchAccountTypeEnum;
begin
  if AAccountType = '' then
    Result := atNotSet
  else
    try
      Result := TRttiEnumerationType.GetValue<TiqbobjsearchAccountTypeEnum>(AAccountType);
    except
      Result := atNotSet;
    end;
end;

procedure TQBIntergration.ResetQBSalesRecord(var ARecord: TQBSalesRecord);
begin
  with ARecord do
  begin
    LastRefID := '';
    LogList := '';
    Date := NullDate;
    NetSales := 0;
    TaxableSales := 0;
    NonTaxableSales := 0;
    TaxExemptSales := 0;
    Tax1Amount := 0;
    Tax2Amount := 0;
    COGS := 0;
    SetLength(Payments, 0);
    SetLength(DeptEntries, 0);
  end;
end;

procedure TQBIntergration.SalesReceiptAddItem(const AItemName, AItemAmount: string);
var
  ACount: Integer;
begin
  QbLastError := '';
  if ItemFind(AItemName) = '' then
  begin
    if QbLastError = '' then
      QbLastError := Format('Item "%s" not found in QB.', [AItemName]);
    raise EHandleInApp.Create(QbLastError);
  end;

  ACount := iqbSalesReceipt1.ItemCount;
  iqbSalesReceipt1.ItemCount := ACount + 1;
  iqbSalesReceipt1.ItemName[ACount] := AItemName;
  iqbSalesReceipt1.ItemAmount[ACount] := AItemAmount;
  iqbSalesReceipt1.ItemClassName[ACount] := SalesSettings('Info', 'ItemClassName');
  // iqbSalesReceipt1.ItemTaxCodeName
end;

function TQBIntergration.SalesJournalEntryAddUpdate(ARec: TQBSalesRecord): string;
var
  I: Integer;
  ACredit, ADebit: Currency;
begin
  iqbJournalEntry1.Reset;
  iqbJournalEntry1.QBRequestId := '';
  if ARec.LastRefID <> '' then
  begin
    try
      iqbJournalEntry1.Get(ARec.LastRefID);
      iqbJournalEntry1.Delete;
    except
    end;
    ARec.LastRefID := '';
    iqbJournalEntry1.Reset;
  end;
  try
    iqbJournalEntry1.TransactionDate := DateToStr(ARec.Date);
    // iqbJournalEntry1.RefNumber := '1';
    { iqbJournalEntry1.CustomerName := SalesSettings('Info', 'CustomerName');
      iqbJournalEntry1.TaxItemName := SalesSettings('Info', 'TaxItemName'); // 'None' }
    // iqbSalesReceipt1.DepositToAccountName;

    JournalEntryAddItem('Sales', 'NetSales', ARec.NetSales);
    JournalEntryAddItem('Sales', 'Taxable', ARec.TaxableSales);
    JournalEntryAddItem('Sales', 'NonTaxable', ARec.NonTaxableSales);
    JournalEntryAddItem('Sales', 'TaxExempt', ARec.TaxExemptSales);
    JournalEntryAddItem('Taxes', 'Tax1', ARec.Tax1Amount);
    JournalEntryAddItem('Taxes', 'Tax2', ARec.Tax2Amount);
    JournalEntryAddItem('COGS', 'COGS', ARec.COGS);
    JournalEntryAddItem('COGS', 'COGS Inevntory Asset', ARec.COGS);

    for I := 0 to Length(ARec.Payments) - 1 do
      JournalEntryAddItem('Payments', ARec.Payments[I].PayName, ARec.Payments[I].PayAmount);

    for I := 0 to Length(ARec.DeptEntries) - 1 do
      with ARec.DeptEntries[I] do
        JournalEntryAddItem('Departments', DeptName, Amount);

    ACredit := 0;
    ADebit := 0;
    for I := 0 to iqbJournalEntry1.JournalLineCount - 1 do
      if iqbJournalEntry1.JournalLineType[I] = jltDebit then
        ADebit := ADebit + StrToCurr(iqbJournalEntry1.JournalLineAmount[I])
      else
        ACredit := ACredit + StrToCurr(iqbJournalEntry1.JournalLineAmount[I]);

    if ACredit <> ADebit then
    begin
      I := iqbJournalEntry1.JournalLineCount;
      JournalEntryAddItem('Sales', 'Adjustment', ACredit - ADebit);
      if I = iqbJournalEntry1.JournalLineCount then // not added
        JournalEntryAddItem('Payments', ARec.Payments[0].PayName, ACredit - ADebit, 'Adjustment');
      if I = iqbJournalEntry1.JournalLineCount then // not added
        QbLastError := Format('Adjustment of %f not added to Journal (payment type %s)',
          [Abs(ACredit - ADebit), ARec.Payments[0].PayName]);
    end;

    if QbLastError = '' then
    begin
      ACredit := 0;
      ADebit := 0;
      for I := 0 to iqbJournalEntry1.JournalLineCount - 1 do
        if iqbJournalEntry1.JournalLineType[I] = jltDebit then
          ADebit := ADebit + StrToCurr(iqbJournalEntry1.JournalLineAmount[I])
        else
          ACredit := ACredit + StrToCurr(iqbJournalEntry1.JournalLineAmount[I]);
      if ACredit <> ADebit then
        QbLastError := Format('Discrepancy of %f found after adding adjustment', [Abs(ACredit - ADebit)]);
    end;

    if QbLastError <> '' then
      raise Exception.Create(QbLastError);

    iqbJournalEntry1.Add;
    Result := iqbJournalEntry1.RefId;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

procedure TQBIntergration.SalesReceiptAddItem(const AItemCat, AItemName: string; const AValue: Currency);
var
  ALineItem: string;
begin
  ALineItem := SalesSettings(AItemCat, AItemName);
  if (ALineItem <> '') and (AValue <> 0) then
    SalesReceiptAddItem(ALineItem, CurrToStr(AValue));
end;

function TQBIntergration.SalesReceiptAddUpdate(ARec: TQBSalesRecord): string;
var
  I: Integer;
begin
  iqbSalesReceipt1.Reset;
  iqbSalesReceipt1.QBRequestId := '';
  if ARec.LastRefID <> '' then
  begin
    try
      iqbSalesReceipt1.Get(ARec.LastRefID);
      iqbSalesReceipt1.Delete;
    except
    end;
    ARec.LastRefID := '';
    iqbSalesReceipt1.Reset;
  end;
  try
    iqbSalesReceipt1.Memo := ARec.LogList;
    iqbSalesReceipt1.TransactionDate := DateToStr(ARec.Date);
    iqbSalesReceipt1.CustomerName := SalesSettings('Info', 'CustomerName');
    iqbSalesReceipt1.TaxItemName := SalesSettings('Info', 'TaxItemName'); // 'None'
    // iqbSalesReceipt1.DepositToAccountName;

    SalesReceiptAddItem('Sales', 'NetSales', ARec.NetSales);
    SalesReceiptAddItem('Sales', 'Taxable', ARec.TaxableSales);
    SalesReceiptAddItem('Sales', 'NonTaxable', ARec.NonTaxableSales);
    SalesReceiptAddItem('Sales', 'TaxExempt', ARec.TaxExemptSales);
    SalesReceiptAddItem('Taxes', 'Tax1', ARec.Tax1Amount);
    SalesReceiptAddItem('Taxes', 'Tax2', ARec.Tax2Amount);

    for I := 0 to Length(ARec.Payments) - 1 do
      SalesReceiptAddItem('Payments', ARec.Payments[I].PayName, ARec.Payments[I].PayAmount);
    { if ARec.LastRefID <> '' then //couldn't get Update to work, that's why we Delete and Add it instead
      iqbSalesReceipt1.Update
      else }
    iqbSalesReceipt1.Add;
    Result := iqbSalesReceipt1.RefId;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBIntergration.SalesReceiptDelete(const ARefID: string): Boolean;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbSalesReceipt1.Reset;
      iqbSalesReceipt1.QBRequestId := '';
      iqbSalesReceipt1.Get(ARefID);
      Result := False;
      iqbSalesReceipt1.Delete;
      Result := True;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID])
        else
          QbLastError := E.Message;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.SalesSettings(const ACat, AName: string): string;
begin
  Result := '';
  SalesSettingsFill;
  if memSalesReceipt.Active and memSalesReceipt.Locate('Cat;Name', VarArrayOf([ACat, AName]), [loCaseInsensitive]) and
    (not memSalesReceiptInActive.AsBoolean) then
    Result := memSalesReceiptQbName.AsString;
end;

function TQBIntergration.SalesSettingsA(const ACat, AName: string): TQBSalesSetting;
begin
  // Result := '';
  SalesSettingsFill;
  if memSalesReceipt.Active and memSalesReceipt.Locate('Cat;Name', VarArrayOf([ACat, AName]), [loCaseInsensitive]) and
    (not memSalesReceiptInActive.AsBoolean) then
    with Result do
    begin
      QbAccount := memSalesReceiptQbAccount.AsString;
      QbAccountType := memSalesReceiptQbAccountType.AsString;
      QbName := memSalesReceiptQbName.AsString;
      QbType := memSalesReceiptQbType.AsString;
    end;
end;

procedure TQBIntergration.SalesSettingsAssert(const ACat, AName: string; var AIndex: Integer);
begin
  if not memSalesReceipt.Locate('Cat;Name', VarArrayOf([ACat, AName]), [loCaseInsensitive]) then
  begin
    Inc(AIndex);
    memSalesReceipt.Insert;
    memSalesReceiptCat.AsString := ACat;
    memSalesReceiptName.AsString := AName;
    memSalesReceiptSortColumn.AsInteger := AIndex;
    memSalesReceiptQbType.AsString := 'Debit';
    memSalesReceipt.Post;
  end;
end;

procedure TQBIntergration.SalesSettingsFill;
var
  I, Y: Integer;
//  APrefSetting: string;
  AXmlEle: IXMLNode;
  // ADataSet: TDataSet;
  AStrList: TStringList;
begin
  if memSalesReceipt.Active then
    Exit;

  // APrefSetting := ProgPref('QBSalesReceiptItems');
  if JournalEntryMode then
    XMLDocument.Xml.LoadFromFile(ExtractFilePath(ParamStr(0))+'JournalEntryModePrefs.xml')
  else
    XMLDocument.Xml.LoadFromFile(ExtractFilePath(ParamStr(0))+'SalesReceiptModePrefs.xml');

  {if APrefSetting = '' then
    APrefSetting := '<Data></Data>';}

  memSalesReceipt.Open;
  memSalesReceipt.First;
  while not memSalesReceipt.Eof do
    memSalesReceipt.Delete;
  //XMLDocument.Xml.Text := APrefSetting;
  XMLDocument.Active := True;
  AXmlEle := XMLDocument.DocumentElement;
  for I := 0 to AXmlEle.ChildNodes.Count - 1 do
  begin
    memSalesReceipt.Insert;
    memSalesReceiptCat.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['Category']);
    memSalesReceiptName.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['Name']);
    memSalesReceiptQbName.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbName']);
    memSalesReceiptQbAccount.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbAccount']);
    memSalesReceiptQbAccountType.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbAccountType']);
    memSalesReceiptQbType.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbType']);
    if memSalesReceiptQbType.AsString = '' then
      memSalesReceiptQbType.AsString := 'Debit';
    memSalesReceiptInActive.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['InActive']);
    memSalesReceiptSortColumn.AsInteger := I;
    memSalesReceipt.Post;
  end;
  SalesSettingsAssert('Info', 'CustomerName', I);
  SalesSettingsAssert('Info', 'TaxItemName', I);
  SalesSettingsAssert('Info', 'ItemClassName', I);
  SalesSettingsAssert('Sales', 'NetSales', I);
  SalesSettingsAssert('Sales', 'Taxable', I);
  SalesSettingsAssert('Sales', 'NonTaxable', I);
  SalesSettingsAssert('Sales', 'TaxExempt', I);
  SalesSettingsAssert('Sales', 'Adjustment', I);
  SalesSettingsAssert('Taxes', 'Tax1', I);
  SalesSettingsAssert('Taxes', 'Tax2', I);
  SalesSettingsAssert('Payments', 'PdAcnt', I);

  SalesSettingsAssert('Payments', 'Cash', I);
  SalesSettingsAssert('Payments', 'Check', I);
  SalesSettingsAssert('Payments', 'Crdt Card', I);
  SalesSettingsAssert('Payments', 'Store Crdt', I);
  SalesSettingsAssert('Payments', 'On Account', I);

  SalesSettingsAssert('COGS', 'COGS', I);
  SalesSettingsAssert('COGS', 'COGS Inevntory Asset', I);

  if JournalEntryMode then
  begin
    AStrList := TStringList.Create;
    try
      AStrList.Add('GROCERY');
      AStrList.Add('DAIRY');
      AStrList.Add('MEAT');
      SalesSettingsAssert('Departments', 'Dept: <Undefined>', I);
      for Y := 0 to AStrList.Count - 1 do
        SalesSettingsAssert('Departments', 'Dept: ' + AStrList[Y], I);
    finally
      AStrList.Free;
    end;
  end;
end;

procedure TQBIntergration.SalesSettingsMemToXML;
var
  AXmlEle: IXMLNode;
  ANewValue: string;
  AStream: TMemoryStream;
  AStrStream: TStringStream;
begin
  if not memSalesReceipt.Active then
    Exit;

  XMLDocument.Xml.Text := '<Data></Data>';
  XMLDocument.Active := True;

  memSalesReceipt.First;
  while not memSalesReceipt.Eof do
  begin
    if (memSalesReceiptQbName.AsString <> '') or (memSalesReceiptQbAccount.AsString <> '') then
    begin
      AXmlEle := XMLDocument.DocumentElement.AddChild('Item');
      AXmlEle.Attributes['Category'] := memSalesReceiptCat.AsString;
      AXmlEle.Attributes['Name'] := memSalesReceiptName.AsString;
      AXmlEle.Attributes['QbName'] := memSalesReceiptQbName.AsString;
      AXmlEle.Attributes['QbAccount'] := memSalesReceiptQbAccount.AsString;
      AXmlEle.Attributes['QbAccountType'] := memSalesReceiptQbAccountType.AsString;
      AXmlEle.Attributes['QbType'] := memSalesReceiptQbType.AsString;
      if memSalesReceiptInActive.AsBoolean then
        AXmlEle.Attributes['InActive'] := 'True';
    end;
    memSalesReceipt.Next;
  end;

  ANewValue := XMLDocument.Xml.Text;
  AStream := TMemoryStream.Create;
  AStrStream := TStringStream.Create(ANewValue);
  try
    AStrStream.Position := 0;
    AStrStream.SaveToStream(AStream);
    AStream.Position := 0;
    // GenHelpers.ProgPrefStore('QBSalesReceiptItems', GlobalSetName, AStream);
  finally
    AStream.Free;
    AStrStream.Free;
  end;

  memSalesReceipt.Close;
  SalesSettingsFill;
end;

function TQBIntergration.VendorCheckBalance(const AVendorName: string; var ABalance: Currency): Boolean;
begin
  Result := VendorFind(AVendorName);
  if Result then
    ABalance := StrToCurr(iqbVendor1.Balance);
end;

procedure TQBIntergration.VendorExpenseList(const AVendorName: string);
var
  AList: TStringList;
  I: Integer;
begin
  if not memExpenseList.Active then
    memExpenseList.Open;
  memExpenseList.First;
  if memExpenseList.IsEmpty then
    ExpenseListFill;

  memExpenseList.DisableControls;
  try
    memExpenseList.First;
    while not memExpenseList.Eof do
    begin
      if (memExpenseListFill.AsInteger > 0) and (memExpenseListFill.AsInteger < 99) then
      begin
        memExpenseList.Edit;
        memExpenseListFill.AsInteger := memExpenseListFillO.AsInteger;
        memExpenseList.Post;
      end;
      memExpenseList.Next
    end;

    if AVendorName <> '' then
    begin
      AList := TStringList.Create;
      try
        if VendorGetAccountPrefills(AVendorName, AList) then
          for I := 0 to AList.Count - 1 do
          begin
            if memExpenseList.Locate('AccountName', AList[I], [loCaseInsensitive]) then
            begin
              memExpenseList.Edit;
              memExpenseListFill.AsInteger := I + 1;
              memExpenseList.Post;
            end;
          end;
      finally
        AList.Free;
      end;
    end;
    memExpenseList.First;
  finally
    memExpenseList.EnableControls;
  end;
end;

function TQBIntergration.VendorFind(const AVendorName: string): Boolean;
  procedure ANameInList;
  var
    I: Integer;
  begin
    for I := 0 to iqbObjSearch1.ResultCount - 1 do
    begin
      iqbVendor1.Reset;
      iqbVendor1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[0];
      if SameText(iqbVendor1.VendorName, AVendorName) then
      begin
        Result := True;
        Break;
      end;
    end;
  end;

begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbObjSearch1.Reset;
      iqbObjSearch1.QueryType := qtVendorSearch;
      iqbObjSearch1.MaxResults := 1;

      iqbObjSearch1.NameStartsWith := AVendorName;
      iqbObjSearch1.NameEndsWith := AVendorName;
      iqbObjSearch1.Search;

      if iqbObjSearch1.ResultCount > 0 then
        ANameInList;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.VendorGetAccountPrefills(const AVendorName: string; var AList: TStringList): Boolean;
var
  AXmlEle, AXmlAcnt: TJvSimpleXmlElem;
  x, Y: Integer;
begin
  AList.Clear;
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbVendor1.Reset;
      iqbVendor1.QBRequestId := '';
      iqbVendor1.GetByName(AVendorName);
      showmessage(iqbVendor1.QBResponseAggregate);
      if Pos('<PrefillAccountRef>', iqbVendor1.QBResponseAggregate) > 0 then
      begin
        Xml1.LoadFromString(iqbVendor1.QBResponseAggregate);
        AXmlEle := Xml1.Root;
        for x := 0 to AXmlEle.Items.Count - 1 do
          if SameText(AXmlEle.Items[x].Name, 'PrefillAccountRef') then
          begin
            AXmlAcnt := AXmlEle.Items[x];
            for Y := 0 to AXmlAcnt.Items.Count - 1 do
              if SameText(AXmlAcnt.Items[Y].Name, 'FullName') then
              begin
                AList.Add(AXmlAcnt.Items[Y].Value);
                Break;
              end;
          end;
      end;
      Result := True;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBIntergration.VendorListFill;
var
  I: Integer;
begin
  if not memVendorList.Active then
    memVendorList.Open;
  memVendorList.First;
  while not memVendorList.Eof do
    memVendorList.Delete;

  // AppActions.ShowHourGlassCursor;
  memVendorList.DisableControls;
  try
    try
      iqbObjSearch1.Reset;
      iqbObjSearch1.QueryType := qtVendorSearch;
      iqbObjSearch1.Search();

      for I := 0 to iqbObjSearch1.ResultCount - 1 do
      begin
        iqbVendor1.Reset;
        iqbVendor1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[I];
        memVendorList.Insert;
        memVendorListVendorName.AsString := iqbVendor1.VendorName;
        memVendorList.Post;
      end;

      // memVendorList.SortedField := 'VendorName';
      I := 100;
      memVendorList.First;
      while not memVendorList.Eof do
      begin
        Inc(I);
        memVendorList.Edit;
        memVendorListFill.AsInteger := I;
        memVendorListFillO.AsInteger := I;
        memVendorList.Post;
        memVendorList.Next;
      end;
      // memVendorList.SortedField := 'Fill';
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memVendorList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.VendorListIdFromAccountPrefills(const AVendorName, AAccountName: string): string;
var
  AXmlEle, AXmlAcnt: TJvSimpleXmlElem;
  AListID: string;
  x, Y: Integer;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbVendor1.Reset;
      iqbVendor1.QBRequestId := '';
      iqbVendor1.GetByName(AVendorName);
      if Pos('<PrefillAccountRef>', iqbVendor1.QBResponseAggregate) > 0 then
      begin
        Xml1.LoadFromString(iqbVendor1.QBResponseAggregate);
        AXmlEle := Xml1.Root;
        for x := 0 to AXmlEle.Items.Count - 1 do
        begin
          AListID := '';
          if SameText(AXmlEle.Items[x].Name, 'PrefillAccountRef') then
          begin
            AXmlAcnt := AXmlEle.Items[x];
            for Y := 0 to AXmlAcnt.Items.Count - 1 do
              if SameText(AXmlAcnt.Items[Y].Name, 'ListID') then
                AListID := AXmlAcnt.Items[Y].Value
              else if SameText(AXmlAcnt.Items[Y].Name, 'FullName') and (AListID <> '') then
              begin
                Result := AListID;
                Break;
              end;
          end;
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBIntergration.VendorListIdFromInvoice(const AVendorName, AAccountName: string): string;
var
  x: Integer;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbObjSearch1.Reset;
      iqbObjSearch1.QueryType := qtBillSearch;
      iqbObjSearch1.MaxResults := 100;

      iqbObjSearch1.EntityName := AVendorName;
      iqbObjSearch1.Search;

      for x := 0 to iqbObjSearch1.ResultCount - 1 do
      begin
        iqbBill1.Reset;
        iqbBill1.QBResponseAggregate := iqbObjSearch1.ResultAggregate[x];
        if SameText(AAccountName, iqbBill1.ExpenseAccountName[0]) then
        begin
          Result := iqbBill1.ExpenseAccountId[0];
          Break;
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBIntergration.VendorNameList(const AVendorName: string);
begin
  if not memVendorList.Active then
    memVendorList.Open;
  memVendorList.First;
  if memVendorList.IsEmpty then
    VendorListFill;

  memVendorList.DisableControls;
  try
    memVendorList.First;
    while not memVendorList.Eof do
    begin
      if memVendorListFill.AsInteger < 99 then
      begin
        if memVendorListFillO.AsInteger = 1 then
        begin
          memVendorList.Delete;
          Continue;
        end
        else
          memVendorList.Edit;
        memVendorListFill.AsInteger := memVendorListFillO.AsInteger;
        memVendorList.Post;
      end;
      memVendorList.Next
    end;

    if memVendorList.Locate('VendorName', AVendorName, [loCaseInsensitive]) then
    begin
      memVendorList.Edit;
      memVendorListFill.AsInteger := 1;
      memVendorList.Post;
    end
    else
    begin
      memVendorList.Insert;
      memVendorListVendorName.AsString := AVendorName;
      memVendorListFill.AsInteger := 1;
      memVendorListFillO.AsInteger := 1;
      memVendorList.Post;
    end;
    memVendorList.First;
  finally
    memVendorList.EnableControls;
  end;
end;

function TQBIntergration.VendorQuickAdd(const AVendorName: string): Boolean;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      iqbVendor1.Reset;
      iqbVendor1.QBRequestId := '';
      iqbVendor1.VendorName := AVendorName;
      iqbVendor1.Add;
      Result := True;
    except
      on E: Exception do
        if SameText(E.Message, Format('Exception 899: The name "%s" of the list element is already deleted.',
          [AVendorName])) then
          QbLastError := Format('If vendor was made inactive in QuickBooks, ' + #13#10 +
            'please reactivate or rename the inactive vendor ' + #13#10 + 'and try again.', [AVendorName])
        else
          QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;


end.
