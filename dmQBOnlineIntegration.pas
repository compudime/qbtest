unit dmQBOnlineIntegration;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.Async, FireDAC.DApt, JvSimpleXml,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc;

  type
    EHandleInApp = class(Exception)
  end;

  TiqbobjsearchAccountTypes = (
    atUnspecified,
    atAccountsPayable,
    atAccountsReceivable,
    atBank,
    atCostOfGoodsSold,
    atCreditCard,
    atEquity,
    atExpense,
    atFixedAsset,
    atIncome,
    atLongTermLiability,
    atOtherAsset,
    atOtherCurrentAsset,
    atOtherCurrentLiability,
    atOtherExpense,
    atOtherIncome,
    atNonPosting
    );
  TQBOnlineSalesSetting = record
    QbAccount: string;
    QbAccountType: string;
    QbName: string;
    QbType: string;
  end;

  TQBOnlineSalesPayments = record
    PayName: string;
    PayAmount: Currency;
  end;

  TQBOnlineSalesDeptEntry = record
    DeptName: string;
    Amount: Currency;
  end;

  TQBOnlineSalesRecord = record
    LastRefID: string;
    LogList: string;
    Date: TDateTime;
    NetSales: Currency;
    TaxableSales: Currency;
    NonTaxableSales: Currency;
    TaxExemptSales: Currency;
    Tax1Amount: Currency;
    Tax2Amount: Currency;
    COGS: Currency;
    Payments: array of TQBOnlineSalesPayments;
    DeptEntries: array of TQBOnlineSalesDeptEntry;
  end;

  TQBOnlineIntegration = class(TDataModule)
    memExpenseList: TFDMemTable;
    memExpenseListAccountName: TStringField;
    memExpenseListFill: TIntegerField;
    memExpenseListAccountType: TStringField;
    memVendorList: TFDMemTable;
    memVendorListVendorName: TStringField;
    memExpenseListFillO: TIntegerField;
    memVendorListFill: TIntegerField;
    memVendorListFillO: TIntegerField;
    memCustomerList: TFDMemTable;
    memCustomerListCustomerName: TStringField;
    memItemList: TFDMemTable;
    memItemListItemName: TStringField;
    memItemListItemType: TStringField;
    memItemListItemAccount: TStringField;
    memClassList: TFDMemTable;
    memClassListClassName: TStringField;
    memCustomerVendorList: TFDMemTable;
    memCustomerVendorListName: TStringField;
    memAccountList: TFDMemTable;
    memAccountListAccountName: TStringField;
    memAccountListAccountType: TStringField;
    commonFDQ: TFDQuery;
    Xml1: TJvSimpleXML;
    commonFDQDetail: TFDQuery;
    XMLDocument: TXMLDocument;
    memSalesReceipt: TFDMemTable;
    memSalesReceiptCat: TStringField;
    memSalesReceiptName: TStringField;
    memSalesReceiptQbName: TStringField;
    memSalesReceiptQbAccount: TStringField;
    memSalesReceiptQbAccountType: TStringField;
    memSalesReceiptQbType: TStringField;
    memSalesReceiptInActive: TBooleanField;
    memSalesReceiptSortColumn: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);

  private
    FQbLastError: string;
    FExpenseAccount: string;
    FCompanyName: string;
    FConnectString: string;
    FAPAccount: string;
    FAPAccountID: string;
    FVendorItemMode: Boolean;
    FJournalEntryMode: Boolean;
   // function GetQBConnectionString: string;
    function LoadSQLIntoFDMem(ASql: string; AFDMem: TFDMemTable; AAppend: Boolean = False): integer;
    procedure ExpenseListFillByType(AFDMem: TFDMemTable; AAccountType: TiqbobjsearchAccountTypes; ACaption: string);
    function Bill_Credit_AddUpdate(AAction, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
      AAcountID, ARefID, AMemo: string): string;


  protected
    property  ExpenseAccount: string read FExpenseAccount;
    procedure ExpenseListFill;
    procedure VendorListFill;
    procedure CustomerListFill;
    procedure ItemListFill;
    procedure ClassListFill;

    //function QbAccountType(const AAccountType: string): TiqbobjsearchAccountTypeEnum;
    function ExpenseAccountIDFind(const AAccountName: string): string;
    function AccountIDFind(const AAccountName: string; AAccountType: string): string; overload;
    function AccountIDFind(const AAccountName: string): string; overload;
    function VendorListIdFromAccountPrefills(const AVendorName, AAccountName: string): string;
    function VendorListIdFromInvoice(const AVendorName, AAccountName: string): string;
    function ItemFind(const AItemName: string): string;

  public
    { Public declarations }
    property QbLastError: string read FQbLastError write FQbLastError;
    property JournalEntryMode: Boolean read FJournalEntryMode write FJournalEntryMode;
    property APAccount: string read FAPAccount;
    property APAccountID: string read FAPAccountID;
    property VendorItemMode: Boolean read FVendorItemMode write FVendorItemMode;

    function BillAdd(AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount, AMemo: string): string;
    function BillDelete(const ARefID: string): Boolean;
    function BillFind(const ADelNo: string): Boolean;
    function BillGetStatus(const ARefID: string; var AIsPaid: Boolean; var ABillAmount: Double;
      var ATransType: string): Boolean;
    function BillUpdate(ARefID, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
      AMemo: string): string;

    function VendorFind(const AVendorName: string; var ABalance: Currency): Boolean; overload;
    function VendorFind(const AVendorName: string; var APreFill: string): Boolean; overload;
    function VendorFind(const AVendorName: string): Boolean; overload;
    function VendorIDFind(const AVendorName: string): string;
    function VendorQuickAdd(const AVendorName: string): Boolean;
    function VendorGetAccountPrefills(const AVendorName: string; var AList: TStringList): Boolean; //not done
    procedure VendorExpenseList(const AVendorName: string); //not tested
    procedure VendorNameList(const AVendorName: string);

    function SalesReceiptAddUpdate(ARec: TQBOnlineSalesRecord): string;
    function SalesReceiptDelete(const ARefID: string): Boolean;
    function SalesJournalEntryAddUpdate(ARec: TQBOnlineSalesRecord): string;
    function JournalDelete(const ARefID: string): Boolean;

    procedure CustomerNameList;
    procedure ItemNameList;
    procedure ClassNameList;
    procedure CustomerVendorNameList;
    procedure AccountNameList;

    procedure SalesSettingsFill;
//    procedure SalesSettingsMemToXML; // not sure what is this
    function SalesSettings(const ACat, AName: string): string;
//    function SalesSettingsA(const ACat, AName: string): TQBSalesSetting;
    procedure SalesSettingsAssert(const ACat, AName: string; var AIndex: Integer);
    function GetTaxAgency(ATaxAgency: string) : string;
    function GetTaxItem(ATaxItem: string) : string;

    procedure ResetQBSalesRecord(var ARecord: TQBOnlineSalesRecord);
    function VendorCheckBalance(const AVendorName: string; var ABalance: Currency): Boolean;

  end;

function QBOnlineIntegration: TQBOnlineIntegration;
const
  QBConnectionStringA = 'ApplicationName="Total Inventory Solutions" ';
  Error817 = 'Error retreiving QB Expense Account Number, can''t add bill.' + #10#13 + 'Possible solutions:' + #10#13 +
    '1. Add Full Rights to Chart of Account for current user (QB Enterprise).' + #10#13 +
    '2. Add Financial Rights for current user (other QB versions).' + #10#13 +
    '3. Set the Expense Account for Vendor in POS and assign the account as an "Account Prefill" in QB.' + #10#13 +
    'QB Error: %s';

var
  __QBOnlineIntegration: TQBOnlineIntegration;

implementation
uses Forms, Dialogs, System.Variants, JclStreams, cxDateUtils, System.Rtti, cdQB;
{$R *.dfm}
{%CLASSGROUP 'Vcl.Controls.TControl'}

function QBOnlineIntegration: TQBOnlineIntegration;
begin
  if (__QBOnlineIntegration = nil) then
    __QBOnlineIntegration := TQBOnlineIntegration.Create(Application);

  Result := __QBOnlineIntegration;
end;

function TQBOnlineIntegration.LoadSQLIntoFDMem(ASql: string; AFDMem: TFDMemTable; AAppend: Boolean = False): integer;
begin
  Result := 0;
  try
    With commonFDQ do begin
      Close;
      SQL.Text       := ASQL;
      Open;
      AFDmem.Active := True;
      If not AAppend then begin
        AFDMem.EmptyDataSet;
        AFDMem.CopyDataSet(commonFDQ, [coAppend]);
      end else begin
        First;
        while not EOF do begin
          AFDMem.Insert;
          AFDMem.FieldByName('AccountName').AsString := FieldByName('AccountName').AsString;
          AFDMem.FieldByName('AccountType').AsString := FieldByName('AccountType').AsString;
          AFDMem.Post;
          Next;
        end;
      end;
      Result := AFDMem.RecordCount;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

(*
function TQBOnlineIntegration.GetQBConnectionString: string;
begin
  Result := QBConnectionStringA;
  if FConnectString <> '' then
    Result := FConnectString + ' ' + Result;
  if FCompanyName <> '' then
    Result := Result + Format('CompanyFile="%s" ', [FCompanyName]);
end;
*)
procedure TQBOnlineIntegration.DataModuleCreate(Sender: TObject);
begin
  FExpenseAccount := ''; // ProgPref('QbBillExpenseAccount');
  FAPAccount := ''; // ProgPref('QbBillAPAccount');
  FCompanyName := ''; // ProgPref('QbBillCompanyName');
  FConnectString := ''; // ProgPref('QbConnectString');
  FVendorItemMode := False; // SameText(ProgPref('QbVendExpenseItemMode'), 'Items');
  FJournalEntryMode := False; // SameText(ProgPref('QbSalesExportMode'), 'Journal Entry');
  FAPAccountID := '';

end;

procedure TQBOnlineIntegration.ExpenseListFillByType(AFDmem: TFDMemTable; AAccountType: TiqbobjsearchAccountTypes;
  ACaption: string);
var
  sSQL : string;
begin
  sSQL := 'SELECT Name AccountName, AccountType FROM Accounts WHERE AccountType = ' + QuotedStr(ACaption) + ' ORDER BY Name';
  LoadSQLIntoFDMem(sSql, AFDMem, True);
end;

procedure TQBOnlineIntegration.ItemNameList;
begin
  if not memItemList.Active then
    memItemList.Open;
  if memItemList.IsEmpty then
    ItemListFill;
  memItemList.First;
end;

procedure TQBOnlineIntegration.ItemListFill;
var
  sSQL : string;
begin
  if not memItemList.Active then
    memItemList.Open;
  memItemList.DisableControls;
  memItemList.EmptyDataset;
  try
    try
      // this works in Online !!sSQL := 'SELECT Name ItemName, Type ItemType, IncomeAccountRef_name FROM Items  ORDER BY ItemName';
      sSQL := 'SELECT Name ItemName, Type ItemType, IncomeAccountRef_name FROM Items  ORDER BY Name';
      LoadSQLIntoFDMem(sSql, memItemList, False);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memItemList.EnableControls;
  end;
end;


procedure TQBOnlineIntegration.CustomerListFill;
var
  sSQL : string;
begin
  if not memCustomerList.Active then
    memCustomerList.Open;
  memCustomerList.DisableControls;
  memCustomerList.EmptyDataset;


  // AppActions.ShowHourGlassCursor;
  memCustomerList.DisableControls;
  try
    try
      sSQL := 'SELECT DisplayName CustomerName FROM Customers  ORDER BY DisplayName';
      LoadSQLIntoFDMem(sSql, memCustomerList, False);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memCustomerList.EnableControls;
  end;
end;

procedure TQBOnlineIntegration.CustomerNameList;
begin
  if not memCustomerList.Active then
    memCustomerList.Open;
  if memCustomerList.IsEmpty then
    CustomerListFill;
  memCustomerList.First;
end;

procedure TQBOnlineIntegration.ClassNameList;
begin
  if not memClassList.Active then
    memClassList.Open;
  if memClassList.IsEmpty then
    ClassListFill;
  memClassList.First;
end;

procedure TQBOnlineIntegration.VendorExpenseList(const AVendorName: string);
var
  AList: TStringList;
  I: Integer;
begin
  if not memExpenseList.Active then
    memExpenseList.Open;
  memExpenseList.First;
  if memExpenseList.IsEmpty then
    ExpenseListFill;

  memExpenseList.DisableControls;
  try
    memExpenseList.First;
    while not memExpenseList.Eof do
    begin
      if (memExpenseListFill.AsInteger > 0) and (memExpenseListFill.AsInteger < 99) then
      begin
        memExpenseList.Edit;
        memExpenseListFill.AsInteger := memExpenseListFillO.AsInteger;
        memExpenseList.Post;
      end;
      memExpenseList.Next
    end;

    if AVendorName <> '' then
    begin
      AList := TStringList.Create;
      try
//        if VendorGetAccountPrefills(AVendorName, AList) then
          for I := 0 to AList.Count - 1 do
          begin
            if memExpenseList.Locate('AccountName', AList[I], [loCaseInsensitive]) then
            begin
              memExpenseList.Edit;
              memExpenseListFill.AsInteger := I + 1;
              memExpenseList.Post;
            end;
          end;
      finally
        AList.Free;
      end;
    end;
    memExpenseList.First;
  finally
    memExpenseList.EnableControls;
  end;
end;

procedure TQBOnlineIntegration.ClassListFill;
var
  sSQL : string;
begin
  if not memClassList.Active then
    memClassList.Open;
  memClassList.DisableControls;
  memClassList.EmptyDataset;

  // AppActions.ShowHourGlassCursor;
  memClassList.DisableControls;
  try
    try
      sSQL := 'SELECT Name ClassName FROM Class  ORDER BY Name';
      LoadSQLIntoFDMem(sSql, memClassList, False);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memClassList.EnableControls;
  end;
end;

procedure TQBOnlineIntegration.ExpenseListFill;
var
  I: Integer;
begin
  if not memExpenseList.Active then
    memExpenseList.Open;
  memExpenseList.EmptyDataset;

  // AppActions.ShowHourGlassCursor;
  memExpenseList.DisableControls;
  try
    try
      ExpenseListFillByType(memExpenseList, atExpense, 'Expense');
//does not work?      ExpenseListFillByType(memExpenseList, atCostOfGoodsSold, 'Cost Of Goods Sold');

      // memExpenseList.SortedField := 'AccountName';
      I := 100;
      memExpenseList.First;
      while not memExpenseList.Eof do
      begin
        Inc(I);
        memExpenseList.Edit;
        memExpenseListFill.AsInteger := I;
        memExpenseListFillO.AsInteger := I;
        memExpenseList.Post;
        memExpenseList.Next;
      end;
      // memExpenseList.SortedField := 'Fill';

      if ExpenseAccount <> '' then
      begin
        memExpenseList.Insert;
        memExpenseListAccountName.AsString := '<DEFAULT>';
        memExpenseListAccountType.AsString := '(' + ExpenseAccount + ')';
        memExpenseListFill.AsInteger := 0;
        memExpenseList.Post;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memExpenseList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBOnlineIntegration.VendorListFill;
var
  I: Integer;
  sSQL : string;
begin
  if not memVendorList.Active then
    memVendorList.Open;
  memVendorList.DisableControls;
  memVendorList.EmptyDataset;

  // AppActions.ShowHourGlassCursor;
  memVendorList.DisableControls;
  try
    try
      sSQL := 'SELECT DisplayName VendorName, 0 Fill, 0 FillO FROM Vendors  ORDER BY DisplayName';
      LoadSQLIntoFDMem(sSql, memVendorList, False);


      // memVendorList.SortedField := 'VendorName';
      I := 100;
      memVendorList.First;
      while not memVendorList.Eof do
      begin
        Inc(I);
        memVendorList.Edit;
        memVendorListFill.AsInteger := I;
        memVendorListFillO.AsInteger := I;
        memVendorList.Post;
        memVendorList.Next;
      end;
      // memVendorList.SortedField := 'Fill';
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memVendorList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBOnlineIntegration.CustomerVendorNameList;
begin
  if not memCustomerVendorList.Active then
    memCustomerVendorList.Open;
  if not memCustomerVendorList.IsEmpty then
    Exit;

  if not memCustomerList.Active then
    memCustomerList.Open;
  if memCustomerList.IsEmpty then
    CustomerListFill;;

  if not memVendorList.Active then
    memVendorList.Open;
  if memVendorList.IsEmpty then
    VendorListFill;

  memVendorList.First;
  while not memVendorList.Eof do
  begin
    memCustomerVendorList.Insert;
    memCustomerVendorListName.AsString := memVendorListVendorName.AsString;
    memCustomerVendorList.Post;
    memVendorList.Next;
  end;

  memVendorList.First;
  while not memCustomerList.Eof do
  begin
    memCustomerVendorList.Insert;
    memCustomerVendorListName.AsString := memCustomerListCustomerName.AsString;
    memCustomerVendorList.Post;
    memCustomerList.Next;
  end;

  memCustomerVendorList.First;
end;

procedure TQBOnlineIntegration.AccountNameList;
begin
  if not memAccountList.Active then
    memAccountList.Open;
(*
     ,
     Cost Of Goods Sold,
     *)
  if memAccountList.IsEmpty then
  begin
    // second parameter is dummy
    ExpenseListFillByType(memAccountList, atExpense, 'Expense');
    ExpenseListFillByType(memAccountList, atAccountsReceivable, 'Accounts Receivable');
    ExpenseListFillByType(memAccountList, atIncome, 'Income');
    ExpenseListFillByType(memAccountList, atOtherAsset, 'Other Asset');
    ExpenseListFillByType(memAccountList, atOtherExpense, 'Other Expense');
    ExpenseListFillByType(memAccountList, atOtherIncome, 'Other Income');
    ExpenseListFillByType(memAccountList, atOtherCurrentAsset, 'Other Current Asset');
    ExpenseListFillByType(memAccountList, atOtherCurrentLiability, 'Other Current Liability');
    ExpenseListFillByType(memAccountList, atBank, 'Bank');
    ExpenseListFillByType(memAccountList, atBank, 'Equity');
    ExpenseListFillByType(memAccountList, atBank, 'Accounts Payable');
    ExpenseListFillByType(memAccountList, atBank, 'Fixed Asset');
    ExpenseListFillByType(memAccountList, atBank, 'Credit Card');
    ExpenseListFillByType(memAccountList, atBank, 'Long Term Liability');
  //does not work? gives accounType not found  ExpenseListFillByType(memAccountList, atBank, 'Cost of Goods Sold');
  end;
end;

function TQBOnlineIntegration.VendorFind(const AVendorName: string; var ABalance: Currency): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT DisplayName, Balance FROM Vendors WHERE Upper(DisplayName) LIKE '
              + QuotedStr('%'  + AVendorName.ToUpper + '%') + ' ORDER BY DisplayName';

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        Result := Locate('DisplayName', AVendorName, [loCaseInsensitive]);
        If Result then ABalance := FieldByName('Balance').AsCurrency;
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.VendorFind(const AVendorName: string; var APreFill: String): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT DisplayName, PrefillAccountRef FROM Vendors WHERE Upper(DisplayName) LIKE '
              + QuotedStr('%'  + AVendorName.ToUpper + '%') + ' ORDER BY DisplayName';

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        Result := Locate('DisplayName', AVendorName, [loCaseInsensitive]);
        If Result then APreFill := FieldByName('PrefillAccountRef').AsString;
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.VendorFind(const AVendorName: string): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT DisplayName FROM Vendors WHERE Upper(DisplayName) LIKE '
              + QuotedStr('%'  + AVendorName.ToUpper + '%') + ' ORDER BY DisplayName';

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        Result := Locate('DisplayName', AVendorName, [loCaseInsensitive]);
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.VendorIDFind(const AVendorName: string): string;
var
  sSQL : string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT ID FROM Vendors WHERE Upper(DisplayName) = '
              + QuotedStr(AVendorName.ToUpper);

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        if RecordCount > 0 then Result := FieldByName('ID').AsString;
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBOnlineIntegration.VendorNameList(const AVendorName: string);
begin
  if not memVendorList.Active then
    memVendorList.Open;
  memVendorList.First;
  if memVendorList.IsEmpty then
    VendorListFill;

  memVendorList.DisableControls;
  try
    memVendorList.First;
    while not memVendorList.Eof do
    begin
      if memVendorListFill.AsInteger < 99 then
      begin
        if memVendorListFillO.AsInteger = 1 then
        begin
          memVendorList.Delete;
          Continue;
        end
        else
          memVendorList.Edit;
        memVendorListFill.AsInteger := memVendorListFillO.AsInteger;
        memVendorList.Post;
      end;
      memVendorList.Next;
    end;

    if memVendorList.Locate('VendorName', AVendorName, [loCaseInsensitive]) then
    begin
      memVendorList.Edit;
      memVendorListFill.AsInteger := 1;
      memVendorList.Post;
    end
    else
    begin
      memVendorList.Insert;
      memVendorListVendorName.AsString := AVendorName;
      memVendorListFill.AsInteger := 1;
      memVendorListFillO.AsInteger := 1;
      memVendorList.Post;
    end;
    memVendorList.First;
  finally
    memVendorList.EnableControls;
  end;
end;


function TQBOnlineIntegration.VendorCheckBalance(const AVendorName: string; var ABalance: Currency): Boolean;
begin
  Result := VendorFind(AVendorName, ABalance);
end;


function TQBOnlineIntegration.VendorGetAccountPrefills(const AVendorName: string; var AList: TStringList): Boolean;
var
  AXmlEle, AXmlAcnt: TJvSimpleXmlElem;
  x, Y: Integer;
  APreFill : string;
begin
  AList.Clear;
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      if VendorFind(AVendorName, APrefill) then begin
        if Pos('<PrefillAccountRef>', APrefill) > 0 then
        begin
          Xml1.LoadFromString(APrefill);
          AXmlEle := Xml1.Root;
          for x := 0 to AXmlEle.Items.Count - 1 do
            if SameText(AXmlEle.Items[x].Name, 'PrefillAccountRef') then
            begin
              AXmlAcnt := AXmlEle.Items[x];
              for Y := 0 to AXmlAcnt.Items.Count - 1 do
                if SameText(AXmlAcnt.Items[Y].Name, 'FullName') then
                begin
                  AList.Add(AXmlAcnt.Items[Y].Value);
                  Break;
                end;
            end;
        end;
      end;
      Result := True;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.VendorQuickAdd(const AVendorName: string): Boolean;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      With commonFDQ do begin
        Close;
        SQL.Text := 'INSERT INTO Vendors (DisplayName) VALUES (' + QuotedStr(AVendorName) + ')' ;
        ExecSQL;
      end;
      Result := True;
    except
      on E: Exception do
        if SameText(E.Message, Format('Exception 899: The name "%s" of the list element is already deleted.',
          [AVendorName])) then
          QbLastError := Format('If vendor was made inactive in QuickBooks, ' + #13#10 +
            'please reactivate or rename the inactive vendor ' + #13#10 + 'and try again.', [AVendorName])
        else
          QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.BillFind(const ADelNo: string): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT TOP 1 DocNumber FROM Bills WHERE Upper(DocNumber) = ' + QuotedStr(ADelNo) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        Open;
        Result := (RecordCount = 1);
      end;
      if not Result then
      begin
        sSQL := 'SELECT TOP 1 DocNumber FROM VendorCredits WHERE Upper(DocNumber) = ' + QuotedStr(ADelNo) ;
        With commonFDQ do begin
          Close;
          SQL.Text := sSQL;
          Open;
          Result := (RecordCount = 1);
        end;
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ADelNo])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ADelNo])
        else
          QbLastError := E.Message;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.BillGetStatus(const ARefID: string; var AIsPaid: Boolean; var ABillAmount: Double;
  var ATransType: string): Boolean;
var
  ASearchCredit: Boolean;
  sSQL : string;
begin
  Result := False;
  ASearchCredit := False;
  ATransType := '';
  try
    try
      sSQL := 'SELECT DocNumber, TotalAmt, Balance FROM Bills WHERE Upper(DocNumber) = ' + QuotedStr(ARefID) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        Open;
        Result := (RecordCount = 1);
        AIsPaid := FieldByName('Balance').AsFloat = 0;
        ABillAmount := FieldByName('TotalAmt').AsFloat;
        ATransType := 'Bill';
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
        begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
          ASearchCredit := True;
        end
        else
          QbLastError := E.Message;
      end;
    end;
    try
      if (not Result) and ASearchCredit then
      begin
        sSQL := 'SELECT TotalAmt FROM VendorCredits WHERE Upper(DocNumber) = ' + QuotedStr(ARefID) ;
        With commonFDQ do begin
          Close;
          SQL.Text := sSQL;
          Open;
          Result := (RecordCount = 1);
          AIsPaid := True;
          ABillAmount := FieldByName('TotalAmt').AsFloat;
          ATransType := 'Credit';
        end;
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID])
        else
          QbLastError := E.Message;
      end;
   end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.BillDelete(const ARefID: string): Boolean;
var
  ASearchCredit: Boolean;
  sSQL : string;
begin
  Result := False;
  ASearchCredit := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'DELETE FROM Bills WHERE Upper(DocNumber) = ' + QuotedStr(ARefID) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        ExecSQL;
        Result := (RowsAffected = 1);
        if not Result then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
        begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
          ASearchCredit := True;
        end
        else
          QbLastError := E.Message;
      end;
    end;
    if (not Result) and ASearchCredit then
    begin
      try
      sSQL := 'DELETE FROM VendorCredits WHERE Upper(DocNumber) = ' + QuotedStr(ARefID) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        ExecSQL;
        Result := (RowsAffected = 1);
        if not Result then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
      end;
      except
        on E: Exception do
        begin
          if SameText(E.Message,
            Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
            [ARefID])) then
            // Do nothing
          else
            QbLastError := E.Message;
        end;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.BillUpdate(ARefID, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
  AMemo: string): string;
var
  AAcountID: string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      if AVendorName = '' then
        raise EHandleInApp.Create('QB Vendor Name not set, can''t update bill.');
      if SameText(AExpenseAccount, '<DEFAULT>') then
        AExpenseAccount := ExpenseAccount;
      if AExpenseAccount = '' then
        raise EHandleInApp.Create('QB Expense Account not set, can''t update bill.');
      AAcountID := ExpenseAccountIDFind(AExpenseAccount);
      if (AAcountID = '') and VendorItemMode then
        AAcountID := ItemFind(AExpenseAccount);
      if (AAcountID = '') and (Copy(QbLastError, 1, 3) = '817') then
      begin
        AAcountID := VendorListIdFromAccountPrefills(AVendorName, AExpenseAccount);
        if AAcountID = '' then
          AAcountID := VendorListIdFromInvoice(AVendorName, AExpenseAccount);
      end;

      if AAcountID = '' then
      begin
        if Copy(QbLastError, 1, 3) = '817' then
          raise EHandleInApp.CreateFmt(Error817, [QbLastError])
        else if QbLastError <> '' then
          raise EHandleInApp.CreateFmt('Error retrieving QB Expense Account Number, can''t update bill.' + #10#13 +
            'QB Error: %s', [QbLastError])
        else
          raise EHandleInApp.CreateFmt('QB Expense Account Number not found for Account %s, can''t update bill.',
            [AExpenseAccount]);
      end;
      Result := Bill_Credit_AddUpdate('Update', AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount,
        AExpenseAccount, AAcountID, ARefID, AMemo);
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ADelNo])
        else
          QbLastError := E.Message;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.ExpenseAccountIDFind(const AAccountName: string): string;
begin
  Result := AccountIDFind(AAccountName, 'Cost of Goods Sold');
  if Result = '' then
    Result := AccountIDFind(AAccountName, 'Expense');
end;

function TQBOnlineIntegration.AccountIDFind(const AAccountName: string; AAccountType: string): string;
begin
  Result := '';
  try
    With CommonFDQ do begin
      Close;
      SQL.Clear;
      SQL.Text := 'SELECT ID FROM Accounts WHERE Upper(Name) = ' + QuotedStr(AAccountName.ToUpper) +
                  ' AND Upper(accountType) = ' + QuotedStr(AAccountType.ToUpper) ;
      Open;
      if RecordCount = 0 then exit;
      Result := FieldByName('ID').AsString;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBOnlineIntegration.AccountIDFind(const AAccountName: string): string;
begin
  Result := '';
  try
    With CommonFDQ do begin
      Close;
      SQL.Clear;
      SQL.Text := 'SELECT ID FROM Accounts WHERE Upper(Name) = ' + QuotedStr(AAccountName.ToUpper);
      Open;
      if RecordCount = 0 then exit;
      Result := FieldByName('ID').AsString;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBOnlineIntegration.ItemFind(const AItemName: string): string;
begin
  Result := '';
  try
    With CommonFDQ do begin
      Close;
      SQL.Clear;
      SQL.Text := 'SELECT ID FROM Items WHERE Upper(Name) = ' + QuotedStr(AItemName.ToUpper) ;
      Open;
      if RecordCount = 0 then exit;
      Result := FieldByName('ID').AsString;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBOnlineIntegration.VendorListIdFromAccountPrefills(const AVendorName, AAccountName: string): string;
var
  AXmlEle, AXmlAcnt: TJvSimpleXmlElem;
  AListID: string;
  x, Y: Integer;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT PrefillAccountRef  FROM Vendors WHERE Upper(DisplayName) = ' + QuotedStr(AVendorName.ToUpper) ;
        Open;
        if RecordCount = 0 then exit;
        if FieldByName('PrefillAccountRef').AsString = '' then exit;
        Xml1.LoadFromString(FieldByName('PrefillAccountRef').AsString);
        AXmlEle := Xml1.Root;
        for x := 0 to AXmlEle.Items.Count - 1 do
        begin
          AListID := '';
          if SameText(AXmlEle.Items[x].Name, 'PrefillAccountRef') then
          begin
            AXmlAcnt := AXmlEle.Items[x];
            for Y := 0 to AXmlAcnt.Items.Count - 1 do
              if SameText(AXmlAcnt.Items[Y].Name, 'ListID') then
                AListID := AXmlAcnt.Items[Y].Value
              else if SameText(AXmlAcnt.Items[Y].Name, 'FullName') and (AListID <> '') then
              begin
                Result := AListID;
                Break;
              end;
          end;
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.VendorListIdFromInvoice(const AVendorName, AAccountName: string): string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  //vendorref_name, apaccountref_name
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT a.apAccountRef  FROM Bills WHERE Upper(vendorref_name) = ' + QuotedStr(AVendorName.ToUpper);
        Open;
        if RecordCount = 0 then exit;
        Result := FieldByName('apAccountRef').AsString;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.Bill_Credit_AddUpdate(AAction, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount,
  AExpenseAccount, AAcountID, ARefID, AMemo: string): string;
var sSQL, sSQLDetail : string;
    vendorId : string;
    BillID : string;
    ExpAccountid : string;
    sDetailXML: string;
begin
  if (APAccount <> '') and (APAccountID = '') then
  begin
    FAPAccountID := AccountIDFind(APAccount, 'Accounts Payable');
    if APAccountID = '' then
      FAPAccount := '';
  end;
  ExpAccountid := ExpenseAccountIDFind(AExpenseAccount);
  vendorId := VendorIdFind(AVendorName);
  if StrToFloat(AAmount) < 0 then
  begin
    With CommonFDQ do begin
      if ((ARefID <> '') and  (SameText(AAction, 'Update'))) then
      begin
        sSQL := 'UPDATE VendorCredits SET  '  +
                                 ' PrivateNote = '      + QuotedStr(AMemo)  +
                                 ',TxnDate     = '      + QuotedStr(ADate)  +
                                 ',TotalAmt    = '      + QuotedStr(AAmount);
        If ApAccountId <> '' then
          sSQL := sSQL + ',APAccountRef = '  +  QuotedStr(APAccountID);
        sSQL := sSQL + ' WHERE DocNumber = ' + QuotedStr(AVendInvNo);
      end
      else if SameText(AAction, 'Add') then begin
          sSQL := 'INSERT INTO VendorCreditLineItems (VendorRef, PrivateNote, DocNumber, TxnDate, Line_Amount,Line_id,line_detailtype, Line_AccountBasedExpenseLineDetail_accountref) VALUES ('
               +  QuotedStr(VendorId) + ',' + QuotedStr(AMemo) + ', ' + QuotedStr(AVendInvNo) + ',' + QuotedStr(ADate) + ', '
               +  FloatToStr(Abs(StrToFloat(AAmount)))  +  ', ' +  '1,' + QuotedStr('AccountBasedExpenseLineDetail') + ',' + ExpAccountid + ')';

      end;
      Close;
      SQL.text := sSQL;
      ExecSQL;
      sSQL := 'SELECT * FROM VendorCredits WHERE Upper(DocNumber) = ' + UpperCase(QuotedStr(AVendInvNo));
      Close;
      SQL.Clear;
      SQL.text := sSQL;
      Open;
      if not EOF then BillID := FieldByName('ID').AsString;
    end;
  end
  else
  begin
    With CommonFDQ do begin
      if ((ARefID <> '') and  (SameText(AAction, 'Update'))) then
      begin
        sSQL := 'UPDATE Bills SET  '  +
                                 ' PrivateNote = '      + QuotedStr(AMemo)  +
                                 ',TxnDate     = '      + QuotedStr(ADate)  +
                                 ',TotalAmt    = '      + QuotedStr(AAmount);
        If ApAccountId <> '' then
          sSQL := sSQL + ',APAccountRef = '  +  QuotedStr(APAccountID);
        sSQL := sSQL + ' WHERE DocNumber = ' + QuotedStr(AVendInvNo);
      end
      else if SameText(AAction, 'Add') then begin
          sSQL := 'INSERT INTO BillLineItems (VendorRef, PrivateNote, DocNumber, TxnDate, Line_Amount,Line_id,line_detailtype, Line_AccountBasedExpenseLineDetail_accountref) VALUES ('
               +  QuotedStr(VendorId) + ',' + QuotedStr(AMemo) + ', ' + QuotedStr(AVendInvNo) + ',' + QuotedStr(ADate) + ', '  + AAmount
               +  ', ' +  '1,' + QuotedStr('AccountBasedExpenseLineDetail') + ',' + ExpAccountid + ')';

      end;
      Close;
      SQL.text := sSQL;
      ExecSQL;
      sSQL := 'SELECT * FROM Bills WHERE Upper(DocNumber) = ' + UpperCase(QuotedStr(AVendInvNo));
      Close;
      SQL.Clear;
      SQL.text := sSQL;
      Open;
      If not EOF then BillID := FieldByName('ID').AsString;
    end;
  end;
  Result := BillID;
end;

function TQBOnlineIntegration.BillAdd(AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
  AMemo: string): string;
var
  AAcountID: string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      if AVendorName = '' then
        raise EHandleInApp.Create('QB Vendor Name not set, can''t add bill.');
      if SameText(AExpenseAccount, '<DEFAULT>') then
        AExpenseAccount := ExpenseAccount;
      if AExpenseAccount = '' then
        raise EHandleInApp.Create('QB Expense Account not set, can''t add bill.');

      AAcountID := ExpenseAccountIDFind(AExpenseAccount);

      if (AAcountID = '') and VendorItemMode then
        AAcountID := ItemFind(AExpenseAccount);

      if (AAcountID = '') and (Copy(QbLastError, 1, 3) = '817') then
      begin
        AAcountID := VendorListIdFromAccountPrefills(AVendorName, AExpenseAccount);
        if AAcountID = '' then
          AAcountID := VendorListIdFromInvoice(AVendorName, AExpenseAccount);
      end;

      if AAcountID = '' then
      begin
        if Copy(QbLastError, 1, 3) = '817' then
          raise EHandleInApp.CreateFmt(Error817, [QbLastError])
        else if QbLastError <> '' then
          raise EHandleInApp.CreateFmt('Error retreiving QB Expense Account Number, can''t add bill.' + #10#13 +
            'QB Error: %s', [QbLastError])
        else
          raise EHandleInApp.CreateFmt('QB Expense Account Number not found for Account %s, can''t add bill.',
            [AExpenseAccount]);
      end;
      Result := Bill_Credit_AddUpdate('Add', AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
        AAcountID, '', AMemo);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBOnlineIntegration.SalesReceiptDelete(const ARefID: string): Boolean;
begin
  Result := False;
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT ID FROM SalesReceipts WHERE Upper(DocNumber) = ' + QuotedStr(UpperCase(ARefId));
        Open;
        result :=  (RecordCount <> 0);
        if result then begin
          SQL.Text := 'DELETE FROM SalesReceipts WHERE ID = ' + FieldByName('ID').AsString;
          ExecSQL;
        end else begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;

end;

function TQBOnlineIntegration.GetTaxAgency(ATaxAgency: string) : string;
var
  TaxId : string;
begin
  TaxId := '';
  With commonFDQ do begin
    Close;
    SQL.Text := 'SELECT ID FROM TaxAgency WHERE Upper(DisplayName) = ' + QuotedStr(UpperCase(ATaxAgency));
    Open;
    if not EOF then TaxId := FieldByName('ID').AsString;
    Close;
  end;
  Result := TaxId;
end;

function TQBOnlineIntegration.GetTaxItem(ATaxItem: string) : string;
var
  TaxId : string;
begin
  TaxId := '';
  With commonFDQ do begin
    Close;
    SQL.Text := 'SELECT ID FROM SalesTaxItems WHERE Upper(Name) = ' + QuotedStr(UpperCase(ATaxItem));
    Open;
    if not EOF then TaxId := FieldByName('ID').AsString;
    Close;
  end;
  Result := TaxId;
end;


function TQBOnlineIntegration.SalesReceiptAddUpdate(ARec: TQBOnlineSalesRecord): string;
var
  CustomerId : string;
  TaxId      : string;
  LastSRId   : string;
  ItemId     : string;
  ItemId1    : string;
begin
  if ARec.LastRefID <> '' then
  begin
    try
      SalesReceiptDelete(Arec.LastRefID);
    except
    end;
  end;
  With commonFDQ do begin
    Close;
    SQL.Text := 'SELECT ID FROM Customers WHERE Upper(DisplayName) = ' + QuotedStr(UpperCase(SalesSettings('Info', 'CustomerName1')));
    Open;
    if not EOF then CustomerID := FieldByName('ID').AsString;
    Close;
  end;
  TaxId := GetTaxAgency('Arizona Dept. of Revenue');
  try
    With commonFDQ do begin
      ItemId := ItemFind('Trimming');
      ItemId1 := ItemFind('Soil');
      // this syntax allows you to add as many line items with a #<n>
      Close;
      SQL.Text := 'INSERT INTO SalesReceiptLineItems (CustomerMemo, TxnDate, CustomerRef, TxnTaxDetail_TxnTaxCodeRef, Line_DetailType,' +
                  'Line_SalesItemLineDetail_ItemRef,Line_Amount)  '      +
                  'VALUES ( ' + QuotedStr(ARec.LogList) + ', ' +  QuotedStr(DateToStr(ARec.Date)) + ',' +
                  QuotedStr(CustomerId) + ',' + QuotedStr(TaxId) + ',' +  QuotedStr('SalesItemLineDetail') + ',' +
                  QuotedStr(ItemId) + ','  + FloatToStr(ARec.NetSales) + ')';
      ExecSQL;
      Close;
      SQL.Text := 'SELECT  ID FROM SalesReceipts WHERE CustomerRef = ' + QuotedStr(CustomerId);
      Open;
      if EOF then exit else Result := FieldByName('ID').AsString;
      Close;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

procedure TQBOnlineIntegration.ResetQBSalesRecord(var ARecord: TQBOnlineSalesRecord);
begin
  with ARecord do
  begin
    LastRefID := '';
    LogList := '';
    Date := NullDate;
    NetSales := 0;
    TaxableSales := 0;
    NonTaxableSales := 0;
    TaxExemptSales := 0;
    Tax1Amount := 0;
    Tax2Amount := 0;
    COGS := 0;
    SetLength(Payments, 0);
    SetLength(DeptEntries, 0);
  end;
end;

function TQBOnlineIntegration.SalesSettings(const ACat, AName: string): string;
begin
  Result := '';
  SalesSettingsFill;
  if memSalesReceipt.Active and memSalesReceipt.Locate('Cat;Name', VarArrayOf([ACat, AName]), [loCaseInsensitive]) and
    (not memSalesReceiptInActive.AsBoolean) then
    Result := memSalesReceiptQbName.AsString;
end;

procedure TQBOnlineIntegration.SalesSettingsFill;
var
  I, Y: Integer;
//  APrefSetting: string;
  AXmlEle: IXMLNode;
  // ADataSet: TDataSet;
  AStrList: TStringList;
begin
  if memSalesReceipt.Active then
    Exit;

  // APrefSetting := ProgPref('QBSalesReceiptItems');
  if JournalEntryMode then
    XMLDocument.Xml.LoadFromFile(ExtractFilePath(ParamStr(0))+'JournalEntryModePrefs.xml')
  else
    XMLDocument.Xml.LoadFromFile(ExtractFilePath(ParamStr(0))+'SalesReceiptModePrefs.xml');

  {if APrefSetting = '' then
    APrefSetting := '<Data></Data>';}

  memSalesReceipt.Open;
  memSalesReceipt.First;
  while not memSalesReceipt.Eof do
    memSalesReceipt.Delete;
  //XMLDocument.Xml.Text := APrefSetting;
  XMLDocument.Active := True;
  AXmlEle := XMLDocument.DocumentElement;
  for I := 0 to AXmlEle.ChildNodes.Count - 1 do
  begin
    memSalesReceipt.Insert;
    memSalesReceiptCat.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['Category']);
    memSalesReceiptName.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['Name']);
    memSalesReceiptQbName.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbName']);
    memSalesReceiptQbAccount.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbAccount']);
    memSalesReceiptQbAccountType.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbAccountType']);
    memSalesReceiptQbType.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbType']);
    if memSalesReceiptQbType.AsString = '' then
      memSalesReceiptQbType.AsString := 'Debit';
    memSalesReceiptInActive.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['InActive']);
    memSalesReceiptSortColumn.AsInteger := I;
    memSalesReceipt.Post;
  end;
  SalesSettingsAssert('Info', 'CustomerName', I);
  SalesSettingsAssert('Info', 'TaxItemName', I);
  SalesSettingsAssert('Info', 'ItemClassName', I);
  SalesSettingsAssert('Sales', 'NetSales', I);
  SalesSettingsAssert('Sales', 'Taxable', I);
  SalesSettingsAssert('Sales', 'NonTaxable', I);
  SalesSettingsAssert('Sales', 'TaxExempt', I);
  SalesSettingsAssert('Sales', 'Adjustment', I);
  SalesSettingsAssert('Taxes', 'Tax1', I);
  SalesSettingsAssert('Taxes', 'Tax2', I);
  SalesSettingsAssert('Payments', 'PdAcnt', I);

  SalesSettingsAssert('Payments', 'Cash', I);
  SalesSettingsAssert('Payments', 'Check', I);
  SalesSettingsAssert('Payments', 'Crdt Card', I);
  SalesSettingsAssert('Payments', 'Store Crdt', I);
  SalesSettingsAssert('Payments', 'On Account', I);

  SalesSettingsAssert('COGS', 'COGS', I);
  SalesSettingsAssert('COGS', 'COGS Inevntory Asset', I);

  if JournalEntryMode then
  begin
    AStrList := TStringList.Create;
    try
      AStrList.Add('GROCERY');
      AStrList.Add('DAIRY');
      AStrList.Add('MEAT');
      SalesSettingsAssert('Departments', 'Dept: <Undefined>', I);
      for Y := 0 to AStrList.Count - 1 do
        SalesSettingsAssert('Departments', 'Dept: ' + AStrList[Y], I);
    finally
      AStrList.Free;
    end;
  end;
end;

procedure TQBOnlineIntegration.SalesSettingsAssert(const ACat, AName: string; var AIndex: Integer);
begin
  if not memSalesReceipt.Locate('Cat;Name', VarArrayOf([ACat, AName]), [loCaseInsensitive]) then
  begin
    Inc(AIndex);
    memSalesReceipt.Insert;
    memSalesReceiptCat.AsString := ACat;
    memSalesReceiptName.AsString := AName;
    memSalesReceiptSortColumn.AsInteger := AIndex;
    memSalesReceiptQbType.AsString := 'Debit';
    memSalesReceipt.Post;
  end;
end;

function TQBOnlineIntegration.SalesJournalEntryAddUpdate(ARec: TQBOnlineSalesRecord): string;
var
  LastJId    : string;
  CustomerId : string;
  TaxId      : string;
  AccountID1 : string;
  AccountID2 : string;
  AccountID3 : string;
begin
  if ARec.LastRefID <> '' then
  begin
    try
      JournalDelete(Arec.LastRefID);
    except
    end;
  end;
  AccountId1 := AccountIDFind('Loan Payable');
  AccountId2 := AccountIDFind('Notes Payable');
  AccountId3 := AccountIDFind('Checking');

  try
    With commonFDQ do begin
      Close;
      SQL.Text :=  'INSERT Into JournalEntryLineItems ' + '(DocNumber, ' +
		               'Line_DetailType#1,Line_JournalEntryLineDetail_PostingType#1, Line_JournalEntryLineDetail_AccountRef#1, Line_Amount#1, ' +
		               'Line_DetailType#2,Line_JournalEntryLineDetail_PostingType#2, Line_JournalEntryLineDetail_AccountRef#2, Line_Amount#2, ' +
                   'Line_DetailType#3,Line_JournalEntryLineDetail_PostingType#3, Line_JournalEntryLineDetail_AccountRef#3, Line_Amount#3) ' +
                   'VALUES   (' + QuotedStr(ARec.LastRefID) + ','  +
                   QuotedStr('JournalEntryLineDetail') + ',' + QuotedStr('Credit') + ',' +  AccountID1 + ',' + '100' + ',' +
                   QuotedStr('JournalEntryLineDetail') + ',' + QuotedStr('Credit') + ',' +  AccountID2 + ',' + '20' + ',' +
                   QuotedStr('JournalEntryLineDetail') + ',' + QuotedStr('Debit') + ',' +  AccountID3 + ',' + '120' + ')' ;
      ExecSQL;
      Close;
      SQL.Text := 'SELECT MAX(ID) ID FROM JournalEntries';
      Open;
      if EOF then exit else Result := FieldByName('ID').AsString;
      Close;

    end;

  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBOnlineIntegration.JournalDelete(const ARefID: string): Boolean;
begin
  Result := False;
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT ID FROM JournalEntries WHERE Upper(DocNumber) = ' + QuotedStr(UpperCase(ARefId));
        Open;
        result :=  (RecordCount <> 0);
        if result then begin
          SQL.Text := 'DELETE FROM JournalEntries WHERE ID = ' + FieldByName('ID').AsString;
          ExecSQL;
        end else begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;

end;

end.
