object Form4: TForm4
  AlignWithMargins = True
  Left = 0
  Top = 0
  Margins.Left = 6
  Margins.Right = 6
  Margins.Bottom = 6
  Caption = 'quick'
  ClientHeight = 613
  ClientWidth = 1181
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlOnlineSetup: TPanel
    Left = 8
    Top = 8
    Width = 113
    Height = 145
    TabOrder = 0
    object lblCompanyId: TLabel
      AlignWithMargins = True
      Left = 7
      Top = 26
      Width = 99
      Height = 13
      Margins.Left = 6
      Margins.Top = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'CompanyId'
      ExplicitWidth = 55
    end
    object lblOnlinesetup: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 105
      Height = 13
      Align = alTop
      Alignment = taCenter
      Caption = 'Online setup'
      ExplicitWidth = 60
    end
    object edtCompanyId: TEdit
      AlignWithMargins = True
      Left = 7
      Top = 42
      Width = 99
      Height = 21
      Margins.Left = 6
      Margins.Top = 0
      Margins.Right = 6
      Align = alTop
      TabOrder = 0
      Text = '123146212005494'
    end
  end
  object pnlDesktopSetup: TPanel
    Left = 127
    Top = 8
    Width = 123
    Height = 145
    TabOrder = 1
    object lblDesktopSetup: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 115
      Height = 13
      Align = alTop
      Alignment = taCenter
      Caption = 'Desktop Setup'
      ExplicitWidth = 70
    end
    object lblUser: TLabel
      AlignWithMargins = True
      Left = 7
      Top = 26
      Width = 109
      Height = 13
      Margins.Left = 6
      Margins.Top = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'User'
      ExplicitWidth = 22
    end
    object lblPassword: TLabel
      AlignWithMargins = True
      Left = 7
      Top = 69
      Width = 109
      Height = 13
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      Caption = 'Password'
      ExplicitWidth = 46
    end
    object edtUser: TEdit
      AlignWithMargins = True
      Left = 7
      Top = 42
      Width = 109
      Height = 21
      Margins.Left = 6
      Margins.Top = 0
      Margins.Right = 6
      Align = alTop
      TabOrder = 0
      Text = 'Admin'
    end
    object edtConnectString: TEdit
      AlignWithMargins = True
      Left = 7
      Top = 115
      Width = 109
      Height = 21
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      TabOrder = 1
      TextHint = 'Connect String'
    end
    object Edit2: TEdit
      AlignWithMargins = True
      Left = 7
      Top = 88
      Width = 109
      Height = 21
      Margins.Left = 6
      Margins.Right = 6
      Align = alTop
      TabOrder = 2
      Text = 'Admin123'
    end
  end
  object pnlVendor: TPanel
    Left = 8
    Top = 159
    Width = 768
    Height = 145
    TabOrder = 2
    object lblVendorName: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 61
      Height = 13
      Caption = 'VendorName'
    end
    object edtVendorName: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 23
      Width = 277
      Height = 21
      TabOrder = 0
      Text = 'KRASDALE'
      TextHint = 'VendorName'
    end
    object mmoVendor: TMemo
      AlignWithMargins = True
      Left = 402
      Top = 4
      Width = 362
      Height = 137
      Align = alRight
      TabOrder = 1
    end
    object btnVendorFind: TButton
      Left = 7
      Top = 50
      Width = 131
      Height = 25
      Caption = 'VendorFind'
      TabOrder = 2
      OnClick = btnVendorFindClick
    end
    object btnVendorQuickAdd: TButton
      Left = 7
      Top = 81
      Width = 131
      Height = 25
      Caption = 'VendorQuickAdd'
      TabOrder = 3
      OnClick = btnVendorQuickAddClick
    end
    object btnVendorGetAccountPrefills: TButton
      Left = 7
      Top = 112
      Width = 131
      Height = 25
      Caption = 'VendorGetAccountPrefills'
      TabOrder = 4
      OnClick = btnVendorGetAccountPrefillsClick
    end
    object btnVendorExpenseList: TButton
      Left = 155
      Top = 50
      Width = 126
      Height = 25
      Caption = 'VendorExpenseList'
      TabOrder = 5
      OnClick = btnVendorExpenseListClick
    end
    object btnVendorNameList: TButton
      Left = 158
      Top = 81
      Width = 123
      Height = 25
      Caption = 'VendorNameList'
      TabOrder = 6
      OnClick = btnVendorNameListClick
    end
    object btnVendorCheckBalance: TButton
      Left = 158
      Top = 112
      Width = 123
      Height = 25
      Caption = 'VendorCheckBalance'
      TabOrder = 7
      OnClick = btnVendorCheckBalanceClick
    end
  end
  object pnlBill: TPanel
    Left = 8
    Top = 310
    Width = 768
    Height = 168
    TabOrder = 3
    object lblBillInfo: TLabel
      AlignWithMargins = True
      Left = 7
      Top = 7
      Width = 757
      Height = 13
      Margins.Left = 6
      Margins.Top = 6
      Margins.Bottom = 6
      Align = alTop
      Caption = 'Bill Info'
      ExplicitWidth = 35
    end
    object pnlBillLeft: TPanel
      Left = 1
      Top = 26
      Width = 766
      Height = 33
      Align = alTop
      TabOrder = 0
      object edtDelNo: TEdit
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 77
        Height = 25
        Align = alLeft
        TabOrder = 0
        Text = '2020011501'
        TextHint = 'DelNo'
        ExplicitHeight = 21
      end
      object edtDelDate: TEdit
        AlignWithMargins = True
        Left = 87
        Top = 4
        Width = 74
        Height = 25
        Align = alLeft
        TabOrder = 1
        Text = '01/15/2020'
        TextHint = 'Date'
        ExplicitHeight = 21
      end
      object edtDelDueDate: TEdit
        AlignWithMargins = True
        Left = 167
        Top = 4
        Width = 74
        Height = 25
        Align = alLeft
        TabOrder = 2
        Text = '01/25/2020'
        TextHint = 'DueDate'
        ExplicitHeight = 21
      end
      object edtVendInvNo: TEdit
        AlignWithMargins = True
        Left = 247
        Top = 4
        Width = 82
        Height = 25
        Align = alLeft
        TabOrder = 3
        Text = 'VU8581'
        TextHint = 'VendInvNo'
        ExplicitHeight = 21
      end
      object edtDelAmount: TEdit
        AlignWithMargins = True
        Left = 335
        Top = 4
        Width = 66
        Height = 25
        Align = alLeft
        TabOrder = 4
        Text = '500.01'
        TextHint = 'Amount'
        ExplicitHeight = 21
      end
      object edtDelExpenseAccount: TEdit
        AlignWithMargins = True
        Left = 407
        Top = 4
        Width = 105
        Height = 25
        Align = alLeft
        TabOrder = 5
        Text = 'POS ITEMS'
        TextHint = 'ExpenseAccount'
        ExplicitHeight = 21
      end
      object edtDelMemo: TEdit
        AlignWithMargins = True
        Left = 518
        Top = 4
        Width = 99
        Height = 25
        Align = alLeft
        TabOrder = 6
        Text = 'Just a memo to test'
        TextHint = 'Memo'
        ExplicitHeight = 21
      end
    end
    object mmoBill: TMemo
      AlignWithMargins = True
      Left = 402
      Top = 62
      Width = 362
      Height = 102
      Align = alRight
      TabOrder = 1
    end
    object btnBillAdd: TButton
      Left = 7
      Top = 65
      Width = 106
      Height = 25
      Caption = 'BillAdd'
      TabOrder = 2
      OnClick = btnBillAddClick
    end
    object btnBillDelete: TButton
      Left = 7
      Top = 96
      Width = 106
      Height = 25
      Caption = 'BillDelete'
      TabOrder = 3
      OnClick = btnBillDeleteClick
    end
    object btnBillFind: TButton
      Left = 7
      Top = 127
      Width = 106
      Height = 25
      Caption = 'BillFind'
      TabOrder = 4
      OnClick = btnBillFindClick
    end
    object btnBillGetStatus: TButton
      Left = 136
      Top = 65
      Width = 106
      Height = 25
      Caption = 'BillGetStatus'
      TabOrder = 5
      OnClick = btnBillGetStatusClick
    end
    object btnBillUpdate: TButton
      Left = 136
      Top = 96
      Width = 106
      Height = 25
      Caption = 'BillUpdate'
      TabOrder = 6
      OnClick = btnBillUpdateClick
    end
    object edtDelRefID: TEdit
      Left = 248
      Top = 61
      Width = 121
      Height = 21
      TabOrder = 7
      Text = '4-1545595823'
      TextHint = 'Last Ref ID'
    end
  end
  object stat: TStatusBar
    Left = 0
    Top = 594
    Width = 1181
    Height = 19
    Panels = <
      item
        Text = 'Connected: '
        Width = 100
      end
      item
        Text = 'Last Error:'
        Width = 50
      end>
  end
  object pnlSales: TPanel
    Left = 8
    Top = 484
    Width = 768
    Height = 108
    TabOrder = 5
    object lblSales: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 760
      Height = 13
      Align = alTop
      Caption = 'Sales'
      ExplicitWidth = 25
    end
    object btnSalesReceiptAddUpdate: TButton
      Left = 6
      Top = 46
      Width = 155
      Height = 25
      Caption = 'SalesReceiptAddUpdate'
      TabOrder = 0
      OnClick = btnSalesReceiptAddUpdateClick
    end
    object mmoSales: TMemo
      AlignWithMargins = True
      Left = 402
      Top = 20
      Width = 362
      Height = 84
      Margins.Top = 0
      Align = alRight
      TabOrder = 1
    end
    object btnSalesReceiptDelete: TButton
      Left = 7
      Top = 77
      Width = 154
      Height = 25
      Caption = 'SalesReceiptDelete'
      TabOrder = 2
      OnClick = btnSalesReceiptDeleteClick
    end
    object btnSalesJournalEntryAddUpdate: TButton
      Left = 167
      Top = 46
      Width = 155
      Height = 25
      Caption = 'SalesJournalEntryAddUpdate'
      TabOrder = 3
      OnClick = btnSalesJournalEntryAddUpdateClick
    end
    object btnFillTestRecord: TButton
      Left = 167
      Top = 15
      Width = 155
      Height = 25
      Caption = 'FillTestRecord'
      TabOrder = 4
      OnClick = btnFillTestRecordClick
    end
    object chkQbSalesExportMode: TCheckBox
      Left = 6
      Top = 23
      Width = 155
      Height = 17
      Caption = 'JournalEntryMode'
      TabOrder = 5
    end
    object edtSalesRefID: TEdit
      Left = 167
      Top = 77
      Width = 121
      Height = 21
      TabOrder = 6
      Text = '1A-1545606682'
      TextHint = 'Last Ref ID'
    end
  end
  object pnl1: TPanel
    Left = 256
    Top = 8
    Width = 521
    Height = 145
    TabOrder = 6
    object Label2: TLabel
      AlignWithMargins = True
      Left = 152
      Top = 68
      Width = 106
      Height = 13
      Margins.Left = 6
      Margins.Top = 6
      Margins.Bottom = 6
      Caption = 'QbBillExpenseAccount'
    end
    object mmoList: TMemo
      AlignWithMargins = True
      Left = 279
      Top = 4
      Width = 238
      Height = 137
      Align = alRight
      Lines.Strings = (
        'In Legacy Connection properties are '
        'set in TQBIntergration.DataModuleCreate, '
        'which reads it from the calling application '
        'Prefrences (ProgPref) function.'
        'By default currently open QB file on local '
        'computer will be accessed.'
        'When FCompanyFile is set, the set QB File will '
        'be opened regardless of the current open file.'
        'To connect to a Remote Conenction (with '
        'http://remoteconnector.com/) we need to set '
        'FConenctString to '
        'URL='#39'http://192.168.1.101:2087'#39' '
        'User='#39'Admin'#39' Password='#39'Admin123'#39)
      TabOrder = 0
    end
    object btnCustomerNameList: TButton
      Left = 8
      Top = 2
      Width = 138
      Height = 25
      Caption = 'CustomerNameList'
      TabOrder = 1
      OnClick = btnCustomerNameListClick
    end
    object btnItemNameList: TButton
      Left = 8
      Top = 33
      Width = 138
      Height = 25
      Caption = 'ItemNameList'
      TabOrder = 2
      OnClick = btnItemNameListClick
    end
    object btnClassNameList: TButton
      Left = 8
      Top = 64
      Width = 138
      Height = 25
      Caption = 'ClassNameList'
      TabOrder = 3
      OnClick = btnClassNameListClick
    end
    object btnCustomerVendorNameList: TButton
      Left = 8
      Top = 95
      Width = 138
      Height = 25
      Caption = 'CustomerVendorNameList'
      TabOrder = 4
      OnClick = btnCustomerVendorNameListClick
    end
    object rbDesktop: TRadioButton
      Left = 8
      Top = 127
      Width = 89
      Height = 15
      Caption = 'Desktop'
      TabOrder = 5
      OnClick = rbDesktopClick
    end
    object btnAccountNameList: TButton
      Left = 152
      Top = 33
      Width = 121
      Height = 26
      Caption = 'AccountNameList'
      TabOrder = 8
      OnClick = btnAccountNameListClick
    end
    object btnConnect: TButton
      Left = 152
      Top = 2
      Width = 121
      Height = 25
      Caption = 'Connect'
      TabOrder = 9
      OnClick = btnConnectClick
    end
    object rbOnline: TRadioButton
      Left = 80
      Top = 126
      Width = 66
      Height = 17
      Caption = 'Online'
      Checked = True
      TabOrder = 6
      TabStop = True
      OnClick = rbOnlineClick
    end
    object rbLegecy: TRadioButton
      Left = 152
      Top = 126
      Width = 89
      Height = 17
      Caption = 'Legacy'
      TabOrder = 7
    end
    object Edit1: TEdit
      Left = 152
      Top = 90
      Width = 121
      Height = 21
      TabOrder = 10
      Text = 'POS ITEMS'
    end
  end
  object DBGrid1: TDBGrid
    Left = 782
    Top = 8
    Width = 390
    Height = 580
    DataSource = DataSource1
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object FDPhysCDataQuickBooksOnlineDriverLink1: TFDPhysCDataQuickBooksOnlineDriverLink
    DriverID = 'CDataQuickBooksOnline'
    Left = 584
    Top = 376
  end
  object FDConnectionOnline: TFDConnection
    Params.Strings = (
      'InitiateOAuth=GETANDREFRESH'
      'UseSandbox=True'
      'DriverID=CDataQuickBooksOnline')
    Left = 584
    Top = 416
  end
  object FDConnectionDesktop: TFDConnection
    Params.Strings = (
      'DriverID=CDataQuickBooks')
    LoginPrompt = False
    Left = 656
    Top = 508
  end
  object FDQuery1: TFDQuery
    Connection = FDConnectionOnline
    Left = 656
    Top = 216
  end
  object DataSource1: TDataSource
    Left = 848
    Top = 296
  end
end
