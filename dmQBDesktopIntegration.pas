unit dmQBDesktopIntegration;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.Async, FireDAC.DApt, JvSimpleXml,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc;

  type
    EHandleInApp = class(Exception)
  end;

  TiqbobjsearchAccountTypes = (
    atUnspecified,
    atAccountsPayable,
    atAccountsReceivable,
    atBank,
    atCostOfGoodsSold,
    atCreditCard,
    atEquity,
    atExpense,
    atFixedAsset,
    atIncome,
    atLongTermLiability,
    atOtherAsset,
    atOtherCurrentAsset,
    atOtherCurrentLiability,
    atOtherExpense,
    atOtherIncome,
    atNonPosting
    );
  TQBDesktopSalesSetting = record
    QbAccount: string;
    QbAccountType: string;
    QbName: string;
    QbType: string;
  end;

  TQBDesktopSalesPayments = record
    PayName: string;
    PayAmount: Currency;
  end;

  TQBDesktopSalesDeptEntry = record
    DeptName: string;
    Amount: Currency;
  end;

  TQBDesktopSalesRecord = record
    LastRefID: string;
    LogList: string;
    Date: TDateTime;
    NetSales: Currency;
    TaxableSales: Currency;
    NonTaxableSales: Currency;
    TaxExemptSales: Currency;
    Tax1Amount: Currency;
    Tax2Amount: Currency;
    COGS: Currency;
    Payments: array of TQBDesktopSalesPayments;
    DeptEntries: array of TQBDesktopSalesDeptEntry;
  end;

  TQBDesktopIntegration = class(TDataModule)
    memExpenseList: TFDMemTable;
    memExpenseListAccountName: TStringField;
    memExpenseListFill: TIntegerField;
    memExpenseListAccountType: TStringField;
    memVendorList: TFDMemTable;
    memVendorListVendorName: TStringField;
    memExpenseListFillO: TIntegerField;
    memVendorListFill: TIntegerField;
    memVendorListFillO: TIntegerField;
    memCustomerList: TFDMemTable;
    memCustomerListCustomerName: TStringField;
    memItemList: TFDMemTable;
    memItemListItemName: TStringField;
    memItemListItemType: TStringField;
    memItemListItemAccount: TStringField;
    memClassList: TFDMemTable;
    memClassListClassName: TStringField;
    memCustomerVendorList: TFDMemTable;
    memCustomerVendorListName: TStringField;
    memAccountList: TFDMemTable;
    memAccountListAccountName: TStringField;
    memAccountListAccountType: TStringField;
    commonFDQ: TFDQuery;
    Xml1: TJvSimpleXML;
    commonFDQDetail: TFDQuery;
    XMLDocument: TXMLDocument;
    memSalesReceipt: TFDMemTable;
    memSalesReceiptCat: TStringField;
    memSalesReceiptName: TStringField;
    memSalesReceiptQbName: TStringField;
    memSalesReceiptQbAccount: TStringField;
    memSalesReceiptQbAccountType: TStringField;
    memSalesReceiptQbType: TStringField;
    memSalesReceiptInActive: TBooleanField;
    memSalesReceiptSortColumn: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);

  private
    FQbLastError: string;
    FExpenseAccount: string;
    FCompanyName: string;
    FConnectString: string;
    FAPAccount: string;
    FAPAccountID: string;
    FVendorItemMode: Boolean;
    FJournalEntryMode: Boolean;
   // function GetQBConnectionString: string;
    function LoadSQLIntoFDMem(ASql: string; AFDMem: TFDMemTable; AAppend: Boolean = False): integer;
    procedure ExpenseListFillByType(AFDMem: TFDMemTable; AAccountType: TiqbobjsearchAccountTypes; ACaption: string);
    function Bill_Credit_AddUpdate(AAction, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
      AAcountID, ARefID, AMemo: string): string;


  protected
    property  ExpenseAccount: string read FExpenseAccount;
    procedure ExpenseListFill;
    procedure VendorListFill;
    procedure CustomerListFill;
    procedure ItemListFill;
    procedure ClassListFill;

    //function QbAccountType(const AAccountType: string): TiqbobjsearchAccountTypeEnum;
    function ExpenseAccountIDFind(const AAccountName: string): string;
    function AccountIDFind(const AAccountName: string; AAccountType: string): string;
    function VendorListIdFromAccountPrefills(const AVendorName, AAccountName: string): string;
    function VendorListIdFromInvoice(const AVendorName, AAccountName: string): string;
    function ItemFind(const AItemName: string): string;

  public
    { Public declarations }
    property QbLastError: string read FQbLastError write FQbLastError;
    property JournalEntryMode: Boolean read FJournalEntryMode write FJournalEntryMode;
    property APAccount: string read FAPAccount;
    property APAccountID: string read FAPAccountID;
    property VendorItemMode: Boolean read FVendorItemMode write FVendorItemMode;

    function BillAdd(AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount, AMemo: string): string;
    function BillDelete(const ARefID: string): Boolean;
    function BillFind(const ADelNo: string): Boolean;
    function BillGetStatus(const ARefID: string; var AIsPaid: Boolean; var ABillAmount: Double;
      var ATransType: string): Boolean;
    function BillUpdate(ARefID, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
      AMemo: string): string;

    function VendorFind(const AVendorName: string; var ABalance: Currency): Boolean; overload;
    function VendorFind(const AVendorName: string; var APreFill: string): Boolean; overload;
    function VendorFind(const AVendorName: string): Boolean; overload;
    function VendorIDFind(const AVendorName: string): string;
    function VendorQuickAdd(const AVendorName: string): Boolean;
    function VendorGetAccountPrefills(const AVendorName: string; var AList: TStringList): Boolean; //not done
    procedure VendorExpenseList(const AVendorName: string); //not tested
    procedure VendorNameList(const AVendorName: string);

    function SalesReceiptAddUpdate(ARec: TQBDesktopSalesRecord): string;
    function SalesReceiptDelete(const ARefID: string): Boolean;
    function SalesJournalEntryAddUpdate(ARec: TQBDesktopSalesRecord): string;
    function JournalDelete(const ARefID: string): Boolean;

    procedure CustomerNameList;
    procedure ItemNameList;
    procedure ClassNameList;
    procedure CustomerVendorNameList;
    procedure AccountNameList;

    procedure SalesSettingsFill;
//    procedure SalesSettingsMemToXML; // not sure what is this
    function SalesSettings(const ACat, AName: string): string;
//    function SalesSettingsA(const ACat, AName: string): TQBSalesSetting;
    procedure SalesSettingsAssert(const ACat, AName: string; var AIndex: Integer);
    function GetTaxAgency(ATaxAgency: string) : string;
    function GetTaxItem(ATaxItem: string) : string;

    procedure ResetQBSalesRecord(var ARecord: TQBDesktopSalesRecord);
    function VendorCheckBalance(const AVendorName: string; var ABalance: Currency): Boolean;

  end;

function QBDesktopIntegration: TQBDesktopIntegration;
const
  QBConnectionStringA = 'ApplicationName="Total Inventory Solutions" ';
  Error817 = 'Error retreiving QB Expense Account Number, can''t add bill.' + #10#13 + 'Possible solutions:' + #10#13 +
    '1. Add Full Rights to Chart of Account for current user (QB Enterprise).' + #10#13 +
    '2. Add Financial Rights for current user (other QB versions).' + #10#13 +
    '3. Set the Expense Account for Vendor in POS and assign the account as an "Account Prefill" in QB.' + #10#13 +
    'QB Error: %s';

var
  __QBDesktopIntegration: TQBDesktopIntegration;

implementation
uses Forms, Dialogs, System.Variants, JclStreams, cxDateUtils, System.Rtti, cdQB;
{$R *.dfm}
{%CLASSGROUP 'Vcl.Controls.TControl'}

function QBDesktopIntegration: TQBDesktopIntegration;
begin
  if (__QBDesktopIntegration = nil) then
    __QBDesktopIntegration := TQBDesktopIntegration.Create(Application);

  Result := __QBDesktopIntegration;
end;

function TQBDesktopIntegration.LoadSQLIntoFDMem(ASql: string; AFDMem: TFDMemTable; AAppend: Boolean = False): integer;
begin
  Result := 0;
  try
    With commonFDQ do begin
      Close;
      SQL.Text       := ASQL;
      Open;
      AFDmem.Active := True;
      If not AAppend then begin
        AFDMem.EmptyDataSet;
        AFDMem.CopyDataSet(commonFDQ, [coAppend]);
      end else begin
        First;
        while not EOF do begin
          AFDMem.Insert;
          AFDMem.FieldByName('AccountName').AsString := FieldByName('AccountName').AsString;
          AFDMem.FieldByName('AccountType').AsString := FieldByName('AccountType').AsString;
          AFDMem.Post;
          Next;
        end;
      end;
      Result := AFDMem.RecordCount;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

(*
function TQBDesktopIntegration.GetQBConnectionString: string;
begin
  Result := QBConnectionStringA;
  if FConnectString <> '' then
    Result := FConnectString + ' ' + Result;
  if FCompanyName <> '' then
    Result := Result + Format('CompanyFile="%s" ', [FCompanyName]);
end;
*)
procedure TQBDesktopIntegration.DataModuleCreate(Sender: TObject);
begin
  FExpenseAccount := ''; // ProgPref('QbBillExpenseAccount');
  FAPAccount := ''; // ProgPref('QbBillAPAccount');
  FCompanyName := ''; // ProgPref('QbBillCompanyName');
  FConnectString := ''; // ProgPref('QbConnectString');
  FVendorItemMode := False; // SameText(ProgPref('QbVendExpenseItemMode'), 'Items');
  FJournalEntryMode := False; // SameText(ProgPref('QbSalesExportMode'), 'Journal Entry');
  FAPAccountID := '';

end;

procedure TQBDesktopIntegration.ExpenseListFillByType(AFDmem: TFDMemTable; AAccountType: TiqbobjsearchAccountTypes;
  ACaption: string);
var
  sSQL : string;
begin
  sSQL := 'SELECT Name AccountName, Type AccountType FROM Accounts WHERE Type = ' + UpperCase(QuotedStr(ACaption)) + ' ORDER BY Name';
  LoadSQLIntoFDMem(sSql, AFDMem, True);
end;

procedure TQBDesktopIntegration.ItemNameList;
begin
  if not memItemList.Active then
    memItemList.Open;
  if memItemList.IsEmpty then
    ItemListFill;
  memItemList.First;
end;

procedure TQBDesktopIntegration.ItemListFill;
var
  sSQL : string;
begin
  if not memItemList.Active then
    memItemList.Open;
  memItemList.DisableControls;
  memItemList.EmptyDataset;
  try
    try
      sSQL := 'SELECT FullName ItemName, Type ItemType, Account IncomeAccountRef_name FROM Items  ORDER BY FullName';
      LoadSQLIntoFDMem(sSql, memItemList, False);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memItemList.EnableControls;
  end;
end;


procedure TQBDesktopIntegration.CustomerListFill;
var
  sSQL : string;
begin
  if not memCustomerList.Active then
    memCustomerList.Open;
  memCustomerList.DisableControls;
  memCustomerList.EmptyDataset;


  // AppActions.ShowHourGlassCursor;
  memCustomerList.DisableControls;
  try
    try
      sSQL := 'SELECT FullName CustomerName FROM Customers  ORDER BY FullName';
      LoadSQLIntoFDMem(sSql, memCustomerList, False);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memCustomerList.EnableControls;
  end;
end;

procedure TQBDesktopIntegration.CustomerNameList;
begin
  if not memCustomerList.Active then
    memCustomerList.Open;
  if memCustomerList.IsEmpty then
    CustomerListFill;
  memCustomerList.First;
end;

procedure TQBDesktopIntegration.ClassNameList;
begin
  if not memClassList.Active then
    memClassList.Open;
  if memClassList.IsEmpty then
    ClassListFill;
  memClassList.First;
end;

procedure TQBDesktopIntegration.VendorExpenseList(const AVendorName: string);
var
  AList: TStringList;
  I    : Integer;
begin
  if not memExpenseList.Active then
    memExpenseList.Open;
  memExpenseList.First;
  if memExpenseList.IsEmpty then
    ExpenseListFill;

  memExpenseList.DisableControls;
  try
    memExpenseList.First;
    while not memExpenseList.Eof do
    begin
      if (memExpenseListFill.AsInteger > 0) and (memExpenseListFill.AsInteger < 99) then
      begin
        memExpenseList.Edit;
        memExpenseListFill.AsInteger := memExpenseListFillO.AsInteger;
        memExpenseList.Post;
      end;
      memExpenseList.Next
    end;

    if AVendorName <> '' then
    begin
      AList := TStringList.Create;
      try
        if VendorGetAccountPrefills(AVendorName, AList) then
          for I := 0 to AList.Count - 1 do
          begin
            if memExpenseList.Locate('AccountName', AList[I], [loCaseInsensitive]) then
            begin
              memExpenseList.Edit;
              memExpenseListFill.AsInteger := I + 1;
              memExpenseList.Post;
            end;
          end;
      finally
        AList.Free;
      end;
    end;
    memExpenseList.First;
  finally
    memExpenseList.EnableControls;
  end;
end;

procedure TQBDesktopIntegration.ClassListFill;
var
  sSQL : string;
begin
  if not memClassList.Active then
    memClassList.Open;
  memClassList.DisableControls;
  memClassList.EmptyDataset;

  // AppActions.ShowHourGlassCursor;
  memClassList.DisableControls;
  try
    try
      sSQL := 'SELECT Name ClassName FROM Class  ORDER BY Name';
      LoadSQLIntoFDMem(sSql, memClassList, False);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memClassList.EnableControls;
  end;
end;

procedure TQBDesktopIntegration.ExpenseListFill;
var
  I: Integer;
begin
  if not memExpenseList.Active then
    memExpenseList.Open;
  memExpenseList.EmptyDataset;

  // AppActions.ShowHourGlassCursor;
  memExpenseList.DisableControls;
  try
    try
      ExpenseListFillByType(memExpenseList, atExpense, 'EXPENSE');
      ExpenseListFillByType(memExpenseList, atCostOfGoodsSold, 'COSTOFGOODSSOLD');

      //memExpenseList.SortedField := 'AccountName';
      I := 100;
      memExpenseList.First;
      while not memExpenseList.Eof do
      begin
        Inc(I);
        memExpenseList.Edit;
        memExpenseListFill.AsInteger := I;
        memExpenseListFillO.AsInteger := I;
        memExpenseList.Post;
        memExpenseList.Next;
      end;
      //memExpenseList.SortedField := 'Fill';

      if ExpenseAccount <> '' then
      begin
        memExpenseList.Insert;
        memExpenseListAccountName.AsString := '<DEFAULT>';
        memExpenseListAccountType.AsString := '(' + ExpenseAccount + ')';
        memExpenseListFill.AsInteger := 0;
        memExpenseList.Post;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memExpenseList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBDesktopIntegration.VendorListFill;
var
  I    : Integer;
  sSQL : string;
begin
  if not memVendorList.Active then
    memVendorList.Open;
  memVendorList.DisableControls;
  memVendorList.EmptyDataset;

  // AppActions.ShowHourGlassCursor;
  memVendorList.DisableControls;
  try
    try
      sSQL := 'SELECT Name VendorName, 0 Fill, 0 FillO FROM Vendors  ORDER BY Name';
      LoadSQLIntoFDMem(sSql, memVendorList, False);


      // memVendorList.SortedField := 'VendorName';
      I := 100;
      memVendorList.First;
      while not memVendorList.Eof do
      begin
        Inc(I);
        memVendorList.Edit;
        memVendorListFill.AsInteger := I;
        memVendorListFillO.AsInteger := I;
        memVendorList.Post;
        memVendorList.Next;
      end;
      // memVendorList.SortedField := 'Fill';
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    memVendorList.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBDesktopIntegration.CustomerVendorNameList;
begin
  if not memCustomerVendorList.Active then
    memCustomerVendorList.Open;
  if not memCustomerVendorList.IsEmpty then
    Exit;

  if not memCustomerList.Active then
    memCustomerList.Open;
  if memCustomerList.IsEmpty then
    CustomerListFill;;

  if not memVendorList.Active then
    memVendorList.Open;
  if memVendorList.IsEmpty then
    VendorListFill;

  memVendorList.First;
  while not memVendorList.Eof do
  begin
    memCustomerVendorList.Insert;
    memCustomerVendorListName.AsString := memVendorListVendorName.AsString;
    memCustomerVendorList.Post;
    memVendorList.Next;
  end;

  memVendorList.First;
  while not memCustomerList.Eof do
  begin
    memCustomerVendorList.Insert;
    memCustomerVendorListName.AsString := memCustomerListCustomerName.AsString;
    memCustomerVendorList.Post;
    memCustomerList.Next;
  end;

  memCustomerVendorList.First;
end;

procedure TQBDesktopIntegration.AccountNameList;
begin
  if not memAccountList.Active then
    memAccountList.Open;
  if memAccountList.IsEmpty then
  begin
    // second parameter is dummy
    ExpenseListFillByType(memAccountList, atExpense, 'EXPENSE');
    ExpenseListFillByType(memAccountList, atAccountsReceivable, 'ACCOUNTSRECEIVABLE');
    ExpenseListFillByType(memAccountList, atIncome, 'INCOME');
    ExpenseListFillByType(memAccountList, atOtherAsset, 'OTHERASSET');
    ExpenseListFillByType(memAccountList, atOtherExpense, 'OTHEREXPENSE');
    ExpenseListFillByType(memAccountList, atOtherIncome, 'OTHERINCOME');
    ExpenseListFillByType(memAccountList, atOtherCurrentAsset, 'OTHERCURRENTASSET');
    ExpenseListFillByType(memAccountList, atOtherCurrentLiability, 'OTHERCURRENTLIABILITY');
    ExpenseListFillByType(memAccountList, atBank, 'BANK');
    ExpenseListFillByType(memAccountList, atBank, 'EQUITY');
    ExpenseListFillByType(memAccountList, atBank, 'ACCOUNTSPAYABLE');
    ExpenseListFillByType(memAccountList, atBank, 'FIXEDASSET');
    ExpenseListFillByType(memAccountList, atBank, 'CREDITCARD');
    ExpenseListFillByType(memAccountList, atBank, 'LONGTERMLIABILITY');
    ExpenseListFillByType(memAccountList, atBank, 'COSTOFGOODSSOLD');
    ExpenseListFillByType(memAccountList, atBank, 'NONPOSTING');
  end;
end;

function TQBDesktopIntegration.VendorFind(const AVendorName: string; var ABalance: Currency): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT Name DisplayName, Balance FROM Vendors WHERE Upper(Name) LIKE '
              + QuotedStr('%'  + AVendorName.ToUpper + '%') + ' ORDER BY Name';

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        Result := Locate('DisplayName', AVendorName, [loCaseInsensitive]);
        If Result then ABalance := FieldByName('Balance').AsCurrency;
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.VendorFind(const AVendorName: string; var APreFill: String): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT Name DisplayName, PrefillAccountId1 FROM Vendors WHERE Upper(Name) LIKE '
              + QuotedStr('%'  + AVendorName.ToUpper + '%') + ' ORDER BY Name';

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        Result := Locate('DisplayName', AVendorName, [loCaseInsensitive]);
        If Result then APreFill := FieldByName('PrefillAccountRef').AsString;
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.VendorFind(const AVendorName: string): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT Name DisplayName FROM Vendors WHERE Upper(Name) LIKE '
              + QuotedStr('%'  + AVendorName.ToUpper + '%') + ' ORDER BY Name';

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        Result := Locate('DisplayName', AVendorName, [loCaseInsensitive]);
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.VendorIDFind(const AVendorName: string): string;
var
  sSQL : string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT ID FROM Vendors WHERE Upper(Name) = '
              + QuotedStr(AVendorName.ToUpper);

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        if RecordCount > 0 then Result := FieldByName('ID').AsString;
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TQBDesktopIntegration.VendorNameList(const AVendorName: string);
begin
  if not memVendorList.Active then
    memVendorList.Open;
  memVendorList.First;
  if memVendorList.IsEmpty then
    VendorListFill;

  memVendorList.DisableControls;
  try
    memVendorList.First;
    while not memVendorList.Eof do
    begin
      if memVendorListFill.AsInteger < 99 then
      begin
        if memVendorListFillO.AsInteger = 1 then
        begin
          memVendorList.Delete;
          Continue;
        end
        else
          memVendorList.Edit;
        memVendorListFill.AsInteger := memVendorListFillO.AsInteger;
        memVendorList.Post;
      end;
      memVendorList.Next;
    end;

    if memVendorList.Locate('VendorName', AVendorName, [loCaseInsensitive]) then
    begin
      memVendorList.Edit;
      memVendorListFill.AsInteger := 1;
      memVendorList.Post;
    end
    else
    begin
      memVendorList.Insert;
      memVendorListVendorName.AsString := AVendorName;
      memVendorListFill.AsInteger := 1;
      memVendorListFillO.AsInteger := 1;
      memVendorList.Post;
    end;
    memVendorList.First;
  finally
    memVendorList.EnableControls;
  end;
end;


function TQBDesktopIntegration.VendorCheckBalance(const AVendorName: string; var ABalance: Currency): Boolean;
begin
  Result := VendorFind(AVendorName, ABalance);
end;


function TQBDesktopIntegration.VendorGetAccountPrefills(const AVendorName: string; var AList: TStringList): Boolean;
var
  sSQL : string;
begin
  AList.Clear;
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT PrefillAccountName1, PrefillAccountName2, PrefillAccountName3 FROM Vendors WHERE Upper(Name) = '
              + QuotedStr(AVendorName.ToUpper);

      With commonFDQ do begin
        Close;
        SQL.Text       := sSQL;
        Open;
        if RecordCount > 0 then Result := True else exit;
        if FieldByName('PrefillAccountName1').AsString <> '' then AList.Add(FieldByName('PrefillAccountName1').AsString);
        if FieldByName('PrefillAccountName2').AsString <> '' then AList.Add(FieldByName('PrefillAccountName2').AsString);
        if FieldByName('PrefillAccountName3').AsString <> '' then AList.Add(FieldByName('PrefillAccountName3').AsString);
        Close;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.VendorQuickAdd(const AVendorName: string): Boolean;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      With commonFDQ do begin
        Close;
        SQL.Text := 'INSERT INTO Vendors (Name) VALUES (' + QuotedStr(AVendorName) + ')' ;
        ExecSQL;
      end;
      Result := True;
    except
      on E: Exception do
        if SameText(E.Message, Format('Exception 899: The name "%s" of the list element is already deleted.',
          [AVendorName])) then
          QbLastError := Format('If vendor was made inactive in QuickBooks, ' + #13#10 +
            'please reactivate or rename the inactive vendor ' + #13#10 + 'and try again.', [AVendorName])
        else
          QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.BillFind(const ADelNo: string): Boolean;
var
  sSQL : string;
begin
  Result := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'SELECT TOP 1 ReferenceNumber FROM Bills WHERE Upper(ReferenceNumber) = ' + QuotedStr(ADelNo) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        Open;
        Result := (RecordCount = 1);
      end;
      if not Result then
      begin
        sSQL := 'SELECT TOP 1 ReferenceNumber FROM VendorCredits WHERE Upper(ReferenceNumber) = ' + QuotedStr(ADelNo) ;
        With commonFDQ do begin
          Close;
          SQL.Text := sSQL;
          Open;
          Result := (RecordCount = 1);
        end;
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ADelNo])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ADelNo])
        else
          QbLastError := E.Message;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.BillGetStatus(const ARefID: string; var AIsPaid: Boolean; var ABillAmount: Double;
  var ATransType: string): Boolean;
var
  ASearchCredit : Boolean;
  sSQL          : string;
begin
  Result := False;
  ASearchCredit := False;
  AIsPaid := False;
  ABillAmount := 0;
  ATransType := '';
  try
    try
      sSQL := 'SELECT ReferenceNumber, Amount, OpenAmount FROM Bills WHERE Upper(ReferenceNumber) = ' + QuotedStr(ARefID) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        Open;
        Result := (RecordCount = 1);
        if Result then begin
           AIsPaid := FieldByName('OpenAmount').AsFloat = 0;
           ABillAmount := FieldByName('Amount').AsFloat;
        end;
        ATransType := 'Bill';
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
        begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
          ASearchCredit := True;
        end
        else
          QbLastError := E.Message;
      end;
    end;
    try
      if (not Result) and ASearchCredit then
      begin
        sSQL := 'SELECT Amount FROM VendorCredits WHERE Upper(ReferenceNumber) = ' + QuotedStr(ARefID) ;
        With commonFDQ do begin
          Close;
          SQL.Text := sSQL;
          Open;
          Result := (RecordCount = 1);
          AIsPaid := True;
          ABillAmount := FieldByName('Amount').AsFloat;
          ATransType := 'Credit';
        end;
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID])
        else
          QbLastError := E.Message;
      end;
   end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.BillDelete(const ARefID: string): Boolean;
var
  ASearchCredit: Boolean;
  sSQL         : string;
begin
  Result := False;
  ASearchCredit := False;
  // AppActions.ShowHourGlassCursor;
  try
    try
      sSQL := 'DELETE FROM Bills WHERE Upper(ReferenceNumber) = ' + QuotedStr(ARefID) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        ExecSQL;
        Result := (RowsAffected = 1);
        if not Result then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
      end;
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
        begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
          ASearchCredit := True;
        end
        else
          QbLastError := E.Message;
      end;
    end;
    if (not Result) and ASearchCredit then
    begin
      try
      sSQL := 'DELETE FROM VendorCredits WHERE Upper(ReferenceNumber) = ' + QuotedStr(ARefID) ;
      With commonFDQ do begin
        Close;
        SQL.Text := sSQL;
        ExecSQL;
        Result := (RowsAffected = 1);
        if not Result then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
      end;
      except
        on E: Exception do
        begin
          if SameText(E.Message,
            Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
            [ARefID])) then
            // Do nothing
          else
            QbLastError := E.Message;
        end;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.BillUpdate(ARefID, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
  AMemo: string): string;
var
  AAcountID: string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      if AVendorName = '' then
        raise EHandleInApp.Create('QB Vendor Name not set, can''t update bill.');
      if SameText(AExpenseAccount, '<DEFAULT>') then
        AExpenseAccount := ExpenseAccount;
      if AExpenseAccount = '' then
        raise EHandleInApp.Create('QB Expense Account not set, can''t update bill.');
      AAcountID := ExpenseAccountIDFind(AExpenseAccount);
      if (AAcountID = '') and VendorItemMode then
        AAcountID := ItemFind(AExpenseAccount);
      if (AAcountID = '') and (Copy(QbLastError, 1, 3) = '817') then
      begin
        AAcountID := VendorListIdFromAccountPrefills(AVendorName, AExpenseAccount);
        if AAcountID = '' then
          AAcountID := VendorListIdFromInvoice(AVendorName, AExpenseAccount);
      end;

      if AAcountID = '' then
      begin
        if Copy(QbLastError, 1, 3) = '817' then
          raise EHandleInApp.CreateFmt(Error817, [QbLastError])
        else if QbLastError <> '' then
          raise EHandleInApp.CreateFmt('Error retrieving QB Expense Account Number, can''t update bill.' + #10#13 +
            'QB Error: %s', [QbLastError])
        else
          raise EHandleInApp.CreateFmt('QB Expense Account Number not found for Account %s, can''t update bill.',
            [AExpenseAccount]);
      end;
      Result := Bill_Credit_AddUpdate('Update', AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount,
        AExpenseAccount, AAcountID, ARefID, AMemo);
    except
      on E: Exception do
      begin
        if SameText(E.Message,
          Format('702: The query request has not been fully completed. There was a required element ("%s") that could not be found in QuickBooks.',
          [ARefID])) then
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ADelNo])
        else
          QbLastError := E.Message;
      end;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.ExpenseAccountIDFind(const AAccountName: string): string;
begin
  Result := AccountIDFind(AAccountName, 'COSTOFGOODSSOLD');
  if Result = '' then
    Result := AccountIDFind(AAccountName, 'EXPENSE');
end;

function TQBDesktopIntegration.AccountIDFind(const AAccountName: string; AAccountType: string): string;
begin
  Result := '';
  try
    With CommonFDQ do begin
      Close;
      SQL.Clear;
      SQL.Text := 'SELECT ID FROM Accounts WHERE Upper(Name) = ' + QuotedStr(AAccountName.ToUpper) +
                  ' AND Upper(Type) = ' + QuotedStr(AAccountType.ToUpper) ;
      Open;
      if RecordCount = 0 then exit;
      Result := FieldByName('ID').AsString;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBDesktopIntegration.ItemFind(const AItemName: string): string;
begin
  Result := '';
  try
    With CommonFDQ do begin
      Close;
      SQL.Clear;
      SQL.Text := 'SELECT ID FROM Items WHERE Upper(Name) = ' + QuotedStr(AItemName.ToUpper) ;
      Open;
      if RecordCount = 0 then exit;
      Result := FieldByName('ID').AsString;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBDesktopIntegration.VendorListIdFromAccountPrefills(const AVendorName, AAccountName: string): string;
var
  AXmlEle  : TJvSimpleXmlElem;
  AXmlAcnt : TJvSimpleXmlElem;
  AListID  : string;
  x        : Integer;
  Y        : Integer;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT PrefillAccountRef  FROM Vendors WHERE Upper(DisplayName) = ' + QuotedStr(AVendorName.ToUpper) ;
        Open;
        if RecordCount = 0 then exit;
        if FieldByName('PrefillAccountRef').AsString = '' then exit;
        Xml1.LoadFromString(FieldByName('PrefillAccountRef').AsString);
        AXmlEle := Xml1.Root;
        for x := 0 to AXmlEle.Items.Count - 1 do
        begin
          AListID := '';
          if SameText(AXmlEle.Items[x].Name, 'PrefillAccountRef') then
          begin
            AXmlAcnt := AXmlEle.Items[x];
            for Y := 0 to AXmlAcnt.Items.Count - 1 do
              if SameText(AXmlAcnt.Items[Y].Name, 'ListID') then
                AListID := AXmlAcnt.Items[Y].Value
              else if SameText(AXmlAcnt.Items[Y].Name, 'FullName') and (AListID <> '') then
              begin
                Result := AListID;
                Break;
              end;
          end;
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.VendorListIdFromInvoice(const AVendorName, AAccountName: string): string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  //vendorref_name, apaccountref_name
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT a.apAccountRef  FROM Bills WHERE Upper(vendorref_name) = ' + QuotedStr(AVendorName.ToUpper);
        Open;
        if RecordCount = 0 then exit;
        Result := FieldByName('apAccountRef').AsString;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.Bill_Credit_AddUpdate(AAction, AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount,
  AExpenseAccount, AAcountID, ARefID, AMemo: string): string;
var sSQL         : string;
    sSQLDetail   : string;
    vendorId     : string;
    BillID       : string;
    ExpAccountid : string;
    sDetailXML   : string;
begin
  if (APAccount <> '') and (APAccountID = '') then
  begin
    FAPAccountID := AccountIDFind(APAccount, 'Accounts Payable');
    if APAccountID = '' then
      FAPAccount := '';
  end;
  ExpAccountid := ExpenseAccountIDFind(AExpenseAccount);
  vendorId := VendorIdFind(AVendorName);
  if StrToFloat(AAmount) < 0 then
  begin
    With CommonFDQ do begin
      if ((ARefID <> '') and  (SameText(AAction, 'Update'))) then
      begin
        sSQL := 'UPDATE VendorCredits SET  '  +
                                 ' Memo   = '      + QuotedStr(AMemo)  +
                                 ',Date   = '      + QuotedStr(ADate)  +
                                 ',Amount = '      + QuotedStr(AAmount);
        If ApAccountId <> '' then
          sSQL := sSQL + ',AccountsPayableId   = ' +  QuotedStr(APAccountID);
        sSQL := sSQL + ' WHERE ReferenceNumber = ' + QuotedStr(AVendInvNo);
      end
      else if SameText(AAction, 'Add') then begin
        if VendorItemMode then
        begin
            sSQL := 'INSERT INTO VendorCreditLineItems ( VendorId, Memo, ReferenceNumber,Date, Amount,ItemLineNumber,ItemId,ItemAmount) VALUES ('
               +  QuotedStr(VendorId) + ',' + QuotedStr(AMemo) + ', ' + QuotedStr(AVendInvNo) + ',' + QuotedStr(ADate) + ', '
               +  FloatToStr(Abs(StrToFloat(AAmount))) + ', ' +  '1,' + QuotedStr(ExpAccountid) + ',' + FloatToStr(Abs(StrToFloat(AAmount))) + ')';
        end else
        begin
          sSQL := 'INSERT INTO VendorCreditExpenseItems ( VendorId, Memo, ReferenceNumber,Date, Amount,ExpenseLineNumber,ExpenseAccountId,EXpenseAmount) VALUES ('
               +  QuotedStr(VendorId) + ',' + QuotedStr(AMemo) + ', ' + QuotedStr(AVendInvNo) + ',' + QuotedStr(ADate) + ', '
               +  FloatToStr(Abs(StrToFloat(AAmount))) + ', ' +  '1,' + QuotedStr(ExpAccountid) + ',' + FloatToStr(Abs(StrToFloat(AAmount))) + ')';
        end;
      end;
      Close;
      SQL.text := sSQL;
      ExecSQL;
      sSQL := 'SELECT * FROM VendorCredits WHERE Upper(ReferenceNumber) = ' + UpperCase(QuotedStr(ARefId));
      Close;
      SQL.Clear;
      SQL.text := sSQL;
      Open;
      if not EOF then BillID := FieldByName('ID').AsString;
    end;
  end
  else
  begin
    With CommonFDQ do begin
      if ((ARefID <> '') and  (SameText(AAction, 'Update'))) then
      begin
        sSQL := 'UPDATE Bills SET  '  +
                                 ' Memo   = '      + QuotedStr(AMemo)  +
                                 ',Date   = '      + QuotedStr(ADate)  +
                                 ',Amount = '      + QuotedStr(AAmount);
        If ApAccountId <> '' then
          sSQL := sSQL + ',AccountsPayableId   = ' +  QuotedStr(APAccountID);
        sSQL := sSQL + ' WHERE ReferenceNumber = ' +  QuotedStr(AVendInvNo);
      end
      else if SameText(AAction, 'Add') then begin
        if  VendorItemMode then
        begin
          sSQL := 'INSERT INTO BillLineItems (VendorId, Memo, ReferenceNumber,Date, Amount,ItemLineNumber,ItemId,ItemAmount) VALUES ('
               +  QuotedStr(VendorId) + ',' + QuotedStr(AMemo) + ', ' + QuotedStr(AVendInvNo) + ',' + QuotedStr(ADate) + ', '  + AAmount + ', '
               +  '1,' + QuotedStr(ExpAccountid) + ',' + AAmount + ')';
        end
        else
        begin
          sSQL := 'INSERT INTO BillExpenseItems (VendorId, Memo, ReferenceNumber,Date, Amount,ExpenseLineNumber,ExpenseAccountId,ExpenseAmount) VALUES ('
               +  QuotedStr(VendorId) + ',' + QuotedStr(AMemo) + ', ' + QuotedStr(AVendInvNo) + ',' + QuotedStr(ADate) + ', '  + AAmount + ', '
               +  '1,' + QuotedStr(ExpAccountid) + ',' + AAmount + ')';
        end;
      end;
      Close;
      SQL.text := sSQL;
      ExecSQL;
      sSQL := 'SELECT * FROM Bills WHERE Upper(ReferenceNumber) = ' + UpperCase(QuotedStr(ARefId));
      Close;
      SQL.Clear;
      SQL.text := sSQL;
      Open;
      If not EOF then BillID := FieldByName('ID').AsString;
    end;
  end;
  Result := BillID;
end;

function TQBDesktopIntegration.BillAdd(AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
  AMemo: string): string;
var
  AAcountID: string;
begin
  Result := '';
  // AppActions.ShowHourGlassCursor;
  try
    try
      if AVendorName = '' then
        raise EHandleInApp.Create('QB Vendor Name not set, can''t add bill.');
      if SameText(AExpenseAccount, '<DEFAULT>') then
        AExpenseAccount := ExpenseAccount;
      if AExpenseAccount = '' then
        raise EHandleInApp.Create('QB Expense Account not set, can''t add bill.');

      AAcountID := ExpenseAccountIDFind(AExpenseAccount);

      if (AAcountID = '') and VendorItemMode then
        AAcountID := ItemFind(AExpenseAccount);

      if (AAcountID = '') and (Copy(QbLastError, 1, 3) = '817') then
      begin
        AAcountID := VendorListIdFromAccountPrefills(AVendorName, AExpenseAccount);
        if AAcountID = '' then
          AAcountID := VendorListIdFromInvoice(AVendorName, AExpenseAccount);
      end;

      if AAcountID = '' then
      begin
        if Copy(QbLastError, 1, 3) = '817' then
          raise EHandleInApp.CreateFmt(Error817, [QbLastError])
        else if QbLastError <> '' then
          raise EHandleInApp.CreateFmt('Error retreiving QB Expense Account Number, can''t add bill.' + #10#13 +
            'QB Error: %s', [QbLastError])
        else
          raise EHandleInApp.CreateFmt('QB Expense Account Number not found for Account %s, can''t add bill.',
            [AExpenseAccount]);
      end;
      Result := Bill_Credit_AddUpdate('Add', AVendorName, ADelNo, ADate, ADueDate, AVendInvNo, AAmount, AExpenseAccount,
        AAcountID, '', AMemo);
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;
end;

function TQBDesktopIntegration.SalesReceiptDelete(const ARefID: string): Boolean;
begin
  Result := False;
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT ID FROM SalesReceipts WHERE ID = ' + QuotedStr(UpperCase(ARefId));
        Open;
        result :=  (RecordCount <> 0);
        if result then begin
          SQL.Text := 'DELETE FROM SalesReceipts WHERE ID = ' + QuotedStr(UpperCase(ARefId));
          ExecSQL;
        end else begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;

end;

function TQBDesktopIntegration.GetTaxAgency(ATaxAgency: string) : string;
var
  TaxId : string;
begin
  TaxId := '';
  With commonFDQ do begin
    Close;
    SQL.Text := 'SELECT ID FROM SalesTaxCodes WHERE Upper(Name) = ' + QuotedStr(UpperCase(ATaxAgency));
    Open;
    if not EOF then TaxId := FieldByName('ID').AsString;
    Close;
  end;
  Result := TaxId;
end;

function TQBDesktopIntegration.GetTaxItem(ATaxItem: string) : string;
var
  TaxId : string;
begin
  TaxId := '';
  With commonFDQ do begin
    Close;
    SQL.Text := 'SELECT ID FROM SalesTaxItems WHERE Upper(Name) = ' + QuotedStr(UpperCase(ATaxItem));
    Open;
    if not EOF then TaxId := FieldByName('ID').AsString;
    Close;
  end;
  Result := TaxId;
end;

function TQBDesktopIntegration.SalesReceiptAddUpdate(ARec: TQBDesktopSalesRecord): string;
var
  CustomerId : string;
  TaxId      : string;
  LastSRId   : string;
  ItemId     : string;
  ItemId1    : string;
begin
  if ARec.LastRefID <> '' then
  begin
    try
      SalesReceiptDelete(Arec.LastRefID);
    except
    end;
  end;
  With commonFDQ do begin
    Close;
    SQL.Text := 'SELECT ID FROM Customers WHERE Upper(FullName) = ' + QuotedStr(UpperCase(SalesSettings('Info', 'CustomerName')));
    Open;
    if not EOF then CustomerID := FieldByName('ID').AsString;
    Close;
  end;
  TaxId := GetTaxItem(SalesSettings('Taxes', 'Tax1'));
  try
    With commonFDQ do begin
      ItemId := ItemFind('Spare1');
      ItemId1 := ItemFind('Service1');
      // this syntax allows you to add as many line items with a #<n>
      Close;
      SQL.Text := 'INSERT INTO SalesReceiptLineItems (Memo, Date, CustomerId, TaxItemId, ItemId#1, ItemAmount#1, ItemId#2, ItemAmount#2)  '
                + 'VALUES ( ' + QuotedStr(ARec.LogList) + ', ' +  QuotedStr(DateToStr(ARec.Date)) + ',' +
                  QuotedStr(CustomerId) + ',' + QuotedStr(TaxId) + ',' +
                  QuotedStr(ItemId) + ',' + FloatToStr(ARec.NetSales) + ',' +
                  QuotedStr(ItemId1) + ',' + FloatToStr(ARec.NetSales*2) +  ')';
      ExecSQL;
      Close;
      SQL.Text := 'SELECT MAX(ID) ID FROM SalesReceipts WHERE CustomerId = ' + QuotedStr(CustomerId);
      Open;
      if EOF then exit else Result := FieldByName('ID').AsString;
      Close;
    end;
  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

procedure TQBDesktopIntegration.ResetQBSalesRecord(var ARecord: TQBDesktopSalesRecord);
begin
  with ARecord do
  begin
    LastRefID := '';
    LogList := '';
    Date := NullDate;
    NetSales := 0;
    TaxableSales := 0;
    NonTaxableSales := 0;
    TaxExemptSales := 0;
    Tax1Amount := 0;
    Tax2Amount := 0;
    COGS := 0;
    SetLength(Payments, 0);
    SetLength(DeptEntries, 0);
  end;
end;

function TQBDesktopIntegration.SalesSettings(const ACat, AName: string): string;
begin
  Result := '';
  SalesSettingsFill;
  if memSalesReceipt.Active and memSalesReceipt.Locate('Cat;Name', VarArrayOf([ACat, AName]), [loCaseInsensitive]) and
    (not memSalesReceiptInActive.AsBoolean) then
    Result := memSalesReceiptQbName.AsString;
end;

procedure TQBDesktopIntegration.SalesSettingsFill;
var
  I, Y: Integer;
//  APrefSetting: string;
  AXmlEle: IXMLNode;
  // ADataSet: TDataSet;
  AStrList: TStringList;
begin
  if memSalesReceipt.Active then
    Exit;

  // APrefSetting := ProgPref('QBSalesReceiptItems');
  if JournalEntryMode then
    XMLDocument.Xml.LoadFromFile(ExtractFilePath(ParamStr(0))+'JournalEntryModePrefs.xml')
  else
    XMLDocument.Xml.LoadFromFile(ExtractFilePath(ParamStr(0))+'SalesReceiptModePrefs.xml');

  {if APrefSetting = '' then
    APrefSetting := '<Data></Data>';}

  memSalesReceipt.Open;
  memSalesReceipt.First;
  while not memSalesReceipt.Eof do
    memSalesReceipt.Delete;
  //XMLDocument.Xml.Text := APrefSetting;
  XMLDocument.Active := True;
  AXmlEle := XMLDocument.DocumentElement;
  for I := 0 to AXmlEle.ChildNodes.Count - 1 do
  begin
    memSalesReceipt.Insert;
    memSalesReceiptCat.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['Category']);
    memSalesReceiptName.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['Name']);
    memSalesReceiptQbName.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbName']);
    memSalesReceiptQbAccount.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbAccount']);
    memSalesReceiptQbAccountType.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbAccountType']);
    memSalesReceiptQbType.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['QbType']);
    if memSalesReceiptQbType.AsString = '' then
      memSalesReceiptQbType.AsString := 'Debit';
    memSalesReceiptInActive.AsString := VarToStr(AXmlEle.ChildNodes[I].Attributes['InActive']);
    memSalesReceiptSortColumn.AsInteger := I;
    memSalesReceipt.Post;
  end;
  SalesSettingsAssert('Info', 'CustomerName', I);
  SalesSettingsAssert('Info', 'TaxItemName', I);
  SalesSettingsAssert('Info', 'ItemClassName', I);
  SalesSettingsAssert('Sales', 'NetSales', I);
  SalesSettingsAssert('Sales', 'Taxable', I);
  SalesSettingsAssert('Sales', 'NonTaxable', I);
  SalesSettingsAssert('Sales', 'TaxExempt', I);
  SalesSettingsAssert('Sales', 'Adjustment', I);
  SalesSettingsAssert('Taxes', 'Tax1', I);
  SalesSettingsAssert('Taxes', 'Tax2', I);
  SalesSettingsAssert('Payments', 'PdAcnt', I);

  SalesSettingsAssert('Payments', 'Cash', I);
  SalesSettingsAssert('Payments', 'Check', I);
  SalesSettingsAssert('Payments', 'Crdt Card', I);
  SalesSettingsAssert('Payments', 'Store Crdt', I);
  SalesSettingsAssert('Payments', 'On Account', I);

  SalesSettingsAssert('COGS', 'COGS', I);
  SalesSettingsAssert('COGS', 'COGS Inevntory Asset', I);

  if JournalEntryMode then
  begin
    AStrList := TStringList.Create;
    try
      AStrList.Add('GROCERY');
      AStrList.Add('DAIRY');
      AStrList.Add('MEAT');
      SalesSettingsAssert('Departments', 'Dept: <Undefined>', I);
      for Y := 0 to AStrList.Count - 1 do
        SalesSettingsAssert('Departments', 'Dept: ' + AStrList[Y], I);
    finally
      AStrList.Free;
    end;
  end;
end;

procedure TQBDesktopIntegration.SalesSettingsAssert(const ACat, AName: string; var AIndex: Integer);
begin
  if not memSalesReceipt.Locate('Cat;Name', VarArrayOf([ACat, AName]), [loCaseInsensitive]) then
  begin
    Inc(AIndex);
    memSalesReceipt.Insert;
    memSalesReceiptCat.AsString := ACat;
    memSalesReceiptName.AsString := AName;
    memSalesReceiptSortColumn.AsInteger := AIndex;
    memSalesReceiptQbType.AsString := 'Debit';
    memSalesReceipt.Post;
  end;
end;

function TQBDesktopIntegration.SalesJournalEntryAddUpdate(ARec: TQBDesktopSalesRecord): string;
var
  CustomerId : string;
  TaxItemId  : string;
  AccountID  : string;
begin
  if ARec.LastRefID <> '' then
  begin
    try
      JournalDelete(Arec.LastRefID);
    except
    end;
  end;
  //insert a set of balancing entries - 2 credits, 1 debit
  // accounts mentioned must be present in the chart of accounts
  try
    With commonFDQ do begin
      Close;
      SQL.Text := 'INSERT INTO JournalEntryLines (ReferenceNumber, Date, LineType#1, LineAccount#1, LineAmount#1, LineType#2, LineAccount#2, LineAmount#2, LineType#3, LineAccount#3, LineAmount#3)  '
                + 'VALUES ( ' + QuotedStr(ARec.LastRefID) + ',' + QuotedStr(DateToStr(ARec.Date)) + ','
                + QuotedStr('Credit') + ',' + QuotedStr('Advertising and Promotion') + ',' + '100' + ','
                + QuotedStr('Credit') + ',' + QuotedStr('Automobile Expense') + ',' + '20' + ','
                + QuotedStr('Debit') + ',' + QuotedStr('bankcash') + ',' + '120' + ')';

      ExecSQL;
      Close;
      SQL.Text := 'SELECT MAX(ID) ID FROM JournalEntries';
      Open;
      if EOF then exit else Result := FieldByName('ID').AsString;
      Close;

    end;

  except
    on E: Exception do
      QbLastError := E.Message;
  end;
end;

function TQBDesktopIntegration.JournalDelete(const ARefID: string): Boolean;
begin
  Result := False;
  try
    try
      With CommonFDQ do begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT ID FROM JournalEntries WHERE Upper(ReferenceNumber) = ' + QuotedStr(UpperCase(ARefId));
        Open;
        result :=  (RecordCount <> 0);
        if result then begin
          SQL.Text := 'DELETE FROM JournalEntries WHERE ID = ' + FieldByName('ID').AsString;
          ExecSQL;
        end else begin
          QbLastError := Format('Invoice #%s not found in Quickbooks', [ARefID]);
        end;
      end;
    except
      on E: Exception do
        QbLastError := E.Message;
    end;
  finally
    // AppActions.HideHourGlassCursor;
  end;

end;

end.
